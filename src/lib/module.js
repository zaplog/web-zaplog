export const Module = (obj) => (x) =>
  Object.entries(obj).reduce(async (acc, [key, value]) => {
    const y = await acc;
    const newValue = typeof value === 'function' ? value(y) : value;
    return {
      ...y,
      [key]: await newValue,
    };
  }, x);
