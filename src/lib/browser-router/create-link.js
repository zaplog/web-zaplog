export const createLink = (props, ...children) =>
  async function* (app) {
    const elementIterator = app.createElement('a', props, ...children)(app);
    const element = (await elementIterator.next()).value;
    const listener = (event) => {
      event.preventDefault();
      app.router.navigateByUrl({ url: new app.URL(element.href) });
    };
    element.addEventListener('click', listener, { signal: app.signal });
    yield element;
    return elementIterator.next();
  };
