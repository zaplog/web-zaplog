import { Router } from 'simple-isomorphic-router';

export class BrowserRouter extends Router {
  #window = null;
  #location = null;
  #activatedRoute = null;
  #URL = null;
  #promise = null;
  #resolve = null;
  constructor({ window, location, URL }) {
    super();
    this.#window = window;
    this.#location = location;
    this.#URL = URL;
    window.addEventListener('popstate', (_event) => {
      this.#redirect({
        activatedRoute: this.match(new this.#URL(document.location)),
        skipLocationChange: true,
      });
    });
  }

  async *[Symbol.asyncIterator]() {
    if (!this.#activatedRoute) {
      this.#activatedRoute = this.match(this.#location);
    }
    yield Promise.resolve(this.#activatedRoute);
    while (true) {
      if (!this.#promise) {
        this.#promise = new Promise((resolve) => {
          this.#resolve = resolve;
        });
      }
      yield this.#promise;
    }
  }

  #redirect({ activatedRoute, replaceUrl, skipLocationChange }) {
    this.#activatedRoute = activatedRoute;
    if (!skipLocationChange) {
      this.#window.history[replaceUrl ? 'replaceState' : 'pushState'](
        null,
        '',
        this.#activatedRoute.url.toString()
      );
    }
    if (this.#resolve) {
      this.#promise = null;
      console.log(this.#activatedRoute, 'nieuwe route');
      this.#resolve(this.#activatedRoute);
    }
  }

  navigate({ route, params, replaceUrl, skipLocationChange }) {
    this.#redirect({
      activatedRoute: {
        route,
        params,
        url: new this.#URL(route.getPathname(params), this.#location),
      },
      replaceUrl,
      skipLocationChange,
    });
  }

  navigateByUrl({ url, replaceUrl, skipLocationChange }) {
    this.#redirect({
      activatedRoute: this.match(url),
      replaceUrl,
      skipLocationChange,
    });
  }
}
