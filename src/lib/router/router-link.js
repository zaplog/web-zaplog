import { pipe, map, combineLatest } from 'create-async-generator';
import { combineObject } from '../combine-object.js';

export const RouterLink =
  (
    {
      href,
      activeClass,
      route,
      params,
      searchParams,
      searchParamsHandling,
      classes,
      ...props
    },
    ...children
  ) =>
  ({ createLink, location }) =>
    createLink(
      {
        href:
          href ??
          pipe(
            combineLatest([
              combineObject(params ?? {}),
              combineObject(searchParams ?? {}),
            ]),
            map(([paramsValue, searchParamsValue]) =>
              route.getHref(paramsValue, {
                searchParams:
                  searchParamsHandling === 'merge'
                    ? route.mergeSearchParams(
                        location.searchParams,
                        searchParamsValue
                      )
                    : new URLSearchParams(searchParamsValue),
              })
            )
          ),
        class: [
          classes ? `${classes} ` : null,
          activeClass
            ? async function* ({ router }) {
                for await (const activatedRoute of router) {
                  if (activatedRoute.route === route) {
                    yield pipe(
                      combineObject(params ?? {}, { eager: true }),
                      map((paramsValue) =>
                        Object.keys(paramsValue).filter(
                          (key) =>
                            activatedRoute.params[key] !== paramsValue[key]
                        ).length === 0
                          ? activeClass
                          : null
                      )
                    );
                  }
                }
              }
            : null,
        ],
        ...props,
      },
      ...children
    );
