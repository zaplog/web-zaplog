const promises = new Map();
const resolves = new Map();

export const getFormData = (name) =>
  async function* () {
    yield null;
    while (true) {
      if (!promises.has(name)) {
        const promise = new Promise((resolve) => {
          resolves.set(name, resolve);
        });
        promises.set(name, promise);
      }
      yield promises.get(name);
    }
  };

export const createForm = ({ name, method, ...props }, ...children) =>
  async function* (app) {
    const elementIterable = app.createElement(
      'form',
      { method: method ?? 'post', name, ...props },
      ...children,
      name
        ? app.createElement('input', {
            type: 'hidden',
            name: 'form_id',
            value: name,
          })
        : null
    )(app);
    const element = (await elementIterable.next()).value;
    const listener = (event) => {
      event.preventDefault();
      if (element.method === 'post') {
        if (resolves.has(name)) {
          const formData = new FormData(element);
          const resolve = resolves.get(name);
          promises.delete(name);
          resolves.delete(name);
          resolve(formData);
        }
      } else if (element.method === 'get') {
        app.router.navigateByUrl({
          url: new app.URL(element.getAttribute('action')),
        });
      }
    };
    element.addEventListener('submit', listener, { signal: app.signal });
    yield element;
    yield elementIterable.next();
  };
