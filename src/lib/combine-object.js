import { combineLatest } from 'async-iterators-combine';
import { createAsyncGenerator } from 'create-async-generator';

export const combineObject = (object, options) =>
  async function* (x) {
    const keys = Object.keys(object);
    const generators = Object.values(object).map((value) =>
      createAsyncGenerator(value)
    );
    // TODO maybe create an option to enable or disable this behavior
    if (!generators.length) {
      yield {};
    } else {
      const combinator = combineLatest(
        generators.map((generator) => generator(x)),
        options
      );
      for await (const values of combinator) {
        yield values.reduce(
          (acc, value, i) => ({ ...acc, [keys[i]]: value?.toString() }),
          {}
        );
      }
    }
  };
