export const createLink =
  (props, ...children) =>
  ({ createElement }) =>
    createElement('a', { ...props }, ...children);
