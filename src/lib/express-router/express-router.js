import { Router } from 'simple-isomorphic-router';

export class ExpressRouter extends Router {
  #res = null;
  #location = null;
  #URL = null;
  #activatedRoute = null;

  constructor({ res, location, URL }) {
    super();
    this.#res = res;
    this.#location = location;
    this.#URL = URL;
  }

  [Symbol.asyncIterator]() {
    if (!this.#activatedRoute) {
      this.#activatedRoute = this.match(this.#location);
    }
    const activatedRoute = this.#activatedRoute;
    let nextDone = false;
    return {
      next() {
        const done = nextDone;
        const value = done ? undefined : activatedRoute;
        nextDone = true;
        return Promise.resolve({ value, done });
      },
      return() {
        nextDone = true;
        return { done: true };
      },
    };
  }

  #redirect({ url, replaceUrl, permanent }) {
    this.#res.redirect(
      replaceUrl ? 303 : permanent ? 301 : 302,
      url.toString()
    );
  }

  navigate({ route, params, replaceUrl, skipLocationChange, permanent }) {
    if (skipLocationChange) {
      const url = new this.#URL(route.getPathname(params), this.#location);
      this.#activatedRoute = { route, params, url };
    } else {
      this.#redirect({ url: route.getHref(params), replaceUrl, permanent });
    }
  }

  navigateByUrl({ url, replaceUrl, skipLocationChange, permanent }) {
    if (skipLocationChange) {
      this.#activatedRoute = this.match(url);
    } else {
      this.#redirect({ url, replaceUrl, permanent });
    }
  }
}
