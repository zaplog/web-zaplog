export const Form =
  (props, ...children) =>
  ({ createForm }) =>
    createForm(props, ...children);
