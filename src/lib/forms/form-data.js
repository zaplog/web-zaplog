export const FormData =
  (name) =>
  ({ getFormData }) =>
    getFormData(name);
