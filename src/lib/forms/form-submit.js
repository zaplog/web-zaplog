export const FormSubmit =
  (name) =>
  ({ getFormData, ...rest }) =>
    async function* () {
      for await (const formData of getFormData(name)({ ...rest })) {
        if (formData) {
          yield formData;
        }
      }
    };
