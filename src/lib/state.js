export const State =
  (stateFn) =>
  ({ runState, ...props }) =>
    runState(() => stateFn, { ...props, runState });
