import { StatusError } from './status-error.js';

export class HttpErrorResponse extends StatusError {
  constructor(json, request, response) {
    super(json?.message ?? response.status, response.status);
    this.json = json;
    this.name = response.statusText;
    this.request = request;
    this.response = response;
  }
}
