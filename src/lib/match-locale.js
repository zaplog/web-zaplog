export const matchLocale = ({ availableLocales, matchLocales, compareOn }) =>
  matchLocales.find((userLocale) =>
    availableLocales.find(
      (availableLocale) =>
        compareOn
          .map((compare) => availableLocale[compare] !== userLocale[compare])
          .filter(Boolean).length === 0
    )
  );
