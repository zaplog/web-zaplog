export const getFormData = (name = null) =>
  async function* ({ req }) {
    if (req.method === 'POST' && (!name || req.body.form_id === name)) {
      yield new URLSearchParams(req.body);
    } else {
      yield null;
    }
  };
