export const createForm =
  ({ name, method, ...props }, ...children) =>
  ({ createElement }) =>
    createElement(
      'form',
      { method: method ?? 'post', name, ...props },
      ...children,
      name
        ? createElement('input', {
            type: 'hidden',
            name: 'form_id',
            value: name,
          })
        : null
    );
