import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: './browser.js',
  output: {
    dir: 'output',
    format: 'es',
  },
  plugins: [nodeResolve(), commonjs()],
};
