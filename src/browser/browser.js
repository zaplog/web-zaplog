import { createAsyncGenerator } from 'create-async-generator';
import { ELEMENT_RENDERERS } from '@async-to-html/dom/element-renderers/get-default-renderers.js';
import { render } from '@async-to-html/render/render.js';
import { Body } from '../app/components/body/body.js';
import { Head } from '../app/components/head/head.js';
import { BrowserApp } from './browser-app.js';

const configuration = {
  apiUrl: '/api',
  availableLanguages: ['en', 'nl'],
  theme: 'osmode',
  production: false,
  configSet: false,
  channelSubdomain: false,
  community: true,
  adminChannelId: 1,
};

let globalError;

export const runAsync = (asyncFn) => {
  (async () => {
    try {
      await asyncFn();
    } catch (error) {
      if (!globalError) {
        globalError = error;
        retry();
      }
    }
  })();
};

export const runState = (asyncFn, x) => {
  let emitted = false;
  let latestResult;
  let resolvers = [];
  // const { generator } = Cancelable({
  //   cancelPromise,
  // });
  // const asyncResult = generator(createAsyncGenerator(asyncFn)({ ...x }));
  const asyncResult = createAsyncGenerator(asyncFn)({ ...x });
  runAsync(async () => {
    try {
      for await (const result of asyncResult) {
        emitted = true;
        latestResult = result;
        for (const resolve of resolvers) {
          resolve(result);
        }
        resolvers = [];
      }
    } catch (error) {
      if (!globalError) {
        globalError = error;
        retry();
      }
    }
  });
  return {
    async *[Symbol.asyncIterator]() {
      if (emitted) {
        yield latestResult;
      }
      yield new Promise((resolve) => {
        resolvers.push(resolve);
      });
    },
  };
};

const render2 = async ({ Head, Body, configuration, error }) => {
  const abortController = new AbortController();

  if (error) {
    document.head.textContent = '';
    document.body.textContent = '';
  }

  const headPlaceholder = document.createComment('head');
  const bodyPlaceholder = document.createComment('body');
  document.head.append(headPlaceholder);
  document.body.append(bodyPlaceholder);

  const app = await BrowserApp({
    document,
    configuration,
    runAsync,
    runState,
    error,
    signal: abortController.signal,
  });

  await Promise.all([
    render(Head, {
      args: [app],
      placeholder: headPlaceholder,
      document,
      renderers: ELEMENT_RENDERERS,
    }),
    render(Body, {
      args: [app],
      placeholder: bodyPlaceholder,
      document,
      renderers: ELEMENT_RENDERERS,
    }),
  ]);

  // RenderModule.render(document.head, null, Head, {
  //   ...container,
  //   ...RenderModule,
  // });
  // RenderModule.render(document.body, null, Body, {
  //   ...container,
  //   ...RenderModule,
  // });
};

const retry = () => {
  try {
    // cancel();
    // const handledError = errorHandler(globalError);
    render2({ Head, Body, configuration });
  } catch (finalError) {
    console.error('execution has halted', finalError);
  }
};

try {
  render2({ Head, Body, configuration });
} catch (error) {
  console.log(error);
  if (!globalError) {
    globalError = error;
    retry();
  }
}

document.body.classList.add('body', 'background');
