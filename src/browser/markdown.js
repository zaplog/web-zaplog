for (let imageElement of document.querySelectorAll('.markdown .image')) {
  const src = imageElement.getAttribute('src');
  imageElement.classList.add('markdown__image--clickable');
  imageElement.addEventListener('click', (_event) => {
    open(src, '_blank');
  });
}
