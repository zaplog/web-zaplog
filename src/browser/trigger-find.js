if (find) {
  for (let findElement of document.querySelectorAll('.trigger-find')) {
    findElement.style.display = 'flex';
    findElement.addEventListener('submit', (event) => {
      event.preventDefault();
      const formData = new FormData(event.target);
      find(formData.get('query'));
    });
  }
}
