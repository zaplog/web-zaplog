import { BrowserCookies } from 'web-storage-browser-cookies';
import { createElement as createElementFactory } from '@async-to-html/dom/create-element.js';
import { html as htmlFactory } from '@async-to-html/dom/html.js';
import { safeHtml } from '@async-to-html/render/safe-html/safe-html.js';
import * as FormsModule from '../lib/browser-forms/browser-forms.js';
import { App } from '../app/app.js';
import { BrowserRouter } from '../lib/browser-router/browser-router.js';
import { createLink } from '../lib/browser-router/create-link.js';

export const BrowserApp = ({ configuration, document, ...props }) => {
  const location = new URL(document.location);
  return App({
    ...props,
    encode: (string) => string,
    decode: (string) => {
      const template = document.createElement('template');
      template.innerHTML = string.toString();
      return template.content.textContent;
    },
    ...FormsModule,
    html: htmlFactory({ document }),
    createElement: createElementFactory({ document }),
    htmlSafe: safeHtml,
    configuration,
    location,
    fetch,
    URL,
    URLSearchParams,
    Request,
    console,
    Intl,
    acceptLanguage: navigator.languages,
    referrer: document.referrer ? new URL(document.referrer) : null,
    translations: (async () => {
      const languages = await Promise.all(
        configuration.availableLanguages.map(async (language) => ({
          [language]: await (
            await fetch(`/assets/i18n/${language}.json`)
          ).json(),
        }))
      );
      return languages.reduce(
        (acc, curr) => ({
          ...acc,
          ...curr,
        }),
        {}
      );
    })(),
    createLink,
    router: new BrowserRouter({ window, location, URL }),
    storage: new BrowserCookies({
      window,
      options: {
        'max-age': 31536000,
        expires: new Date(
          new Date().setFullYear(new Date().getFullYear() + 1)
        ).toUTCString(),
        path: '/',
      },
    }),
  });
};
