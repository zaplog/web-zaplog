class ZaplogMarkdownTextarea extends HTMLElement {
  static STORAGE_KEY_WIDTH = 'zaplog-markdown-textarea__width';
  static STORAGE_KEY_FONT_SIZE = 'zaplog-markdown-textarea__font-size';
  static STORAGE_KEY_FONT_FAMILY = 'zaplog-markdown-textarea__font-family';
  #textarea = null;
  #slot = null;
  #fontSizes = [0.5, 0.75, 1, 1.25, 1.5, 2];

  resizeObserver = new ResizeObserver((entries) => {
    for (const entry of entries) {
      localStorage.setItem(
        ZaplogMarkdownTextarea.STORAGE_KEY_WIDTH,
        entry.borderBoxSize[0].inlineSize
      );
    }
  });

  connectedCallback() {
    const width = localStorage.getItem(
      ZaplogMarkdownTextarea.STORAGE_KEY_WIDTH
    );
    this.#textarea = this.querySelector('textarea');
    this.#slot = this.querySelector('.new-article__slot');
    this.#textarea.style.width = `${width}px`;
    this.resizeObserver.observe(this.#textarea);
    const fontSizeInput = document.createElement('select');
    const fontSize =
      parseFloat(
        localStorage.getItem(ZaplogMarkdownTextarea.STORAGE_KEY_FONT_SIZE)
      ) ?? 1;
    fontSizeInput.setAttribute('value', `${fontSize ?? '1'}`);
    for (const size of this.#fontSizes) {
      const sizeEl = document.createElement('option');
      sizeEl.setAttribute('value', size);
      if (size === fontSize) {
        sizeEl.setAttribute('selected', '');
      }
      sizeEl.append(`${size * 100}%`);
      fontSizeInput.append(sizeEl);
    }
    fontSizeInput.setAttribute('class', 'form-field');
    fontSizeInput.addEventListener('input', (event) => {
      this.setFontSize(event.target.value);
      localStorage.setItem(
        ZaplogMarkdownTextarea.STORAGE_KEY_FONT_SIZE,
        event.target.value
      );
    });
    this.#slot.append(fontSizeInput);
    this.setFontSize(fontSize);

    const fontFamilyInput = document.createElement('select');
    fontFamilyInput.setAttribute('class', 'form-field');
    const option = document.createElement('option');
    option.append('default');
    option.setAttribute('value', '');
    const fontFamily = localStorage.getItem(
      ZaplogMarkdownTextarea.STORAGE_KEY_FONT_FAMILY
    );
    fontFamilyInput.append(option);
    fontFamilyInput.addEventListener('input', (event) => {
      this.setFontFamily(event.target.value);
      localStorage.setItem(
        ZaplogMarkdownTextarea.STORAGE_KEY_FONT_FAMILY,
        event.target.value
      );
    });
    this.#slot.append(fontFamilyInput);
    this.setFontFamily(fontFamily);

    Promise.all([
      import('/browser/zaplog-markdown-textarea/windows-fonts.js'),
      import('/browser/zaplog-markdown-textarea/linux-fonts.js'),
    ]).then((imports) => {
      imports
        .reduce((acc, curr) => [...acc, ...curr.default], [])
        .filter((fontFace) => {
          try {
            return document.fonts.check(`1em ${fontFace}`);
          } catch (_error) {
            return false;
          }
        })
        .sort((a, b) => a.localeCompare(b, undefined, { sensitivity: 'base' }))
        .map((fontFace) => {
          const option = document.createElement('option');
          option.setAttribute('value', fontFace);
          if (fontFamily === fontFace) {
            option.setAttribute('selected', '');
          }
          option.append(fontFace);
          fontFamilyInput.append(option);
        });
    });
  }

  setFontSize(fontSize) {
    this.#textarea.style.fontSize = `${fontSize}rem`;
  }

  setFontFamily(fontFamily) {
    this.#textarea.style.fontFamily = fontFamily;
  }

  disconnectedCallback() {
    this.resizeObserver.unobserve(this.#textarea);
  }
}

customElements.define('zaplog-markdown-textarea', ZaplogMarkdownTextarea);
