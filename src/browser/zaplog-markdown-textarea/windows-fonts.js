export default [
  'Agency',
  'Aharoni',
  'Aldhabi',
  'Andalus',
  'Angsana',
  'AngsanaUPC',
  'Aparajita',
  'Arabic Typesetting',
  'Arial',
  'Bahnschrift',
  'Batang',
  'BatangChe',
  'BIZ UDGothic',
  'BIZ UDMincho',
  'Book Antiqua',
  'Browallia New',
  'BrowalliaUPC',
  'Calibri',
  'Calisto MT',
  'Cambria',
  'Cambria Math',
  'Candara',
  'Cascadia Code',
  'Century Gothic',
  'Comic Sans MS',
  'Consolas',
  'Constantia',
  'Copperplate Gothic',
  'Corbel',
  'Cordia New',
  'CordiaUPC',
  'Courier New',
  'DaunPenh',
  'David',
  'DengXian',
  'DilleniaUPC',
  'DFKai-SB',
  'DokChampa',
  'Dotum',
  'DotumChe',
  'Ebrima',
  'Estrangelo Edessa',
  'EucrosiaUPC',
  'Euphemia',
  'FangSong',
  'Franklin Gothic',
  'FrankRuehl',
  'FreesiaUPC',
  'Gabriola',
  'Gadugi',
  'Gautami',
  'Georgia',
  'Gill Sans MT',
  'Gisha',
  'Gulim',
  'GulimChe',
  'Gungsuh',
  'GungsuhChe',
  'HoloLens MDL2 Assets',
  'Impact',
  'Ink Free',
  'IrisUPC',
  'Iskoola Pota',
  'JasmineUPC',
  'Javanese Text',
  'KaiTi',
  'Kalinga',
  'Kartika',
  'Khmer UI',
  'KodchiangUPC',
  'Kokila',
  'Lao UI',
  'Latha',
  'Leelawadee',
  'Leelawadee UI',
  'Levenim MT',
  'LilyUPC',
  'Lucida Console',
  'Lucida Handwriting',
  'Lucida Sans Unicode',
  'Malgun Gothic',
  'Mangal',
  'Marlett',
  'Meiryo, Meiryo UI',
  'Microsoft Himalaya',
  'Microsoft JhengHei',
  'Microsoft JhengHei UI',
  'Microsoft New Tai Lue',
  'Microsoft PhagsPa',
  'Microsoft Sans Serif',
  'Microsoft Tai Le',
  'Microsoft Uighur',
  'Microsoft YaHei',
  'Microsoft YaHei UI',
  'Microsoft Yi Baiti',
  'MingLiU, PMingLiU',
  'MingLiU-ExtB, PMingLiU-ExtB',
  'MingLiU_HKSCS',
  'MingLiU_HKSCS-ExtB',
  'Miriam',
  'Miriam Fixed',
  'Mongolian Baiti',
  'MoolBoran',
  'MS Gothic',
  'MS PGothic',
  'MS Mincho',
  'MS PMincho',
  'MS UI Gothic',
  'MV Boli',
  'Myanmar Text',
  'Narkisim',
  'Nirmala UI',
  'NSimSun',
  'Nyala',
  ' OCR-A Extended',
  'Palatino Linotype',
  'Plantagenet Cherokee',
  'Raavi',
  'Rod',
  'Sakkal Majalla',
  'Sanskrit Text',
  'Segoe MDL2 Assets',
  'Segoe Print',
  'Segoe Script',
  'Segoe SD',
  'Segoe UI',
  'Segoe UI Emoji',
  'Segoe UI Historic',
  'Segoe UI Symbol',
  'Segoe UI Variable',
  'Segoe Fluent Icons',
  'Shonar Bangla',
  'Shruti',
  'SimHei',
  'Simplified Arabic',
  'SimSun',
  'SimSun-ExtB',
  'Sitka Banner',
  'Sitka Display',
  'Sitka Heading',
  'Sitka Small',
  'Sitka Subheading',
  'Sitka Text',
  'Sylfaen',
  'Symbol',
  'Tahoma',
  'Times New Roman',
  'Traditional Arabic',
  'Trebuchet MS',
  'Tw Cen MT',
  'Tunga',
  'UD Digi Kyokasho N-R',
  'UD Digi Kyokasho N-B',
  'UD Digi Kyokasho NK-R',
  'UD Digi Kyokasho NK-B',
  'UD Digi Kyokasho NP-R',
  'UD Digi Kyokasho NP-B',
  'Urdu Typesetting',
  'Utsaah',
  'Vani',
  'Verdana',
  'Vijaya',
  'Vrinda',
  'Webdings',
  'Wingdings',
  'Yu Gothic',
  'Yu Gothic UI',
  'Yu Mincho',
];
