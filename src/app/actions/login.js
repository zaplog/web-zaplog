import { asyncToString } from 'async-to-string';
import { pipe, map } from 'create-async-generator';
import { LoginEmailTemplate } from '../components/login-email-template/login-email-template.js';
import { SiteTitle } from '../resources/site-settings.js';
import { StoreTwofa } from './store-twofa.js';

export const Login =
  ({ email, markdown }) =>
  ({ translate, routes, api, location, ...rest }) =>
    pipe(
      StoreTwofa({ email }),
      map(async () => {
        for await (const siteTitle of SiteTitle({
          translate,
          routes,
          api,
          ...rest,
        })) {
          const subject = translate('emailSubjects.LOGIN', {
            siteTitle: siteTitle,
          });
          const template = LoginEmailTemplate({
            subject,
            loginurl: `${location.origin}${routes.twofa.getPathname({
              token: '',
            })}`,
          });
          const response = await api.createSession({
            email,
            markdown,
            subject: (
              await asyncToString(subject, {
                args: [
                  {
                    translate,
                    routes,
                    api,
                    ...rest,
                  },
                ],
              })
            ).toString(),
            template: (
              await asyncToString(template, {
                args: [
                  {
                    translate,
                    routes,
                    api,
                    ...rest,
                  },
                ],
              })
            ).toString(),
          });
          return response;
        }
      })
    );
