export const SetAuthentication =
  (token, channel) =>
  ({ storage, configuration }) => {
    storage.setItem(configuration.sessionName, token);
    storage.setItem(configuration.channelNameName, channel.name);
    storage.setItem(configuration.channelIdName, channel.id);
  };
