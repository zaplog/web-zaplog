export const ResetAuthentication =
  () =>
  ({ storage, configuration }) => {
    storage.removeItem(configuration.sessionName);
    storage.removeItem(configuration.channelNameName);
    storage.removeItem(configuration.channelIdName);
  };
