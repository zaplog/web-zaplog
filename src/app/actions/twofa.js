import { SetAuthentication } from './set-authentication.js';

export const Twofa =
  (params) =>
  async ({ api, configuration }) => {
    const response = await api.getToken({
      token: params.token,
    });
    const { token, channel } = await response.json();
    return SetAuthentication(token[configuration.sessionName], channel);
  };
