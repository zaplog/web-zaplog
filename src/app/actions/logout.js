import { ResetAuthentication } from './reset-authentication.js';

export const Logout =
  () =>
  async ({ api }) => {
    try {
      await api.deleteSession();
      return ResetAuthentication();
    } catch (_error) {
      return ResetAuthentication();
    }
  };
