export const StoreTwofa =
  ({ email }) =>
  async ({ storage, configuration, StoredTwoFa }) => {
    for await (const storedTwoFa of StoredTwoFa()) {
      const newStoredTwoFa = [
        ...storedTwoFa.filter(
          ({ type, value }) => !(type === 'email' && value === email)
        ),
        { type: 'email', value: email },
      ];
      storage.setItem(
        configuration.userTwoFaName,
        JSON.stringify(newStoredTwoFa)
      );
      return;
    }
  };
