export const getChannelSubdomain = (location) => {
  const hostparts = location.hostname.split('.');
  return hostparts.length > 2 && isNaN(hostparts[hostparts.length - 1])
    ? hostparts[0]
    : null;
};
