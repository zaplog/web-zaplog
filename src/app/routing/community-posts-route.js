import { PathToRegexpRoute } from 'simple-isomorphic-router';

export class CommunityPostsRoute extends PathToRegexpRoute {
  URLSearchParams = null;
  constructor(parts, { URLSearchParams, ...options }) {
    super(parts, { URLSearchParams, ...options });
    this.URLSearchParams = URLSearchParams;
  }

  getHref(
    { tag, tags, channel, query, ...params } = {},
    { searchParams } = {}
  ) {
    if (!searchParams) {
      searchParams = new this.URLSearchParams();
    }
    const newQuery = [
      tags
        ? tags
            .split(',')
            .map((tag) => `#${tag}`)
            .join(' ')
        : null,
      tag ? `#${tag}` : null,
      channel ? `@${channel}` : null,
      query ?? null,
    ].filter(Boolean);
    searchParams.set(
      'query',
      newQuery.length ? newQuery.join(' ') : searchParams.get('query') ?? ''
    );
    return super.getHref(params, {
      searchParams,
    });
  }
}
