import { PathToRegexpRoute } from 'simple-isomorphic-router';
import { ArticleRoute } from './article-route.js';
import { CommunityPostsRoute } from './community-posts-route.js';

export const Routes = ({
  channelHostname,
  channelPathnamePrefix,
  communityHostname,
  configuration,
  URLSearchParams,
  ...rest
}) => ({
  rawArticle: new ArticleRoute(
    {
      hostname: channelHostname,
      pathname: '/post/:id/:title.md',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  article: new ArticleRoute(
    {
      hostname: channelHostname,
      pathname: '/post/:id/:title',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  articleDelete: new ArticleRoute(
    {
      hostname: channelHostname,
      pathname: '/post/:id/:title/delete',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  home: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  posts: new CommunityPostsRoute(
    {
      hostname: communityHostname,
      pathname: '/posts',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  postsAtom: new CommunityPostsRoute(
    {
      hostname: communityHostname,
      pathname: '/posts.atom',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  index: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/topics',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channelIndex: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/channels',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  login: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/login',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  logout: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/logout',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  // TODO: this should not be needed
  emptyTwofa: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/2fa/',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  twofa: new PathToRegexpRoute(
    {
      hostname: communityHostname,
      pathname: '/2fa/:token([^/]*)',
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channel: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channelAtom: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/index.atom`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channelEdit: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/edit`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channelMembers: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/members`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  channelMemberships: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/memberships`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  newArticle: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/createpost`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  editArticle: new PathToRegexpRoute(
    {
      hostname: channelHostname,
      pathname: `${channelPathnamePrefix}/editpost/:id`,
      ...rest,
    },
    {
      URLSearchParams,
    }
  ),
  ...(configuration.community
    ? {
        homeAtom: new PathToRegexpRoute(
          {
            hostname: communityHostname,
            pathname: '/index.atom',
            ...rest,
          },
          {
            URLSearchParams,
          }
        ),
        discussion: new PathToRegexpRoute(
          {
            hostname: communityHostname,
            pathname: '/discussion',
            ...rest,
          },
          {
            URLSearchParams,
          }
        ),
      }
    : {}),
});
