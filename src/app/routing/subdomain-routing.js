import { match } from 'path-to-regexp';

export const SubdomainRouting = ({ location }) => {
  const channelSubdomain = match(':channel.:domainname.:tld');
  const communityDomain = match(':domainname.:tld');
  const params =
    channelSubdomain(location.hostname)?.params ||
    communityDomain(location.hostname)?.params;

  const hostname = params
    ? `${params.domainname}.${params.tld}`
    : location.hostname;

  return {
    channelPathnamePrefix: '',
    channelHostname: `:channel.${hostname}`,
    communityHostname: hostname,
    port: location.port,
    protocol: location.protocol,
  };
};
