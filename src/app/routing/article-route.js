import { PathToRegexpRoute } from 'simple-isomorphic-router';
import { formatArticleTitle } from '../format-article-title.js';

export class ArticleRoute extends PathToRegexpRoute {
  getHash({ commentId }) {
    return commentId ?? null;
  }

  getPathname({ title, ...rest }) {
    return `${super.getPathname({
      title: formatArticleTitle(title),
      ...rest,
    })}`;
  }

  getHostname(params = {}) {
    // TODO: make sure channel is always passed, redirecting on null is a stupid solution
    return super.getHostname({ ...params, channel: params.channel ?? 'null' });
  }
}
