import { getChannelSubdomain } from './get-channel-subdomain.js';

export const getTld = (location, port = true) => {
  const channelSubdomain = getChannelSubdomain(location);
  const hostparts = location[port ? 'host' : 'hostname'].split('.');
  return (channelSubdomain ? hostparts.slice(1) : hostparts).join('.');
};
