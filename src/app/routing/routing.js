import { Routes } from './routes.js';
import { SubdomainRouting } from './subdomain-routing.js';

export const Routing = ({
  URLSearchParams,
  location,
  configuration,
  router,
}) => {
  const routes = Routes({
    URLSearchParams,
    configuration,
    ...(configuration.channelSubdomain
      ? SubdomainRouting({ location })
      : {
          channelPathnamePrefix: '/channel/:channel',
        }),
  });
  router.addRoute(...Object.values(routes));
  return {
    ...routes,
  };
};
