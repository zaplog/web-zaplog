export const licenses = {
  noRightsApply: 'No Rights Apply',
  allRightsReserved: 'All Rights Reserved',
  noRightsReserved: 'No Rights Reserved (CC0 1.0)',
  someRightsReserved: 'Some Rights Reserved (CC BY-SA 4.0)',
};

export const algorithms = {
  all: 'all', // all articles selected for channelpage
  channel: 'channel', // only own articles selected for channelpage
  popular: 'popular', // only most popular selected for channelpage
  voted: 'voted', // articles voted upon selected for channelpage
  mixed: 'mixed', // popular|channels + voted
};

export const Interactions = {
  REACTED: 'reacted',
  TAGGED: 'tagged',
  VOTES: 'votes',
  POSTED: 'posted',
};

export const ActivityTypes = {
  InsertLink: 'on_insert_link',
  UpdateLink: 'on_update_link',
  InsertReaction: 'on_insert_reaction',
  InsertVote: 'on_insert_vote',
  DeleteVote: 'on_delete_vote',
};

export const Errors = {
  MembersonlyArticle: 'Membersonly article requires membership',
};

export const languages = [
  'ar',
  'bn',
  'cs',
  'da',
  'de',
  'el',
  'en',
  'es',
  'fi',
  'fr',
  'he',
  'hi',
  'hu',
  'id',
  'it',
  'ja',
  'ko',
  'nl',
  'no',
  'pl',
  'pt',
  'ro',
  'ru',
  'sk',
  'sv',
  'ta',
  'th',
  'tr',
  'zh',
];
