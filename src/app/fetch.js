import { HttpErrorResponse } from '../lib/http/http-error-response.js';

export const Fetch =
  ({ Request, storage, configuration, fetch, context, console }) =>
  async (...args) => {
    const request = new Request(...args);
    const token = storage.getItem(configuration.sessionName);
    if (token) {
      request.headers.append(
        configuration.channelHeaderName,
        storage.getItem(configuration.channelIdName)
      );
      request.headers.append('Authorization', `Bearer ${token}`);
    }
    if (configuration.apiKey) {
      request.headers.append('X-Api-Key', configuration.apiKey);
    }
    const response = await fetch.apply(context, [request]);
    if (response.status.toString().charAt(0) !== '2') {
      let error;
      try {
        error = await response.json();
      } catch (jsonError) {
        console.log(jsonError);
      }
      throw new HttpErrorResponse(error, request, response);
    }
    return response;
  };
