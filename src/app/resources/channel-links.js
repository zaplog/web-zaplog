import { pipe, map, combineLatest } from 'create-async-generator';
import { toUTCDate } from '../components/i18n/to-utc-date.js';
import { ChannelName } from './channel-name.js';

export const ChannelLinksResponse = async ({
  runAsync,
  router,
  api,
  configuration,
}) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return pipe(
    combineLatest([ChannelName, router]),
    map(([channelName, { params, url }]) => {
      if (!storedValue) {
        storedValue = (async () => {
          const response = await api.getChannelLinks({
            channel: params.channel ?? channelName,
            offset: url.searchParams.has('page')
              ? (parseInt(url.searchParams.get('page'), 10) - 1) *
                configuration.pageSize
              : 0,
            count: configuration.pageSize,
          });
          return [response, response.json()];
        })();
      }
      return storedValue;
    })
  );
};

export const ChannelLinks = pipe(
  ({ ChannelLinksResponse }) => ChannelLinksResponse,
  map(
    ([_channelLinksResponse, channelLinksResponseJson]) =>
      channelLinksResponseJson
  )
);

export const ChannelLinksLastModified = pipe(
  ({ ChannelLinksResponse }) => ChannelLinksResponse,
  map(
    ([channelLinksResponse, _channelLinksResponseJson]) =>
      new Date(channelLinksResponse.headers.get('last-modified'))
  )
);

export const ChannelLinksLastModifiedIsoString = pipe(
  ChannelLinksLastModified,
  map((lastModified) => toUTCDate(lastModified).toISOString())
);
