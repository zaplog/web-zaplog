import { map, pipe } from 'create-async-generator';
import { RetryFormData } from './retry-form-data.js';

export const RetryFormDataString = pipe(
  RetryFormData,
  map((formData) => JSON.stringify(formData))
);
