import { pipe, map } from 'create-async-generator';

export const LinkReactions = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return async () => {
    for await (const { params } of router) {
      if (!storedValue) {
        storedValue = (async () =>
          (
            await api.getLinkReactions({
              linkId: params.id,
            })
          ).json())();
      }
      return storedValue;
    }
  };
};

export const LinkReactionsLength = pipe(
  ({ LinkReactions }) => LinkReactions,
  map(({ length }) => length)
);
