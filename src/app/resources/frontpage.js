import { pipe, map } from 'create-async-generator';
import { toUTCDate } from '../components/i18n/to-utc-date.js';

export const FrontpageResponse = async ({ runAsync, router, api }) => {
  let storedResponse = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedResponse = null;
    }
  });
  return () => {
    if (!storedResponse) {
      storedResponse = (async () => {
        const response = await api.getFrontpage();
        return [response, response.json()];
      })();
    }
    return storedResponse;
  };
};

export const Frontpage = pipe(
  ({ FrontpageResponse }) => FrontpageResponse,
  map(([_response, json]) => json)
);

export const FrontpageLastModified = pipe(
  ({ FrontpageResponse }) => FrontpageResponse,
  map(([response]) => new Date(response.headers.get('last-modified')))
);

export const FrontpageLastModifiedIsoString = pipe(
  FrontpageLastModified,
  map((lastModified) => toUTCDate(lastModified).toISOString())
);

export const FrontpageTrendinglinks = pipe(
  Frontpage,
  map(({ trendinglinks }) => trendinglinks)
);

export const FrontpageFeatured = pipe(
  FrontpageTrendinglinks,
  map((trendinglinks) => trendinglinks.slice(0, 6))
);

export const FrontpageTrendingChannels = pipe(
  Frontpage,
  map(({ trendingchannels }) => trendingchannels)
);

export const FrontpageTrendingTags = pipe(
  Frontpage,
  map(({ trendingtags }) => trendingtags)
);

export const FrontpageTrendingReactions = pipe(
  Frontpage,
  map(({ trendingreactions }) => trendingreactions)
);
