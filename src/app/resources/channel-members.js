export const ChannelMembers = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () => {
    if (!storedValue) {
      storedValue = (async () => (await api.getChannelMembers()).json())();
    }
    return storedValue;
  };
};
