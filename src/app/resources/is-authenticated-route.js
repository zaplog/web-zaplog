export const IsAuthenticatedRoute = async function* ({ router, routes }) {
  for await (const { route } of router) {
    yield [
      routes.channelEdit,
      routes.channelMembers,
      routes.channelMemberships,
      routes.newArticle,
      routes.editArticle,
    ].indexOf(route) !== -1;
  }
};
