export const StoredTwoFa = ({ storage, configuration }) => {
  let storedValue = null;
  return async function* () {
    if (!storedValue) {
      try {
        const storedTwoFaString = storage.getItem(configuration.userTwoFaName);
        const storedTwoFa = storedTwoFaString
          ? JSON.parse(storedTwoFaString)
          : [];
        storedValue = Array.isArray(storedTwoFa) ? storedTwoFa : [];
      } catch (_error) {
        storage.setItem(configuration.userTwoFaName, JSON.stringify([]));
        storedValue = [];
      }
    }
    yield storedValue;
  };
};
