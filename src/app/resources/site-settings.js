import { pipe, map } from 'create-async-generator';

export const siteSettings = { storedValue: null };

export const SiteSettings = ({ api, runAsync, configuration }) => {
  return async function* () {
    if (!siteSettings.storedValue) {
      siteSettings.storedValue = (async () => (await api.config()).json())();
      await runAsync(async () => {
        const { language } = await siteSettings.storedValue;
        if (configuration.availableLanguages.indexOf(language) === -1) {
          throw new Error(
            'The selected language is not supported by the application'
          );
        }
      });
    }
    yield siteSettings.storedValue;
  };
};

export const SiteLanguage = pipe(
  ({ SiteSettings }) => SiteSettings,
  map(({ language }) => language)
);

export const SiteTitle = pipe(
  ({ SiteSettings }) => SiteSettings,
  map(({ name }) => name)
);

export const SiteAvatar = pipe(
  ({ SiteSettings }) => SiteSettings,
  map(({ logo }) => logo)
);

export const SiteImage = pipe(
  ({ SiteSettings }) => SiteSettings,
  map(({ header }) => header)
);

export const SiteDescription = pipe(
  ({ SiteSettings }) => SiteSettings,
  map(({ description }) => description)
);
