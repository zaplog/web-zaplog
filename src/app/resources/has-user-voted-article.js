import { pipe, map } from 'create-async-generator';
import { Interactions } from '../models.js';
import { ChannelId } from './channel-id.js';

export const HasUserVotedArticle = pipe(
  ChannelId,
  map((channelId) => {
    if (!channelId) {
      return false;
    } else {
      return pipe(
        ({ Article }) => Article,
        map(
          ({ interactors }) =>
            channelId &&
            !!interactors
              .find(({ id }) => id === channelId)
              ?.interactions.split(',')
              .includes(Interactions.VOTES)
        )
      );
    }
  })
);
