import { pipe, map, combineLatest } from 'create-async-generator';

export const HasUserJustVotedArticle = pipe(
  ({ VotedLinkId, Article }) => combineLatest([VotedLinkId, Article]),
  map(([votedLinkId, { id }]) => id === votedLinkId)
);
