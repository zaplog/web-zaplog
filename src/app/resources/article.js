import { pipe, map } from 'create-async-generator';
import { licenses } from '../models.js';
import { formatArticleTitle } from '../format-article-title.js';

export const Article = async ({ runAsync, router, api, routes, referrer }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return async () => {
    for await (const { route, params } of router) {
      if (!storedValue) {
        storedValue = (async () =>
          (
            await api.getLink({
              id: params.id,
              referrer: route === routes.article ? referrer?.toString() : null,
            })
          ).json())();
      }
      return storedValue;
    }
  };
};

export const IsArticlePublished = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.published)
);

export const ArticleRelated = pipe(
  ({ Article }) => Article,
  map(({ related }) => related)
);

export const ArticleChannelId = pipe(
  ({ Article }) => Article,
  map(({ channel }) => channel.id)
);

export const ArticleId = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.id)
);

export const ArticleVotescount = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.votescount)
);

export const ArticleTitle = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.title)
);

export const FormattedArticleTitle = pipe(
  ArticleTitle,
  map((title) => formatArticleTitle(title))
);

export const ArticleXText = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.xtext)
);

export const ArticleDescription = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.description)
);

export const ArticleImage = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.image)
);

export const ArticleTags = pipe(
  ({ Article }) => Article,
  map(({ tags }) => tags)
);

export const ArticleKeywords = pipe(
  ({ Article }) => Article,
  map(({ tags }) => tags.map(({ tag }) => tag).join(','))
);

export const ArticleCopyright = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.copyright)
);

export const ArticleCreateDatetime = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.createdatetime)
);

export const ArticleMarkdown = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.markdown)
);

export const ArticleAuthor = pipe(
  ({ Article }) => Article,
  map(({ channel }) => channel.name)
);

export const ArticleAuthorHref = pipe(
  ArticleAuthor,
  map(
    (author) =>
      ({ routes }) =>
        routes.channel.getHref({ channel: author })
  )
);

export const ArticleCanonicalUrl = pipe(
  ({ Article }) => Article,
  map(
    ({ link, channel }) =>
      ({ routes }) =>
        routes.article.getHref({
          id: link.id,
          title: link.title,
          channel: channel.name,
        })
  )
);

export const ArticleShareUrl = pipe(
  ({ Article }) => Article,
  map(({ link, channel }) =>
    link.copyright === licenses.noRightsApply
      ? link.url
      : ({ routes }) =>
          `${routes.article.getHref({
            id: link.id,
            title: link.title,
            channel: channel.name,
          })}`
  )
);

export const ArticleShowReactions = pipe(
  ({ Article }) => Article,
  map(({ link }) => link.reactionsallowed)
);
