export const Token = ({ storage, configuration }) =>
  storage.getItem(configuration.sessionName);
