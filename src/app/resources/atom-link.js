export const AtomLink = async function* ({ router, routes, configuration }) {
  for await (const { route, params } of router) {
    switch (route) {
      case routes.home:
        if (configuration.community) {
          yield routes.homeAtom.getHref();
        } else {
          yield null;
        }
        break;
      case routes.posts:
        yield routes.postsAtom.getHref({
          ...params,
        });
        break;
      case routes.channel:
        yield routes.channelAtom.getHref({
          ...params,
        });
        break;
      default:
        yield null;
        break;
    }
  }
};
