export const UnpublishedLinks = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () => {
    if (!storedValue) {
      storedValue = async () => (await api.getUnpublishedLinks()).json();
    }
    return storedValue;
  };
};
