export const TwofaLinkSent = ({ storage }) => {
  let storedValue = null;
  return async function* () {
    if (!storedValue) {
      const value = storage.getItem('2faLinkSent');
      if (value) {
        storage.removeItem('2faLinkSent');
      }
      storedValue = value;
    }
    yield storedValue;
  };
};
