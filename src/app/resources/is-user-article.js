import { pipe, map } from 'create-async-generator';
import { ArticleChannelId } from './article.js';
import { ChannelId } from './channel-id.js';

export const IsUserArticle = pipe(
  ChannelId,
  map((channelId) => {
    if (!channelId) {
      return false;
    } else {
      return pipe(
        ArticleChannelId,
        map((articleChannelId) => articleChannelId === channelId)
      );
    }
  })
);
