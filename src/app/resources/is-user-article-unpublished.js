import { pipe, map } from 'create-async-generator';
import { IsArticlePublished } from './article.js';
import { IsUserArticle } from './is-user-article.js';

export const IsUserArticleUnpublished = pipe(
  IsArticlePublished,
  map((published) => {
    if (!published) {
      return IsUserArticle;
    } else {
      return false;
    }
  })
);
