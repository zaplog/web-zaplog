import { SiteTitle } from './site-settings.js';
import { ArticleTitle } from './article.js';

export const MetaTitle = async function* ({ router, routes, error }) {
  if (error) {
    yield SiteTitle;
  } else {
    for await (const { route, params, url } of router) {
      switch (route) {
        case routes.article:
          yield ArticleTitle;
          break;
        case routes.channel:
          yield `@${params.channel}`;
          break;
        case routes.posts:
          yield url.searchParams.has('query')
            ? url.searchParams.get('query')
            : SiteTitle;
          break;
        default:
          yield SiteTitle;
          break;
      }
    }
  }
};
