import { SiteImage } from './site-settings.js';
import { LinksImage } from './links-image.js';
import { ArticleImage } from './article.js';

export const MetaImage = async function* ({ error, router, routes }) {
  if (error) {
    yield SiteImage;
  } else {
    for await (const { route } of router) {
      switch (route) {
        case routes.article:
          yield ArticleImage;
          break;
        case routes.posts:
          yield LinksImage;
          break;
        default:
          yield SiteImage;
      }
    }
  }
};
