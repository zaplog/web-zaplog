import { pipe, map } from 'create-async-generator';

export const SearchValue = async function* ({ router, routes }) {
  for await (const { url, params, route } of router) {
    const action = url.searchParams.get('action');
    if (action === 'clear') {
      yield null;
    } else {
      if ([routes.article, routes.articleDelete].indexOf(route) !== -1) {
        yield pipe(
          ({ Article }) => Article,
          map(({ channel }) => `@${channel.name}`)
        );
      } else {
        yield url.searchParams.has('query')
          ? url.searchParams.get('query').replace(/"/g, '&quot;')
          : params.channel
          ? `@${params.channel}`
          : null;
      }
    }
  }
};
