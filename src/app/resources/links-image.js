import { pipe, map } from 'create-async-generator';
import { SiteImage } from './site-settings.js';

export const LinksImage = pipe(
  ({ Links }) => Links,
  map(({ links }) => {
    if (links.length) {
      return links[0].image;
    } else {
      return SiteImage;
    }
  })
);
