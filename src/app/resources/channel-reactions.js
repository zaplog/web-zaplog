export const ChannelReactions = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return async () => {
    for await (const { params } of router) {
      if (!storedValue) {
        storedValue = (async () =>
          (
            await api.getChannelReactions({
              channelId: params.channel,
            })
          ).json())();
      }
      return storedValue;
    }
  };
};
