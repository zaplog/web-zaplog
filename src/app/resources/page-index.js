import { pipe, map } from 'create-async-generator';

export const PageIndex = async function* ({ router }) {
  for await (const { url } of router) {
    yield url.searchParams.has('page')
      ? parseInt(url.searchParams.get('page'), 10)
      : 1;
  }
};

export const PaginationIndex = pipe(
  PageIndex,
  map((pageIndex) => ({
    previousPageIndex: pageIndex - 1,
    currentPageIndex: pageIndex,
    nextPageIndex: pageIndex + 1,
  }))
);
