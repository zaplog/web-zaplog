import { pipe, map, combineLatest } from 'create-async-generator';

export const ArticleReactionItems = pipe(
  combineLatest([
    ({ Article }) => Article,
    ({ LinkReactions }) => LinkReactions,
  ]),
  map(([{ link }, reactions]) =>
    reactions.map(({ xtext, id, ...rest }) => ({
      description: xtext,
      linkid: link.id,
      title: link.title,
      id,
      channelLink: true,
      enableVote: true,
      ...rest,
    }))
  )
);
