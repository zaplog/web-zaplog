import { pipe, map } from 'create-async-generator';
import { Token } from './token.js';

export const Authenticated = pipe(
  Token,
  map((token) => Boolean(token))
);
