export const VotedLinkId = ({ storage }) => {
  let storedValue = null;
  return async function* () {
    if (!storedValue) {
      const value = storage.getItem('votedLinkId');
      if (value) {
        storage.removeItem('votedLinkId');
      }
      storedValue = value ? parseInt(value, 10) : null;
    }
    yield storedValue;
  };
};
