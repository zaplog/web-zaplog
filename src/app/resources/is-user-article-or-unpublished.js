import { pipe, map, combineLatest } from 'create-async-generator';
import { IsUserArticle } from './is-user-article.js';

export const IsUserArticleOrUnpublished = pipe(
  ({ Article }) => combineLatest([Article, IsUserArticle]),
  map(([{ link }, isUserArticle]) => isUserArticle || !link.published)
);
