import { map, pipe } from 'create-async-generator';

export const RetryFormId = ({ loginAndRetryForm }) =>
  pipe(
    loginAndRetryForm,
    map(({ formId }) => formId)
  );
