import { pipe, map, all } from 'create-async-generator';
import { FormData } from '../../lib/forms/form-data.js';
import { Forms } from '../forms.js';
import {
  loginAndRetryFormFields,
  loginAndRetryStatus,
} from '../effects/login-and-retry-form.js';

export const CurrentArticleValues = async function* ({ router, error }) {
  if (error) {
    return {
      id: null,
      markdown: null,
      copyright: '',
      tags: [],
      membersonly: 0,
    };
  }
  for await (const { params } of router) {
    if (params?.id) {
      yield pipe(
        ({ Article }) => Article,
        map(({ link, tags }) => ({
          id: link.id,
          markdown: link.markdown,
          copyright: link.copyright,
          tags: tags.map(({ tag }) => tag),
          membersonly: link.membersonly,
        }))
      );
    } else {
      yield {
        id: null,
        markdown: null,
        copyright: '',
        tags: [],
        membersonly: 0,
      };
    }
  }
};

export const NewArticleValues = ({ createArticleForm }) =>
  pipe(
    all([
      pipe(
        createArticleForm,
        map(({ values }) => values)
      ),
      pipe(
        FormData(),
        map((formData) =>
          formData &&
          formData.get(loginAndRetryFormFields.STATUS) ===
            loginAndRetryStatus.RETRIED &&
          formData.get(loginAndRetryFormFields.FORM_ID) ===
            Forms.CreateArticleForm
            ? JSON.parse(formData.get(loginAndRetryFormFields.FORM_DATA))
            : null
        )
      ),
    ]),
    map(
      ([createArticle, retryCreateArticle]) =>
        createArticle ?? retryCreateArticle ?? CurrentArticleValues
    )
  );
