export const MembershipsLinks = async ({
  runAsync,
  router,
  api,
  configuration,
}) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return async () => {
    for await (const { url } of router) {
      if (!storedValue) {
        const clear = url.searchParams.get('action') === 'clear';
        storedValue = (async () =>
          (
            await api.getMembershipsLinks({
              offset: url.searchParams.has('page')
                ? (parseInt(url.searchParams.get('page'), 10) - 1) *
                  configuration.pageSize
                : 0,
              count: configuration.pageSize,
              search: clear ? null : url.searchParams.get('query'),
            })
          ).json())();
      }
      return storedValue;
    }
  };
};
