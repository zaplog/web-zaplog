import { SiteDescription } from './site-settings.js';
import { ArticleDescription } from './article.js';

export const MetaDescription = async function* ({ error, router, routes }) {
  if (error) {
    yield SiteDescription;
  } else {
    for await (const { route } of router) {
      switch (route) {
        case routes.article:
          yield ArticleDescription;
          break;
        default:
          yield SiteDescription;
          break;
      }
    }
  }
};
