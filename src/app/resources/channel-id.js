export const ChannelId = async function* ({ storage, configuration }) {
  const value = storage.getItem(configuration.channelIdName);
  yield value ? parseInt(value, 10) : null;
};
