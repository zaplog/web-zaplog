import { pipe, map, combineLatest } from 'create-async-generator';
import { ChannelName } from './channel-name.js';

export const Concepts = pipe(
  ({ UnpublishedLinks }) => combineLatest([UnpublishedLinks, ChannelName]),
  map(([unpublishedLinks, myChannelName]) =>
    unpublishedLinks.map(({ channelname, ...rest }) => ({
      ...rest,
      channelname,
      own: channelname === myChannelName,
    }))
  )
);
