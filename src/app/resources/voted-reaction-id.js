export const VotedReactionId = ({ storage }) => {
  let storedValue = null;
  return async function* () {
    if (!storedValue) {
      const value = storage.getItem('votedReactionId');
      if (value) {
        storage.removeItem('votedReactionId');
      }
      storedValue = value ? parseInt(value, 10) : null;
    }
    yield storedValue;
  };
};
