import { pipe, map } from 'create-async-generator';
import { matchLocale } from '../../lib/match-locale.js';
import { SiteLanguage } from './site-settings.js';

export const UserLocale = ({ configuration, Intl, acceptLanguage }) =>
  pipe(
    SiteLanguage,
    map((defaultLanguage) => {
      const matchLocales = acceptLanguage
        .map((code) => {
          let locale;
          try {
            locale = new Intl.Locale(code).maximize();
          } catch (_error) {
            locale = null;
          }
          return locale;
        })
        .filter(Boolean);
      const availableLocales = configuration.availableLanguages.map(
        (language) => new Intl.Locale(language).maximize()
      );
      const userLocale = matchLocale({
        availableLocales,
        matchLocales,
        compareOn: ['language'],
        // compareOn: ['language', 'region', 'script'],
      });

      if (userLocale) {
        return userLocale;
      } else {
        return new Intl.Locale(defaultLanguage).maximize();
      }
    })
  );

export const UserLanguage = pipe(
  UserLocale,
  map(({ language }) => language)
);
