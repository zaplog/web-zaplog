import { map, pipe } from 'create-async-generator';

export const RetryFormData = ({ loginAndRetryForm }) =>
  pipe(
    loginAndRetryForm,
    map(({ formData }) => formData)
  );
