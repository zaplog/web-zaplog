import { pipe, map } from 'create-async-generator';

export const Statistics = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () => {
    if (!storedValue) {
      storedValue = (async () => (await api.getStatistics()).json())();
    }
    return storedValue;
  };
};

export const StatisticsNumchannels = pipe(
  ({ Statistics }) => Statistics,
  map(({ numchannels }) => numchannels)
);

export const StatisticsNumtags = pipe(
  ({ Statistics }) => Statistics,
  map(({ numtags }) => numtags)
);
