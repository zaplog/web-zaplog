import { pipe, map, combineLatest } from 'create-async-generator';
import { algorithms, languages } from '../models.js';
import { ChannelName } from './channel-name.js';

export const Channel = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () =>
    pipe(
      combineLatest([ChannelName, router]),
      map(([channelName, { params }]) => {
        const id = params.channel ?? channelName;
        if (id) {
          if (!storedValue) {
            storedValue = (async () =>
              (
                await api.getChannel({
                  id,
                })
              ).json())();
          }
          return storedValue;
        }
      })
    );
};

export const IsUserChannel = pipe(
  combineLatest([({ Channel }) => Channel, ChannelName]),
  map(([{ channel }, channelName]) => channel.name === channelName)
);

export const IsMyChannelPage = async function* ({ router, routes }) {
  for await (const { route } of router) {
    if (
      [
        routes.channelEdit,
        routes.channelMembers,
        routes.channelMemberships,
        routes.newArticle,
        routes.editArticle,
        routes.channel,
      ].indexOf(route) !== -1
    ) {
      yield IsUserChannel;
    } else {
      yield false;
    }
  }
};

// TODO: fix stupid name, it collides with the channel name cookie of the logged in user
export const ChannelResourceName = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.name)
);
export const IsAdminChannel = pipe(
  ({ Channel, configuration }) =>
    combineLatest([Channel, configuration.adminChannelId]),
  map(([{ channel }, adminChannelId]) => channel.id === adminChannelId)
);
export const ChannelTags = pipe(
  ({ Channel }) => Channel,
  map(({ tags }) => tags)
);
export const ChannelMemberships = pipe(
  ({ Channel }) => Channel,
  map(({ memberships }) => memberships)
);
export const ChannelHeader = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.header)
);
export const ChannelAvatar = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.avatar)
);
export const ChannelBitcoinaddress = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.bitcoinaddress)
);
export const ChannelAlgorithm = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.algorithm)
);
export const ChannelLanguage = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.language)
);
export const ChannelBio = pipe(
  ({ Channel }) => Channel,
  map(({ channel }) => channel.bio)
);

export const ChannelLanguages = pipe(
  ChannelLanguage,
  map((channelLanguage) =>
    Object.values(languages).map((language) => ({
      selected: channelLanguage === language,
      language,
    }))
  )
);

export const ChannelAlgorithms = pipe(
  ChannelAlgorithm,
  map((ChannelAlgorithm) =>
    Object.values(algorithms).map((algorithm) => ({
      selected: ChannelAlgorithm === algorithm,
      algorithm,
    }))
  )
);
