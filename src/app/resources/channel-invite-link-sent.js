export const ChannelInviteLinkSent = ({ storage }) => {
  let storedValue = null;
  return async function* () {
    if (!storedValue) {
      const value = storage.getItem('channelInviteLinkSent');
      if (value) {
        storage.removeItem('channelInviteLinkSent');
      }
      storedValue = value;
    }
    if (storedValue) {
      yield storedValue;
    }
  };
};
