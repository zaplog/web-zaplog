import { pipe, map } from 'create-async-generator';
import { toUTCDate } from '../components/i18n/to-utc-date.js';

export const Links = async ({ runAsync, router, api, configuration }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return async () => {
    for await (const { url } of router) {
      if (!storedValue) {
        const clear = url.searchParams.get('action') === 'clear';
        storedValue = (async () =>
          (
            await api.getLinks({
              offset: url.searchParams.has('page')
                ? (parseInt(url.searchParams.get('page'), 10) - 1) *
                  configuration.pageSize
                : 0,
              count: configuration.pageSize,
              search: clear ? null : url.searchParams.get('query'),
            })
          ).json())();
      }
      return storedValue;
    }
  };
};

export const LinksLastModified = pipe(
  ({ Links }) => Links,
  map(({ lastmodified }) => (lastmodified ? new Date(lastmodified) : null))
);

export const LinksLastModifiedIsoString = pipe(
  LinksLastModified,
  map((lastModified) =>
    lastModified ? toUTCDate(lastModified).toISOString() : null
  )
);

export const LinksItems = pipe(
  ({ Links }) => Links,
  map(({ links }) => links)
);

export const LinksTags = pipe(
  ({ Links }) => Links,
  map(({ tags }) => tags)
);

export const LinksChannels = pipe(
  ({ Links }) => Links,
  map(({ channels }) => channels)
);
