import { pipe, map, combineLatest } from 'create-async-generator';
import { ChannelLinks } from './channel-links.js';
import { IsUserChannel } from './channel.js';
import { PageIndex } from './page-index.js';

export const IsChannelEmpty = pipe(
  combineLatest([IsUserChannel, PageIndex, ChannelLinks]),
  map(
    ([isUserChannel, pageIndex, links]) =>
      isUserChannel && !links.length && pageIndex !== 1
  )
);
