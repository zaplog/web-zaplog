import { pipe, map } from 'create-async-generator';

export const Activities = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () => {
    if (!storedValue) {
      storedValue = (async () => (await api.getActivities()).json())();
    }
    return storedValue;
  };
};

export const ActivityLinks = pipe(
  ({ Activities }) => Activities,
  map((activities) => {
    const activityMap = new Map();
    activities
      .filter(({ linkid }) => linkid)
      .reduce((acc, activity) => {
        const activityTypes = acc.get(activity.linkid) ?? new Map();
        const currentActivities = activityTypes.get(activity.type) ?? [];
        if (
          !currentActivities.find(
            ({ channelid }) => channelid === activity.channelid
          )
        ) {
          activityTypes.set(activity.type, [...currentActivities, activity]);
          acc.set(activity.linkid, activityTypes);
        }
        return acc;
      }, activityMap)
      .forEach((activityTypes, linkid) => {
        activityMap.set(linkid, Array.from(activityTypes.values()));
      });
    return Array.from(activityMap.values());
  })
);
