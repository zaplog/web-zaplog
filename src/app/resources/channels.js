export const Channels = async ({ runAsync, router, api }) => {
  let storedValue = null;
  await runAsync(async () => {
    for await (const _activatedRoute of router) {
      storedValue = null;
    }
  });
  return () => {
    if (!storedValue) {
      storedValue = (async () => (await api.getChannels()).json())();
    }
    return storedValue;
  };
};
