import { ArticleCanonicalUrl } from './article.js';

export const CanonicalUrl = async function* ({
  error,
  location,
  router,
  routes,
}) {
  if (error) {
    yield location.href;
  } else {
    for await (const { route } of router) {
      switch (route) {
        case routes.article:
          yield ArticleCanonicalUrl;
          break;
        default:
          yield location.href;
          break;
      }
    }
  }
};
