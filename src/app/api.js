export const Api = ({ appFetch, configuration, URLSearchParams }) => ({
  getToken: ({ token }) => {
    return appFetch(`${configuration.apiUrl}/2factor/${token}`);
  },

  createSession: ({ email, markdown, template, subject }) => {
    const body = new URLSearchParams({
      email,
      template,
      subject,
    });
    if (markdown) {
      body.append('article_markdown', markdown);
    }
    return appFetch(`${configuration.apiUrl}/sessions`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body,
    });
  },

  deleteSession: () => {
    return appFetch(`${configuration.apiUrl}/sessions`, {
      method: 'DELETE',
    });
  },

  getChannels: () => {
    return appFetch(`${configuration.apiUrl}/channels`);
  },

  getChannel: ({ id }) => {
    return appFetch(`${configuration.apiUrl}/channels/id/${id}`);
  },

  updateChannel: ({
    name,
    avatar,
    bio,
    language,
    algorithm,
    bitcoinaddress,
  }) => {
    const params = new URLSearchParams({
      name,
      bio,
    });
    if (avatar) {
      params.append('avatar', avatar);
    }

    if (language) {
      params.append('language', language);
    }
    if (algorithm) {
      params.append('algorithm', algorithm);
    }
    if (bitcoinaddress) {
      params.append('bitcoinaddress', bitcoinaddress);
    }
    return appFetch(`${configuration.apiUrl}/channels`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: params,
    });
  },

  getFrontpage: () => {
    return appFetch(`${configuration.apiUrl}/frontpage`);
  },

  createArticle: ({ id, markdown, tags, membersonly }) => {
    const params = new URLSearchParams({
      markdown,
      membersonly,
      ...(id ? { id } : {}),
    });
    if (tags) {
      tags.forEach((tag, i) => {
        params.append(`tags[${i}]`, tag);
      });
    }
    return appFetch(`${configuration.apiUrl}/links`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: params,
    });
  },

  publishArticle: ({ id }) => {
    return appFetch(`${configuration.apiUrl}/links/id/${id}`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    });
  },

  deleteArticle: ({ id }) => {
    return appFetch(`${configuration.apiUrl}/links/id/${id}`, {
      method: 'DELETE',
    });
  },

  getLink: ({ id, referrer }) => {
    return appFetch(
      `${configuration.apiUrl}/links/id/${id}${
        referrer ? `?http_referer=${encodeURIComponent(referrer)}` : ''
      }`
    );
  },

  getLinks: ({ offset, count, search } = {}) => {
    return appFetch(
      `${configuration.apiUrl}/archivepage?offset=${offset ?? 0}&count=${
        count ?? configuration.pageSize
      }${search ? `&search=${encodeURIComponent(search)}` : ''}`
    );
  },

  getUnpublishedLinks: () => {
    return appFetch(`${configuration.apiUrl}/links/unpublished`);
  },

  getMembershipsLinks: ({ offset, count } = {}) => {
    return appFetch(
      `${configuration.apiUrl}/links/memberships?offset=${offset ?? 0}&count=${
        count ?? configuration.pageSize
      }`
    );
  },

  getChannelLinks: ({ channel, offset, count }) => {
    return appFetch(
      `${configuration.apiUrl}/links/channel/${channel}?offset=${
        offset ?? 0
      }&count=${count ?? configuration.pageSize}`
    );
  },

  vote: ({ linkId }) => {
    return appFetch(`${configuration.apiUrl}/votes/link/${linkId}`, {
      method: 'POST',
    });
  },

  reactionVote: ({ reactionId }) => {
    return appFetch(
      `${configuration.apiUrl}/reactionvotes/reaction/${reactionId}`,
      {
        method: 'POST',
      }
    );
  },

  getTags: () => {
    return appFetch(`${configuration.apiUrl}/index`);
  },

  getActivities: () => {
    return appFetch(`${configuration.apiUrl}/activities`);
  },

  getStatistics: () => {
    return appFetch(`${configuration.apiUrl}/statistics`);
  },

  getLinkReactions: ({ linkId }) => {
    return appFetch(`${configuration.apiUrl}/reactions/link/${linkId}`);
  },

  getChannelReactions: ({ channelId }) => {
    return appFetch(`${configuration.apiUrl}/reactions/channel/${channelId}`);
  },

  getUrlmetadata: ({ url }) => {
    return appFetch(
      `${configuration.apiUrl}/urlmetadata?urlencoded=${encodeURIComponent(
        url
      )}`
    );
  },

  comment: ({ linkId, markdown }, preview = false) => {
    return appFetch(
      `${configuration.apiUrl}/reactions/link/${linkId}${
        preview ? '?preview=1' : ''
      }`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
          markdown,
        }),
      }
    );
  },

  config: () => {
    return appFetch(`${configuration.apiUrl}/config`);
  },

  getChannelMembers: () => {
    return appFetch(`${configuration.apiUrl}/channels/members`);
  },

  addChannelMember: ({ email, template, subject }) => {
    return appFetch(`${configuration.apiUrl}/channels/members`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      body: new URLSearchParams({
        email,
        template,
        subject,
      }),
    });
  },

  deleteChannelMember: ({ channelName }) => {
    return appFetch(`${configuration.apiUrl}/channels/members/${channelName}`, {
      method: 'DELETE',
    });
  },

  deleteChannelMembership: ({ channelName }) => {
    return appFetch(
      `${configuration.apiUrl}/channels/memberships/${channelName}`,
      {
        method: 'DELETE',
      }
    );
  },

  transferConcept: ({ linkId, channelName }) => {
    return appFetch(
      `${configuration.apiUrl}/links/linkid/${linkId}/newchannelid/${channelName}`,
      {
        method: 'PATCH',
      }
    );
  },
});
