import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const PublishArticleForm = ({ error, api, router, routes }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.PublishArticleForm),
        map(async (formData) => {
          try {
            const id = formData.get('id');
            const title = formData.get('title');
            const channel = formData.get('channelname');
            await api.publishArticle({
              id: parseInt(id, 10),
            });
            router.navigate({
              route: routes.article,
              params: {
                id,
                title,
                channel,
              },
              replaceUrl: true,
            });
          } catch (error) {
            return { error };
          }
        })
      );
