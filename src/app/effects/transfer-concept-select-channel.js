import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const TransferConceptSelectChannel = ({ error }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.TransferConceptSelectChannel),
        map((formData) => formData.get('linkId'))
      );
