import { pipe, map } from 'create-async-generator';
import { FormattedArticleTitle } from '../resources/article.js';

export const ArticleTitle = async ({ error, router, routes }) => {
  if (error) {
    return;
  }
  for await (const { route, params } of router) {
    switch (route) {
      case routes.rawArticle:
      case routes.article:
        return pipe(
          FormattedArticleTitle,
          map((formattedArticleTitle) => {
            if (formattedArticleTitle !== params.title) {
              router.navigate({
                route: routes.article,
                params: {
                  ...params,
                  title: formattedArticleTitle,
                },
                replaceUrl: true,
              });
            }
          })
        );
    }
  }
};
