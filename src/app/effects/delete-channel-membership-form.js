import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const DeleteChannelMembershipForm = ({ error, api, router, routes }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.DeleteChannelMembershipForm),
        map(async (formData) => {
          await api.deleteChannelMembership({
            channelName: formData.get('channelMembership'),
          });
          router.navigate({
            route: routes.channelMembers,
            params: {
              channel: formData.get('channelName'),
            },
            replaceUrl: true,
          });
        })
      );
