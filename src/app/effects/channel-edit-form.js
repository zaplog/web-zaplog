import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const ChannelEditForm = ({
  error,
  api,
  storage,
  configuration,
  router,
  routes,
}) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.ChannelEditForm),
        map(async (formData) => {
          const name = formData.get('name');
          try {
            await api.updateChannel({
              name,
              avatar: formData.get('avatar'),
              bio: formData.get('bio'),
              header: formData.get('header'),
              language: formData.get('language'),
              bitcoinaddress: formData.get('bitcoinaddress'),
              algorithm: formData.get('algorithm'),
            });
            storage.setItem(configuration.channelNameName, name);
            router.navigate({
              route: routes.channel,
              params: { channel: name },
              replaceUrl: true,
            });
          } catch (error) {
            return { error };
          }
        })
      );
