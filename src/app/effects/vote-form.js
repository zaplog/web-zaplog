import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const VoteForm = ({ error, api, storage, URL, location, router }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.VoteForm),
        map(async (formData) => {
          const id = formData.get('id');
          await api.vote({ linkId: id });
          const linkId = parseInt(id, 10);
          storage.setItem('votedLinkId', linkId);
          const url = new URL(location.href);
          router.navigateByUrl({ url, replaceUrl: true });
        })
      );
