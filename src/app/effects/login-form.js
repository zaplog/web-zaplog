import { pipe, map } from 'create-async-generator';
import { FormData } from '../../lib/forms/form-data.js';
import { Login } from '../actions/login.js';
import { Forms } from '../forms.js';

export const twofaStorageKey = 'twofaLinkSent';
export const twofaLinkSent = 'twofaLinkSent';
export const twofaLinkNotSent = 'twofaLinkNotSent';

export const LoginForm = ({ storage, error, router, routes }) => {
  const twofaLinkStatus = storage.getItem(twofaStorageKey) ?? twofaLinkNotSent;
  if (twofaLinkStatus === twofaLinkSent) {
    storage.removeItem(twofaStorageKey);
  }
  return error
    ? twofaLinkStatus
    : pipe(
        FormData(Forms.LoginForm),
        map((formData) => {
          return formData
            ? pipe(
                Login({ email: formData.get('email') }),
                map(() => {
                  if (formData.get('replaceByGet') !== 'skip') {
                    storage.setItem(twofaStorageKey, twofaLinkSent);
                    router.navigate({
                      route: routes.login,
                      replaceUrl: true,
                    });
                  }
                  return twofaLinkSent;
                })
              )
            : twofaLinkStatus;
        })
      );
};
