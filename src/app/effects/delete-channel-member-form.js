import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const DeleteChannelMemberForm = ({ api, router, routes, error }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.DeleteChannelMemberForm),
        map(async (formData) => {
          await api.deleteChannelMember({
            channelName: formData.get('channelMember'),
          });
          router.navigate({
            route: routes.channelMembers,
            params: {
              channel: formData.get('channelName'),
            },
            replaceUrl: true,
          });
        })
      );
