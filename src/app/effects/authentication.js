import { ResetAuthentication } from '../actions/reset-authentication.js';

export const Authentication = async function* Authentication({ error }) {
  if (error) {
    if (error?.status === 401) {
      yield ResetAuthentication();
    }

    return null;
  }
};
