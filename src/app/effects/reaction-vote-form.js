import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const ReactionVoteForm = ({
  error,
  api,
  storage,
  URL,
  router,
  location,
}) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.ReactionVoteForm),
        map(async (formData) => {
          const id = formData.get('id');
          await api.reactionVote({ reactionId: id });
          const reactionId = parseInt(id, 10);
          storage.setItem('votedReactionId', reactionId);
          const url = new URL(location.href);
          url.hash = `reaction-${id}`;
          router.navigateByUrl({ url, replaceUrl: true });
        })
      );
