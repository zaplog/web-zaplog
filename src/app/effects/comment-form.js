import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const CommentForm = ({ error, api, URL, location, router }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.CommentForm),
        map(async (formData) => {
          const action = formData.get('action');
          const comment = formData.get('comment');
          const linkId = formData.get('link_id');
          try {
            if (action === 'preview') {
              const response = await api.comment(
                { markdown: comment, linkId },
                true
              );
              const { markdown, xtext } = await response.json();
              return { markdown, preview: xtext };
            } else {
              const response = await api.comment({
                markdown: comment,
                linkId,
              });
              const url = new URL(location.href);
              url.hash = `reaction-${await response.json()}`;
              router.navigateByUrl({ url, replaceUrl: true });
            }
          } catch (error) {
            return { error };
          }
        })
      );
