import { Twofa } from '../actions/twofa.js';

export const TwofaRoute = async function* ({
  error,
  router,
  routes,
  configuration,
}) {
  if (error) {
    return null;
  }
  for await (const { route, params } of router) {
    switch (route) {
      case routes.twofa:
        yield Twofa({ token: params.token });
        if (configuration.community) {
          router.navigate({
            route: routes.discussion,
            replaceUrl: true,
          });
        } else {
          router.navigate({ route: routes.home, replaceUrl: true });
        }
        break;
    }
  }
};
