import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';
import { ChannelName } from '../resources/channel-name.js';

export const DeleteArticleForm = ({ error, api, router, routes }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.DeleteArticleForm),
        map(async (formData) => {
          const id = formData.get('id');
          await api.deleteArticle({
            id,
          });
          return pipe(
            ChannelName,
            map((channelName) => {
              router.navigate({
                route: routes.channel,
                params: {
                  channel: channelName,
                },
                replaceUrl: true,
              });
            })
          );
        })
      );
