import { pipe, map, combineLatest } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';
import { SiteTitle } from '../resources/site-settings.js';
import { LoginEmailTemplate } from '../components/login-email-template/login-email-template.js';
import { asyncToString } from 'async-to-string';

export const AddChannelMemberForm = ({ error, translate, routes, ...rest }) =>
  error
    ? null
    : pipe(
        combineLatest([FormSubmit(Forms.AddChannelMemberForm), SiteTitle]),
        map(([formData, siteTitle]) => {
          const subject = translate(
            'emailSubjects.CONFIRM_CHANNEL_MEMBERSHIP',
            {
              siteTitle: siteTitle,
            }
          );
          const template = LoginEmailTemplate({
            subject,
            loginurl: `${routes.twofa.getHref({
              token: '',
            })}`,
          });
          return async (app) => {
            try {
              await app.api.addChannelMember({
                email: formData.get('email'),
                subject: (
                  await asyncToString(subject, {
                    args: [{ error, translate, routes, ...rest }],
                  })
                ).toString(),
                template: (
                  await asyncToString(template, {
                    args: [{ error, translate, routes, ...rest }],
                  })
                ).toString(),
              });
            } catch (error) {
              return { error };
            }
            app.storage.setItem('channelInviteLinkSent', 1);
            app.router.navigate({
              route: app.routes.channelMembers,
              params: { channel: formData.get('channelName') },
              replaceUrl: true,
            });
          };
        })
      );
