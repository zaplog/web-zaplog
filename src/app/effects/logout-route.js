import { pipe, map } from 'create-async-generator';
import { Authenticated } from '../resources/authenticated.js';
import { Logout } from '../actions/logout.js';

export const LogoutRoute = async function* ({ error, router, routes }) {
  if (error) {
    return null;
  }
  for await (const { route } of router) {
    switch (route) {
      case routes.logout:
        yield pipe(
          Authenticated,
          map((isAuthenticated) => {
            if (isAuthenticated) {
              return Logout();
            }
          }),
          map(() => router.navigate({ route: routes.home, replaceUrl: true }))
        );
        break;
    }
  }
};
