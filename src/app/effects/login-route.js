import { pipe, map } from 'create-async-generator';
import { Authenticated } from '../resources/authenticated.js';

export const LoginRoute = async function* ({ error, router, routes }) {
  if (error) {
    return null;
  }
  for await (const { route } of router) {
    switch (route) {
      case routes.login:
        yield pipe(
          Authenticated,
          map((isAuthenticated) => {
            if (isAuthenticated) {
              router.navigate({ route: routes.home, replaceUrl: true });
            }
          })
        );
        break;
    }
  }
};
