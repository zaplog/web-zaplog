import { pipe, map } from 'create-async-generator';
import { FormData } from '../../lib/forms/form-data.js';

export const loginAndRetryStatus = {
  RETRIED: 'retried',
};

export const loginAndRetryFormFields = {
  STATUS: 'loginAndRetryStatus',
  FORM_ID: 'loginAndRetryFormId',
  FORM_DATA: 'loginAndRetryFormData',
};

export const LoginAndRetryForm = ({ error }) =>
  pipe(
    FormData(),
    map(async function* (formData) {
      let retryFormId;
      let retryFormData;
      if (
        formData &&
        formData.get(loginAndRetryFormFields.STATUS) ===
          loginAndRetryStatus.RETRIED
      ) {
        retryFormId = formData.get(loginAndRetryFormFields.FORM_ID);
        retryFormData = JSON.parse(
          formData.get(loginAndRetryFormFields.FORM_DATA)
        );
      } else if (error?.status === 401 && error?.request.method === 'POST') {
        retryFormId = formData.get('form_id');
        retryFormData = Array.from(formData.entries())
          .filter(([key]) => key !== 'form_id')
          .reduce(
            (acc, [key, value]) => ({
              ...acc,
              [key]: value,
            }),
            {}
          );
      }

      if (retryFormId) {
        yield {
          formId: retryFormId,
          formData: retryFormData,
        };
      }
    })
  );
