import { pipe, map } from 'create-async-generator';
import { FormData } from '../../lib/forms/form-data.js';
import { Login } from '../actions/login.js';
import { ChannelName } from '../resources/channel-name.js';
import { Forms } from '../forms.js';
import { twofaLinkSent, twofaStorageKey } from './login-form.js';

export const CreateArticleForm = ({ storage, error, api, router, routes }) =>
  pipe(
    FormData(Forms.CreateArticleForm),
    map(async (formData) => {
      if (!formData) {
        return {
          values: null,
        };
      }
      const email = formData.get('email');
      const action = formData.get('action');
      const id = formData.get('id');
      const markdown = formData.get('markdown');
      const copyrightCheck = formData.get('copyrightCheck');
      const copyright = formData.get('copyright');
      const membersonly = formData.get('membersonly');
      const tags = formData.has('tags') ? formData.get('tags').split(' ') : [];
      if (error) {
        return {
          values: {
            id,
            markdown,
            action,
            copyright,
            tags,
            copyrightCheck,
            membersonly,
          },
        };
      }
      if (email) {
        return pipe(
          Login({
            email,
            markdown: action === 'existing' ? null : markdown,
          }),
          map(() => {
            storage.setItem(twofaStorageKey, twofaLinkSent);
            router.navigate({
              route: routes.login,
              replaceUrl: true,
            });
          })
        );
      } else if (markdown?.length > 500 && !copyrightCheck) {
        return {
          values: {
            id,
            markdown,
            action,
            copyright,
            tags,
            copyrightCheck: true,
            membersonly,
          },
        };
      } else {
        try {
          const response = await api.createArticle({
            id: id ? parseInt(id, 10) : null,
            markdown,
            copyright,
            tags,
            membersonly: membersonly === 'on' ? 1 : 0,
          });
          const link = await response.json();
          return pipe(
            ChannelName,
            map((channel) => {
              router.navigate({
                route: routes.article,
                params: {
                  id: link.id.toString(),
                  title: link.title,
                  channel,
                },
                replaceUrl: true,
              });
            })
          );
        } catch (error) {
          if (
            error?.status === 401 ||
            !error?.status ||
            error.status.toString()[0] !== '4'
          ) {
            throw error;
          }
          return {
            error,
            values: {
              id,
              markdown,
              action,
              copyright,
              tags,
              copyrightCheck: false,
              membersonly,
            },
          };
        }
      }
    })
  );
