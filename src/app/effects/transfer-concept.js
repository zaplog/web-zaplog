import { pipe, map } from 'create-async-generator';
import { FormSubmit } from '../../lib/forms/form-submit.js';
import { Forms } from '../forms.js';

export const TransferConcept = ({ error, api, router, routes }) =>
  error
    ? null
    : pipe(
        FormSubmit(Forms.TransferConcept),
        map(async (formData) => {
          await api.transferConcept({
            linkId: formData.get('linkId'),
            channelName: formData.get('channelName'),
          });
          for await (const { params } of router) {
            router.navigate({
              route: routes.channel,
              params: {
                channel: params.channel,
              },
              replaceUrl: true,
            });
            return;
          }
        })
      );
