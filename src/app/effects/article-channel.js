import { pipe, map } from 'create-async-generator';
import { ArticleAuthor } from '../resources/article.js';

export const ArticleChannel = async ({ error, router, routes }) => {
  if (error) {
    return;
  }
  for await (const { route, params } of router) {
    switch (route) {
      case routes.rawArticle:
      case routes.article:
        return pipe(
          ArticleAuthor,
          map((articleAuthor) => {
            if (params.channel && params.channel !== articleAuthor) {
              router.navigate({
                route: routes.article,
                params: {
                  ...params,
                  channel: articleAuthor,
                },
                permanent: 301,
              });
            }
          })
        );
    }
  }
};
