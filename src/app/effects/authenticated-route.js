import { pipe, map, combineLatest } from 'create-async-generator';
import { Authenticated } from '../resources/authenticated.js';
import { IsAuthenticatedRoute } from '../resources/is-authenticated-route.js';

export const AuthenticatedRoute = ({ error, router, routes, getFormData }) => {
  if (error) {
    return null;
  }
  return pipe(
    combineLatest([Authenticated, IsAuthenticatedRoute, getFormData()]),
    map(([isAuthenticated, isAuthenticatedRoute, formData]) => {
      if (!isAuthenticated && isAuthenticatedRoute && !formData) {
        router.navigate({
          route: routes.login,
          skipLocationChange: true,
        });
        return true;
      }
    })
  );
};
