import { pipe, map } from 'create-async-generator';
import { Authenticated } from '../resources/authenticated.js';
import { ChannelName } from '../resources/channel-name.js';

export const HomeRoute = async function* ({
  error,
  router,
  routes,
  configuration,
}) {
  if (error) {
    return null;
  }
  for await (const { route } of router) {
    switch (route) {
      case routes.home:
        if (!configuration.community) {
          yield pipe(
            Authenticated,
            map((isAuthenticated) => {
              if (isAuthenticated) {
                return pipe(
                  ChannelName,
                  map((channelName) => {
                    router.navigate({
                      route: routes.channel,
                      params: { channel: channelName },
                      replaceUrl: true,
                    });
                  })
                );
              } else {
                router.navigate({
                  route: routes.login,
                  skipLocationChange: true,
                });
              }
            })
          );
        }
        break;
    }
  }
};
