import { map, pipe } from 'create-async-generator';
import { TranslateRaw } from 'simpler-translate';
import { Module } from '../lib/module.js';
import { Routing } from './routing/routing.js';
import { Api } from './api.js';
import { Fetch } from './fetch.js';
import { SiteSettings } from './resources/site-settings.js';
import { baseConfiguration } from './base-configuration.js';
import { ArticleTitle } from './effects/article-title.js';
import { ArticleChannel } from './effects/article-channel.js';
import { Authentication } from './effects/authentication.js';
import { VoteForm } from './effects/vote-form.js';
import { CreateArticleForm } from './effects/create-article-form.js';
import { ReactionVoteForm } from './effects/reaction-vote-form.js';
import { LoginForm } from './effects/login-form.js';
import { CommentForm } from './effects/comment-form.js';
import { PublishArticleForm } from './effects/publish-article-form.js';
import { DeleteArticleForm } from './effects/delete-article-form.js';
import { ChannelEditForm } from './effects/channel-edit-form.js';
import { AddChannelMemberForm } from './effects/add-channel-member-form.js';
import { DeleteChannelMemberForm } from './effects/delete-channel-member-form.js';
import { Activities } from './resources/activities.js';
import { Article } from './resources/article.js';
import { ChannelInviteLinkSent } from './resources/channel-invite-link-sent.js';
import { ChannelLinksResponse } from './resources/channel-links.js';
import { ChannelMembers } from './resources/channel-members.js';
import { ChannelReactions } from './resources/channel-reactions.js';
import { Channel } from './resources/channel.js';
import { Channels } from './resources/channels.js';
import { FrontpageResponse } from './resources/frontpage.js';
import { Links } from './resources/links.js';
import { LinkReactions } from './resources/link-reactions.js';
import { Statistics } from './resources/statistics.js';
import { Tags } from './resources/tags.js';
import { UnpublishedLinks } from './resources/unpublished-links.js';
import { MembershipsLinks } from './resources/memberships-links.js';
import { VotedLinkId } from './resources/voted-link-id.js';
import { VotedReactionId } from './resources/voted-reaction-id.js';
import { StoredTwoFa } from './resources/stored-two-fa.js';
import { UserLanguage } from './resources/user-locale.js';
import { State } from '../lib/state.js';
import { DeleteChannelMembershipForm } from './effects/delete-channel-membership-form.js';
import { TransferConcept } from './effects/transfer-concept.js';
import { TransferConceptSelectChannel } from './effects/transfer-concept-select-channel.js';
import { HomeRoute } from './effects/home-route.js';
import { LoginRoute } from './effects/login-route.js';
import { LogoutRoute } from './effects/logout-route.js';
import { AuthenticatedRoute } from './effects/authenticated-route.js';
import { TwofaRoute } from './effects/twofa-route.js';
import { LoginAndRetryForm } from './effects/login-and-retry-form.js';

export const App = Module({
  configuration: ({ configuration }) => ({
    ...baseConfiguration,
    ...configuration,
  }),
  appFetch: Fetch,
  routes: Routing,
  api: Api,
  SiteSettings,
  translate: ({ html, translations }) => {
    const translate = TranslateRaw(translations);
    return (...args) =>
      pipe(
        UserLanguage,
        map(async (language) => html(...(await translate(language, ...args))))
      );
  },
  Activities,
  Article,
  ChannelInviteLinkSent,
  ChannelLinksResponse,
  ChannelMembers,
  ChannelReactions,
  Channel,
  Channels,
  FrontpageResponse,
  Links,
  LinkReactions,
  Statistics,
  Tags,
  UnpublishedLinks,
  MembershipsLinks,
  VotedLinkId,
  VotedReactionId,
  StoredTwoFa,
  loginAndRetryForm: State(LoginAndRetryForm),
  articleTitle: State(ArticleTitle),
  articleChannel: State(ArticleChannel),
  authentication: State(Authentication),
  homeRoute: State(HomeRoute),
  loginRoute: State(LoginRoute),
  logoutRoute: State(LogoutRoute),
  myChannelRoute: State(AuthenticatedRoute),
  twofaRoute: State(TwofaRoute),
  voteForm: State(VoteForm),
  reactionVoteForm: State(ReactionVoteForm),
  createArticleForm: State(CreateArticleForm),
  loginForm: State(LoginForm),
  commentForm: State(CommentForm),
  publishArticleForm: State(PublishArticleForm),
  deleteArticleForm: State(DeleteArticleForm),
  channelEditForm: State(ChannelEditForm),
  addChannelMemberForm: State(AddChannelMemberForm),
  deleteChannelMemberForm: State(DeleteChannelMemberForm),
  deleteChannelMembershipForm: State(DeleteChannelMembershipForm),
  transferConcept: State(TransferConcept),
  transferConceptSelectChannel: State(TransferConceptSelectChannel),
});
