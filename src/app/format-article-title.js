export const formatArticleTitle = (title) =>
  title.toLowerCase().replace(/[^a-z0-9-]+/g, '-');
