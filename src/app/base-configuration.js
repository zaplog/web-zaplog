export const baseConfiguration = {
  sessionName: 'X-Session-Token',
  channelHeaderName: 'XChannelId',
  channelIdName: 'ZaplogChannelId',
  channelNameName: 'ZaplogChannelName',
  userTwoFaName: 'ZaplogUserTwoFA',
  pageSize: 24,
  asideSize: 17,
};
