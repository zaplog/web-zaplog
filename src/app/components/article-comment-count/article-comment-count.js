import { RouterLink } from '../../../lib/router/router-link.js';
import { LinkReactionsLength } from '../../resources/link-reactions.js';

export const ArticleCommentCount = ({ html, translate }) => [
  RouterLink({
    href: '#comments',
    title: translate('article.DISPLAY_SOURCE'),
    classes: 'icon icon--s icon--comment icon--link',
  }),
  html`<span class="article-comment-count"> ${LinkReactionsLength} </span>`,
];
