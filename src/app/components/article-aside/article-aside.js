import { RouterLink } from '../../../lib/router/router-link.js';
import { ArticleRelated } from '../../resources/article.js';

export const ArticleAside = ({ html, decode }) => html`<ol
  class="article-aside"
>
  ${async function* ({ routes, ...rest }) {
    for await (const related of ArticleRelated({ routes, ...rest })) {
      yield related.map(
        ({ title, description, id, channelname }) =>
          html`<li class="article-aside__item">
            ${RouterLink(
              {
                classes: 'panel article-aside__related link link--plain',
                route: routes.article,
                params: { id, title, channel: channelname },
              },
              html`<h2 class="article-aside__title">${decode(title)}</h2>
                <p class="article-aside__description">${description}</p>`
            )}
          </li>`
      );
    }
  }}
</ol>`;
