import { pipe, map } from 'create-async-generator';
import { RouterLink } from '../../../lib/router/router-link.js';
import { PaginationIndex } from '../../resources/page-index.js';

export const Pagination = ({ html }) => html`<div class="pagination">
  ${pipe(
    PaginationIndex,
    map(({ previousPageIndex, currentPageIndex, nextPageIndex }) => [
      ({ html }) => html`<div class="pagination__item pagination__left">
        ${previousPageIndex > 0
          ? ({ router }) =>
              pipe(
                router,
                map(({ route, params }) =>
                  RouterLink(
                    {
                      route,
                      params,
                      searchParams: {
                        page: previousPageIndex,
                      },
                      searchParamsHandling: 'merge',
                      classes: 'pagination__paginate link link--nav',
                    },
                    ({ html, translate }) => html`<span
                        class="icon icon--previous"
                      ></span>
                      ${translate('PAGE')} ${previousPageIndex}`
                  )
                )
              )
          : null}
      </div>`,
      ({ html, translate }) => html`<div
        class="pagination__item pagination__center"
      >
        ${translate('PAGE')} ${currentPageIndex}
      </div>`,
      ({ html }) => html`<div class="pagination__item pagination__right">
        ${({ router }) =>
          pipe(
            router,
            map(({ route, params }) =>
              RouterLink(
                {
                  route,
                  params,
                  searchParams: {
                    page: nextPageIndex,
                  },
                  searchParamsHandling: 'merge',
                  classes: 'pagination__paginate link link--nav',
                },
                ({ html, translate }) => html`${translate('PAGE')}
                  ${nextPageIndex} <span class="icon icon--next"></span>`
              )
            )
          )}
      </div>`,
    ])
  )}
</div>`;
