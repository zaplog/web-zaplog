import { pipe, map } from 'create-async-generator';
import { RouterLink } from '../../../lib/router/router-link.js';

export const Chips =
  ({ itemprop, items, moreLink, moreAmount, className }) =>
  ({ html }) =>
    html`<ol
      class="chips ${className}"
      ${itemprop ? html`itemprop="${itemprop}"` : null}
    >
      ${pipe(
        items,
        map((itemsList) =>
          itemsList.map(
            ({ route, params, text, className }) =>
              html`<li>
                ${RouterLink(
                  {
                    route,
                    params,
                    classes: `link${
                      className ? ` ${className}` : ' chips__item'
                    }`,
                  },
                  text
                )}
              </li>`
          )
        )
      )}
      ${moreLink
        ? html`<li>
            ${RouterLink(
              { classes: 'chips__more button button--xs', href: moreLink },
              ({ translate }) => {
                if (moreAmount) {
                  return pipe(
                    moreAmount,
                    map((amount) => translate('MORE_AMOUNT', { amount }))
                  );
                } else {
                  return translate('MORE');
                }
              }
            )}
          </li>`
        : null}
    </ol>`;
