import { map, pipe } from 'create-async-generator';
import { NotFound } from '../not-found/not-found.js';
import { Login } from '../login/login.js';
import { Frontpage } from '../frontpage/frontpage.js';
import { Posts } from '../posts/posts.js';
import { ArticlePage } from '../article-page/article-page.js';
import { TagIndex } from '../tag-index/tag-index.js';
import { ChannelIndex } from '../channel-index/channel-index.js';
import { ChannelDetailPage } from '../channel-detail-page/channel-detail-page.js';
import { ChannelEdit } from '../channel-edit/channel-edit.js';
import { ChannelMembers } from '../channel-members/channel-members.js';
import { ChannelMembershipsPage } from '../channel-memberships/channel-memberships.js';
import { Discussion } from '../discussion/discussion.js';
import { NewArticle } from '../new-article/new-article.js';
import { ErrorPage } from '../error-page/error-page.js';
import { LoginAndRetryFormDialog } from '../login-and-retry-form-dialog/login-and-retry-form-dialog.js';

export const Body = (_props, ...children) => [
  ({ loginAndRetryForm }) =>
    pipe(
      loginAndRetryForm,
      map(() => LoginAndRetryFormDialog)
    ),
  async function* ({ router, routes, error }) {
    if (error && !(error?.status === 401 && error?.request.method === 'POST')) {
      switch (error?.status) {
        case 401:
          yield Login;
          break;
        case 404:
          yield NotFound;
          break;
        default:
          yield ErrorPage;
          break;
      }
    } else {
      for await (const { route } of router) {
        switch (route) {
          case routes.login:
            yield Login;
            break;
          case routes.home:
            yield Frontpage;
            break;
          case routes.posts:
            yield Posts;
            break;
          case routes.articleDelete:
          case routes.article:
            yield ArticlePage;
            break;
          case routes.index:
            yield TagIndex;
            break;
          case routes.channelIndex:
            yield ChannelIndex;
            break;
          case routes.channel:
            yield ChannelDetailPage;
            break;
          case routes.channelEdit:
            yield ChannelEdit;
            break;
          case routes.channelMembers:
            yield ChannelMembers;
            break;
          case routes.channelMemberships:
            yield ChannelMembershipsPage;
            break;
          case routes.discussion:
            yield Discussion;
            break;
          case routes.newArticle:
          case routes.editArticle:
            yield NewArticle;
            break;
          case null:
            yield NotFound;
            break;
        }
      }
    }
  },
  ...children,
];
