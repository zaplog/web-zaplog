import { Form } from '../../../lib/forms/form.js';
import { Reaction } from '../reaction/reaction.js';
import getCurrentDateModule from '../i18n/get-current-date.js';
import { ChannelName } from '../../resources/channel-name.js';
import { Forms } from '../../forms.js';
import { ArticleId, IsArticlePublished } from '../../resources/article.js';

export const ArticleComment = ({
  html,
  createElement,
  translate,
  Textarea,
}) => [
  html`<div class="article-comment__instructions">
    ${translate('article.FORMAT_INSTRUCTIONS')}
    <a
      target="_blank"
      href="${translate('article.MARKDOWN_CHEATSHEET')}"
      class="link"
      >${translate('article.MARKDOWN_NOTATION')}</a
    >
  </div>`,
  Form(
    {
      name: Forms.CommentForm,
      class: 'article-comment__add',
    },
    html`<fieldset class="fieldset">
      ${Textarea(
        {
          class: 'fieldset__full-width form-field form-field--textarea',
          id: 'commentField',
          name: 'comment',
          placeholder: translate('article.COMMENT'),
          rows: '10',
          required: true,
        },
        async function* ({ commentForm }) {
          for await (const { markdown } of commentForm) {
            yield markdown;
          }
        }
      )}
    </fieldset>`,
    async function* (app) {
      for await (const isArticlePublished of IsArticlePublished(app)) {
        if (!isArticlePublished) {
          yield html`<div class="concept-feedback">
            <input
              type="checkbox"
              class="form-field form-field--checkbox"
              id="conceptFeedbackField"
              required
            />
            <label for="conceptFeedbackField" class="form-field--checkbox-label"
              >${translate('newArticle.CONCEPT_FEEDBACK_CHECK')}</label
            >
          </div>`;
        }
      }
    },
    html`<input type="hidden" name="link_id" value="${ArticleId}" />`,
    async function* ({ commentForm }) {
      for await (const { preview } of commentForm) {
        yield html`<div class="panel">
          ${Reaction({
            description: preview,
            createdatetime: getCurrentDateModule.getCurrentDate().toString(),
            channelLink: true,
            name: ChannelName,
          })}
        </div>`;
      }
    },
    createElement(
      'div',
      { class: 'article-comment__buttons button-row' },
      html`<button class="button" type="submit" name="action" value="add">
        ${translate('article.ADD_COMMENT')}
      </button>`,
      html`<button
        type="submit"
        name="action"
        value="preview"
        class="button button--secondary"
      >
        ${translate('article.PREVIEW_COMMENT')}
      </button>`
    )
  ),
];
