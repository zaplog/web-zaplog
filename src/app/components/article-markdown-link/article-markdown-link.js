import { RouterLink } from '../../../lib/router/router-link.js';
import {
  ArticleAuthor,
  ArticleId,
  ArticleTitle,
} from '../../resources/article.js';

export const ArticleMarkdownLink = ({ translate, routes }) =>
  RouterLink({
    route: routes.rawArticle,
    params: {
      id: ArticleId,
      title: ArticleTitle,
      channel: ArticleAuthor,
    },
    target: '_blank',
    title: translate('article.DISPLAY_SOURCE'),
    classes: 'icon icon--markdown icon--link',
  });
