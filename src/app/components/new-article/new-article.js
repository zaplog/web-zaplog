import { pipe, map } from 'create-async-generator';
import { MarkdownTextarea } from '../markdown-textarea/markdown-textarea.js';
import { NewArticleValues } from '../../resources/new-article-values.js';
import { Forms } from '../../forms.js';
import { Form } from '../../../lib/forms/form.js';
import { Dialog } from '../dialog/dialog.js';
import { Error } from '../error/error.js';
import { SiteAvatar, SiteTitle } from '../../resources/site-settings.js';

export const NewArticle = ({ html, translate, createElement }) =>
  Form(
    { name: Forms.CreateArticleForm },
    html`<zaplog-markdown-textarea class="base new-article">
      <header class="new-article__header">
        <div class="new-article__header-wrapper">
          <div class="new-article__header-image-wrapper">
            <img
              src="${SiteAvatar}"
              class="new-article__header-image"
              alt="${SiteTitle}"
              title="${SiteTitle}"
            />
          </div>
          <div class="new-article__header-column">
            <div class="new-article__header-row">
              <div class="new-article__slot"></div>
              <div class="button-row">
                ${createElement(
                  'div',
                  { class: 'create-article__submit' },
                  html`<button
                    class="button button--s"
                    type="submit"
                    name="action"
                    value="create"
                  >
                    ${pipe(
                      NewArticleValues,
                      map(({ id, published }) =>
                        id
                          ? ({ translate }) =>
                              translate(
                                published
                                  ? 'article.SAVE'
                                  : 'newArticle.SAVE_CONCEPT'
                              )
                          : ({ translate }) =>
                              translate('newArticle.PREVIEW_CONCEPT')
                      )
                    )}
                  </button>`
                )}
                <fieldset
                  class="new-article__field new-article__membersonly button button--s button--secondary"
                >
                  <input
                    type="checkbox"
                    name="membersonly"
                    id="membersonlyField"
                    class="button__form-field form-field form-field--checkbox"
                    ${pipe(
                      NewArticleValues,
                      map(({ membersonly }) => (membersonly ? 'checked' : null))
                    )}
                  />
                  <label
                    for="membersonlyField"
                    class="button__label fieldset__label"
                  >
                    ${translate('newArticle.MEMBERS_ONLY')}
                  </label>
                </fieldset>
                <a
                  class="new-article__link button button--s button--secondary"
                  target="_blank"
                  href="https://gitlab.com/zaplog/api-zaplog/-/wikis/markdown-cheatsheet"
                >
                  ${translate('newArticle.MARKDOWN_CHEATSHEET')}
                </a>
              </div>
            </div>
            <div class="new-article__header-row">
              <fieldset class="fieldset new-article__field">
                <input
                  class="form-field fieldset__full-width"
                  id="tagsField"
                  type="text"
                  name="tags"
                  value="${pipe(
                    NewArticleValues,
                    map(({ tags }) => (tags ? tags.join(' ') : ''))
                  )}"
                  placeholder="${translate('newArticle.TAGS_PLACEHOLDER')}"
                />
              </fieldset>
            </div>
          </div>
        </div>
        ${async function* ({ createArticleForm, translate, html }) {
          for await (const { error } of createArticleForm) {
            if (error) {
              yield html`<section class="new-article__error">
                ${Error({
                  title: translate('newArticle.SAVE_ARTICLE_ERROR'),
                  description: error,
                })}
              </section> `;
            }
          }
        }}
      </header>
      <main class="new-article__content">
        <input
          type="hidden"
          name="id"
          value="${pipe(
            NewArticleValues,
            map(({ id }) => id ?? '')
          )}"
        />
        <input
          type="hidden"
          name="copyright"
          value="${pipe(
            NewArticleValues,
            map(({ copyright }) => copyright ?? '')
          )}"
        />
        <fieldset class="new-article__field new-article__markdown fieldset">
          ${MarkdownTextarea({
            clean: true,
            className: 'fieldset__full-width new-article__textarea',
          })}
        </fieldset>
        ${pipe(
          NewArticleValues,
          map(({ copyrightCheck }) =>
            copyrightCheck
              ? Dialog({
                  dialogClass: 'new-article__copyright-check-dialog',
                  actions: [
                    createElement(
                      'div',
                      { class: 'create-article__submit' },
                      html`<button
                        class="button button--s"
                        type="submit"
                        name="action"
                        value="create"
                      >
                        ${pipe(
                          NewArticleValues,
                          map(({ id, published }) =>
                            id
                              ? ({ translate }) =>
                                  translate(
                                    published
                                      ? 'article.SAVE'
                                      : 'newArticle.SAVE_CONCEPT'
                                  )
                              : ({ translate }) =>
                                  translate('newArticle.PREVIEW_CONCEPT')
                          )
                        )}
                      </button>`
                    ),
                  ],
                  content: ({ html, translate }) =>
                    html`<fieldset
                      class="fieldset new-article__copyright-check-fieldset"
                    >
                      <input
                        class="form-field form-field--checkbox"
                        type="checkbox"
                        name="copyrightCheck"
                        id="copyrightCheckField"
                      />
                      <label
                        for="copyrightCheckField"
                        class="form-field--checkbox-label"
                      >
                        ${translate('newArticle.COPYRIGHT_CHECK')}
                      </label>
                    </fieldset>`,
                })
              : null
          )
        )}
      </main>
    </zaplog-markdown-textarea>`
  );
