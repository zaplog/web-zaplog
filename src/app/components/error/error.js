import { Notification } from '../notification/notification.js';

export const Error = ({ title, description }, ...children) =>
  Notification(
    { title, description, icon: 'error', type: 'error' },
    ...children
  );
