import { pipe, map, combineLatest } from 'create-async-generator';
import { UserLocale } from '../../resources/user-locale.js';
import getTimeZoneModule from './get-time-zone.js';
import { toUTCDate } from './to-utc-date.js';

export const DateFormatter =
  ({ dateString, ...options }) =>
  ({ Intl }) =>
    pipe(
      combineLatest([UserLocale, dateString]),
      map(([{ baseName }, dateValue]) =>
        new Intl.DateTimeFormat(baseName, {
          dateStyle: 'long',
          timeStyle: 'short',
          timeZone: getTimeZoneModule.getTimeZone({ Intl }),
          ...options,
        }).format(toUTCDate(dateValue))
      )
    );
