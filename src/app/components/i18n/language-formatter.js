import { pipe, map, combineLatest } from 'create-async-generator';
import { UserLocale } from '../../resources/user-locale.js';

export const LanguageFormatter =
  ({ language }) =>
  ({ Intl }) =>
    pipe(
      combineLatest([UserLocale, language]),
      map(([locale, language]) =>
        new Intl.DisplayNames(locale.language, {
          type: 'language',
        }).of(language)
      )
    );
