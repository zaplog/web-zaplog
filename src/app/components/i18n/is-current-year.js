import getCurrentDateModule from './get-current-date.js';

export const isCurrentYear = (date) => {
  const today = getCurrentDateModule.getCurrentDate();
  return date.getFullYear() === today.getFullYear();
};
