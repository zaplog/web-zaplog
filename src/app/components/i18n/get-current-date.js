const getCurrentDate = () => {
  return new Date();
};

// default export needed to mock this function
export default {
  getCurrentDate,
};
