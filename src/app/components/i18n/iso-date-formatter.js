import { pipe, map } from 'create-async-generator';
import { toUTCDate } from './to-utc-date.js';

export const IsoDateFormatter = ({ dateString }) =>
  pipe(
    dateString,
    map((dateValue) => toUTCDate(dateValue).toISOString())
  );
