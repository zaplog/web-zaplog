import { pipe, map, combineLatest } from 'create-async-generator';
import { UserLocale } from '../../resources/user-locale.js';
import getTimeZoneModule from './get-time-zone.js';
import { isCurrentYear } from './is-current-year.js';
import { isToday } from './is-today.js';
import { toUTCDate } from './to-utc-date.js';

export const ShortDateFormatter =
  ({ dateString, short, full }) =>
  ({ Intl }) =>
    pipe(
      combineLatest([UserLocale, dateString]),
      map(([{ baseName }, dateValue]) => {
        const date = toUTCDate(dateValue);
        const formatter = new Intl.DateTimeFormat(
          baseName,
          !full && isToday(date)
            ? {
                timeStyle: 'short',
                timeZone: getTimeZoneModule.getTimeZone({ Intl }),
              }
            : {
                day: 'numeric',
                month: short ? 'short' : 'long',
                ...(!full && isCurrentYear(date) ? {} : { year: 'numeric' }),
                ...(!full ? {} : { hour: 'numeric', minute: 'numeric' }),
                timeZone: getTimeZoneModule.getTimeZone({ Intl }),
              }
        );
        return formatter.format(date);
      })
    );
