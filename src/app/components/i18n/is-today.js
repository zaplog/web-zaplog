import getCurrentDateModule from './get-current-date.js';

export const isToday = (date) => {
  const today = getCurrentDateModule.getCurrentDate();
  return (
    date.getDate() === today.getDate() &&
    date.getMonth() === today.getMonth() &&
    date.getFullYear() === today.getFullYear()
  );
};
