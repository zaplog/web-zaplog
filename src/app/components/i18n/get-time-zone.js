const getTimeZone = ({ Intl }) => {
  return Intl.DateTimeFormat().resolvedOptions().timeZone;
};

// default export needed to mock this function
export default {
  getTimeZone,
};
