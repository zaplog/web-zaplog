import {
  ArticleAuthorHref,
  ArticleCanonicalUrl,
  ArticleCopyright,
  ArticleCreateDatetime,
  ArticleDescription,
  ArticleImage,
  ArticleKeywords,
  ArticleTitle,
} from '../../resources/article.js';

export const ArticleMeta = ({ html }) => html`<meta
    name="keywords"
    content="${ArticleKeywords}"
  />
  <meta name="news_keywords" content="${ArticleKeywords}" />
  <meta name="copyright" content="${ArticleCopyright}" />
  <meta name="author" content="${ArticleAuthorHref}" />
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "${ArticleCanonicalUrl}"
      },
      "headline": "${ArticleTitle}",
      "image": ["${ArticleImage}"],
      "datePublished": "${ArticleCreateDatetime}",
      "author": {},
      "publisher": {},
      "description": "${ArticleDescription}"
    }
  </script>`;
