import { PageStandalone } from '../page-standalone/page-standalone.js';

export const NotFound = ({ html, translate }) =>
  PageStandalone(
    { className: 'not-found' },
    html`<h1 class="not-found__title">${translate('pages.NOT_FOUND')}</h1>
      <p class="not-found__description">
        ${translate('notFound.DESCRIPTION')}
      </p>`
  );
