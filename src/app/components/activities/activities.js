import { pipe, map } from 'create-async-generator';
import { RouterLink } from '../../../lib/router/router-link.js';
import { Activity } from '../activity/activity.js';
import { ActivityTypes } from '../../models.js';
import { Reaction } from '../reaction/reaction.js';

export const Activities =
  ({ activityLinks }) =>
  ({ html, routes, translate }) =>
    html`<ol class="activities">
      ${pipe(
        activityLinks,
        map((activityLinksList) =>
          activityLinksList.map(
            (activityTypes) =>
              html`<li>
                ${RouterLink(
                  {
                    route: routes.article,
                    params: {
                      id: activityTypes[0][0].linkid,
                      title: activityTypes[0][0].linktitle,
                      channel: activityTypes[0][0].channelname,
                    },
                    classes: 'activities__link link',
                  },
                  html`<h2 class="activities__title">
                    ${activityTypes[0][0].linktitle}
                  </h2>`,
                  activityTypes[0][0].linkpublished
                    ? null
                    : html`<button
                        class="activities__concept button button--xs"
                      >
                        ${translate('article.CONCEPT')}
                      </button>`
                )}
                <ol class="activities__link-list">
                  ${activityTypes.map((activities) =>
                    activities[0].type === ActivityTypes.InsertReaction
                      ? activities.map(
                          ({
                            linkid,
                            linktitle,
                            reactionid,
                            channelavatar,
                            channelname,
                            reactiontext,
                            datetime,
                          }) => html`<li class="activities__link-item comment">
                            ${Reaction({
                              id: reactionid,
                              name: channelname,
                              avatar: channelavatar,
                              description: reactiontext,
                              createdatetime: datetime,
                              linkid,
                              title: linktitle,
                              showIcon: true,
                            })}
                          </li>`
                        )
                      : html`<li
                          class="panel activities__link-item activities__panel"
                        >
                          ${Activity({ activities })}
                        </li>`
                  )}
                </ol>
              </li>`
          )
        )
      )}
    </ol>`;
