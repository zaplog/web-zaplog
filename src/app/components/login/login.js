import { pipe, map } from 'create-async-generator';
import { Notification } from '../notification/notification.js';
import { Error } from '../error/error.js';
import { Entries } from '../entries/entries.js';
import { ExistingAccounts } from '../existing-accounts/existing-accounts.js';
import { NewAccount } from '../new-account/new-account.js';
import { PageStandalone } from '../page-standalone/page-standalone.js';
import { Intro } from '../intro/intro.js';
import { FrontpageFeatured } from '../../resources/frontpage.js';
import { RouterLink } from '../../../lib/router/router-link.js';
import { twofaLinkSent } from '../../effects/login-form.js';

export const Login = ({ html, translate, configuration }) =>
  PageStandalone(
    { className: 'login' },
    configuration.community
      ? html`<p class="login__description">${translate('login.INTRO')}</p>`
      : pipe(
          ({ StoredTwoFa }) => StoredTwoFa,
          map((storedTwoFa) => {
            if (!storedTwoFa?.length) {
              return Intro;
            }
          })
        ),
    async function* ({ createArticleForm }) {
      for await (const { error } of createArticleForm) {
        if (error) {
          yield Error({
            title: translate('login.ERROR'),
            description: error,
          });
        }
      }
    },
    ({ error }) => {
      if (error) {
        return Error({
          title: translate('login.UNAUTHORIZED'),
          description: error,
        });
      }
    },
    ({ loginForm }) =>
      pipe(
        loginForm,
        map((loginStatus) =>
          loginStatus === twofaLinkSent
            ? [
                Notification({
                  icon: 'success',
                  description: translate('login.SUCCESS'),
                }),
                html`<ul class="login__list bullet-list">
                  <li class="bullet-list__item">${translate('login.RULE1')}</li>
                  <li class="bullet-list__item">${translate('login.RULE2')}</li>
                  <li class="bullet-list__item">${translate('login.RULE3')}</li>
                  <li class="bullet-list__item">${translate('login.RULE4')}</li>
                  <li class="bullet-list__item">${translate('login.RULE5')}</li>
                </ul>`,
              ]
            : [
                ExistingAccounts(),
                NewAccount(),
                configuration.community
                  ? null
                  : ({ createElement, translate, routes }) =>
                      createElement(
                        'div',
                        { class: 'login__featured' },
                        html`<h1 class="login__featured-title">
                          ${translate('FEATURED')}
                          ${RouterLink(
                            { route: routes.posts },
                            html`<div
                              class="icon icon--l icon--search login__featured-icon icon--link"
                            ></div>`
                          )}
                        </h1>`,
                        Entries({
                          hideDescription: true,
                          links: FrontpageFeatured,
                        })
                      ),
              ]
        )
      )
  );
