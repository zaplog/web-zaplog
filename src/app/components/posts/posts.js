import { Pagination } from '../pagination/pagination.js';
import { Entries } from '../entries/entries.js';
import { Tags } from '../tags/tags.js';
import { Channels } from '../channels/channels.js';
import { Page } from '../page/page.js';
import {
  StatisticsNumchannels,
  StatisticsNumtags,
} from '../../resources/statistics.js';
import { LinksChannels, LinksItems, LinksTags } from '../../resources/links.js';

export const Posts = Page({
  className: ({ configuration }) =>
    `posts ${configuration.community ? 'page--full-width' : 'page--column'}`,
  main: ({ configuration }) => [
    configuration.community
      ? ({ html, routes }) => html`<ul>
          <li class="panel posts__tags">
            ${Tags({
              tags: LinksTags,
              moreLink: routes.index.getHref(),
              moreAmount: StatisticsNumtags,
            })}
          </li>
          <li class="panel posts__channels">
            ${Channels({
              channels: LinksChannels,
              moreLink: routes.channelIndex.getHref(),
              moreAmount: StatisticsNumchannels,
            })}
          </li>
        </ul>`
      : null,
    Entries({
      links: LinksItems,
    }),
    Pagination,
  ],
});
