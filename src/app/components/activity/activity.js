import { RouterLink } from '../../../lib/router/router-link.js';
import { Time } from '../time/time.js';
import { ActivityIconClass } from '../activity-icon-class/activity-icon-class.js';

export const Activity =
  ({ activities }) =>
  ({ html, routes, translate }) => {
    const { type, linkid, linktitle, datetime } = activities[0];
    return html`<div class="activity" title="${translate(`activity.${type}`)}">
      <ol class="activity__channels">
        ${activities.map(
          ({ channelname, channelavatar }) => html`<li
            class="activity__channel"
          >
            ${RouterLink(
              {
                route: routes.article,
                params: {
                  id: linkid,
                  title: linktitle,
                  channel: channelname,
                },
                classes: 'activity__channel-link link',
              },
              html`<img
                src="${channelavatar}"
                class="activity__channel-avatar"
                alt="${channelname}"
              />`,
              html`<span class="activity__channel-name">@${channelname}</span>`
            )}
          </li>`
        )}
      </ol>
      ${Time({
        className: 'activity__time',
        datetime,
        short: true,
      })}
      <span
        class="activity__icon icon icon--s icon--${ActivityIconClass({ type })}"
      ></span>
    </div>`;
  };
