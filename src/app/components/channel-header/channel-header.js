import { RouterLink } from '../../../lib/router/router-link.js';
import { ChannelAvatar, ChannelResourceName } from '../../resources/channel.js';

export const ChannelHeader = ({ html, routes }) =>
  RouterLink(
    {
      route: routes.channel,
      params: { channel: ChannelResourceName },
      classes: 'channel-header link link--plain',
    },
    html`<img
      src="${ChannelAvatar}"
      class="channel-header__image"
      alt="${ChannelResourceName}"
      title="${ChannelResourceName}"
    />`,
    html`<div class="channel-header__title">@${ChannelResourceName}</div>`
  );
