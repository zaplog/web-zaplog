import { pipe, map } from 'create-async-generator';
import { AtomEntry } from '../atom-entry/atom-entry.js';
import { SiteDescription, SiteTitle } from '../../resources/site-settings.js';

export const AtomFeed = ({ htmlUrl, lastModified, pagination, entries }) =>
  async function* ({ html, router }) {
    for await (const { params, route } of router) {
      const page = params.page ? parseInt(params.page, 10) : 1;
      yield html`<?xml version="1.0" encoding="utf-8"?>
        <feed xmlns="http://www.w3.org/2005/Atom">
          <id>${route.getHref(params)}</id>
          <updated>${lastModified}</updated>
          <title>${SiteTitle}</title>
          <subtitle>${SiteDescription}</subtitle>
          <link href="${route.getHref(params)}" rel="self" />
          <link href="${htmlUrl}" rel="alternate" type="text/html" />
          ${pagination
            ? `${
                page > 1
                  ? `<link rel="prev"
                         type="application/atom+xml"
                         href="${route.getHref({
                           ...params,
                           page: page - 1,
                         })}" />`
                  : ''
              }
                  <link rel="next"
                      type="application/atom+xml"
                      href="${route.getHref({
                        ...params,
                        page: page + 1,
                      })}" />`
            : ''}
          ${pipe(
            entries,
            map((entriesList) =>
              entriesList.map(
                ({
                  id,
                  title,
                  updatedatetime,
                  description,
                  xtext,
                  channelname,
                }) =>
                  AtomEntry({
                    title,
                    id,
                    channelname,
                    updatedatetime,
                    description,
                    xtext,
                  })
              )
            )
          )}
        </feed>`;
    }
  };
