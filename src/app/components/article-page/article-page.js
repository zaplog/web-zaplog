import { Page } from '../page/page.js';
import { ArticleReactions } from '../article-reactions/article-reactions.js';
import { ArticleInteractions } from '../article-interactions/article-interactions.js';
import { ArticleLegal } from '../article-legal/article-legal.js';
import { ArticlePublish } from '../article-publish/article-publish.js';
import { ArticleAside } from '../article-aside/article-aside.js';
import {
  ArticleAuthor,
  ArticleCopyright,
  ArticleShowReactions,
  ArticleTags,
  ArticleXText,
} from '../../resources/article.js';
import { Tags } from '../tags/tags.js';

export const ArticlePage = Page({
  className: 'page--column',
  main: ({ html, htmlSafe }) =>
    html`<article
      class="article-page"
      itemscope
      itemtype="http://schema.org/Article"
    >
      <section class="article-page__top">
        ${ArticlePublish}
        <section class="article-page__tags panel">
          ${Tags({
            channel: ArticleAuthor,
            tags: ArticleTags,
          })}
        </section>
      </section>
      <section class="article-page__legal">${ArticleLegal}</section>
      <section class="article-page__content markdown" itemprop="description">
        ${htmlSafe(ArticleXText)}
      </section>
      <section class="article-page__copyright">${ArticleCopyright}</section>
      <section class="article-page__interactions">
        ${ArticleInteractions}
      </section>
      <section class="article-page__reactions">
        ${async function* (app) {
          for await (const showReactions of ArticleShowReactions(app)) {
            yield showReactions ? ArticleReactions : null;
          }
        }}
      </section>
    </article>`,
  aside: ({ configuration }) => (configuration.community ? ArticleAside : null),
});
