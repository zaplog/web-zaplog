import { pipe, map } from 'create-async-generator';
import { LanguageFormatter } from '../i18n/language-formatter.js';
import {
  ChannelAlgorithms,
  ChannelLanguages,
} from '../../resources/channel.js';

export const ChannelEditAdmin = ({ html, translate }) =>
  html`<dt class="description-list__term">
      <label for="algorithmField">${translate('channel.ALGORITHM')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      <select
        class="form-field fieldset__full-width"
        id="algorithmField"
        name="algorithm"
      >
        ${async function* (app) {
          for await (const algorithms of ChannelAlgorithms(app)) {
            yield algorithms.map(
              ({ selected, algorithm }) => html`<option
                value="${algorithm}"
                ${selected ? ' selected' : null}
              >
                ${algorithm}
              </option>`
            );
          }
        }}
      </select>
    </dd>
    <dt class="description-list__term">
      <label for="languageField">${translate('LANGUAGE')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      <select
        class="form-field fieldset__full-width"
        id="languageField"
        name="language"
      >
        ${pipe(
          ChannelLanguages,
          map((languages) =>
            languages.map(
              ({ selected, language }) => html`<option
                value="${language}"
                ${selected ? ' selected' : null}
              >
                ${LanguageFormatter({ language })}
              </option>`
            )
          )
        )}
      </select>
    </dd>`;
