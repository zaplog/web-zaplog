import { ArticleVote } from '../article-vote/article-vote.js';
import { ArticleShare } from '../article-share/article-share.js';
import { ArticleMarkdownLink } from '../article-markdown-link/article-markdown-link.js';
import { ArticleCommentCount } from '../article-comment-count/article-comment-count.js';

export const ArticleInteractions = ({ html }) => html`<section
  class="article-interactions"
>
  <section class="article-interactions__item">${ArticleVote}</section>
  <section class="article-interactions__item">${ArticleCommentCount}</section>
  <section class="article-interactions__item article-share">
    ${ArticleShare}
  </section>
  <section class="article-interactions__item">${ArticleMarkdownLink}</section>
</section>`;
