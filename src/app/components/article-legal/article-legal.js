import { ArticleCreateDatetime } from '../../resources/article.js';
import { Time } from '../time/time.js';

export const ArticleLegal = Time({
  datetime: ArticleCreateDatetime,
  className: 'publish-date',
  full: true,
});
