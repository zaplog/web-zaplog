import {
  loginAndRetryFormFields,
  loginAndRetryStatus,
} from '../../effects/login-and-retry-form.js';
import { RetryFormDataString } from '../../resources/retry-form-data-string.js';
import { RetryFormId } from '../../resources/retry-form-id.js';

export const RetryFormFields = ({ html }) => [
  html`<input
    type="hidden"
    name="${loginAndRetryFormFields.STATUS}"
    value="${loginAndRetryStatus.RETRIED}"
  />`,
  html`<input
    type="hidden"
    name="${loginAndRetryFormFields.FORM_ID}"
    value="${RetryFormId}"
  />`,
  html`<input
    type="hidden"
    name="${loginAndRetryFormFields.FORM_DATA}"
    value="${RetryFormDataString}"
  />`,
];
