import { RouterLink } from '../../../lib/router/router-link.js';
import { Time } from '../time/time.js';

export const Entry =
  ({
    id,
    image,
    title,
    description,
    createdatetime,
    channelname,
    hideDescription,
  }) =>
  ({ html, routes, decode }) =>
    RouterLink(
      {
        route: routes.article,
        params: {
          id,
          title,
          channel: channelname,
        },
        classes: 'entry link',
      },
      Time({
        datetime: createdatetime,
        className: 'entry__datetime',
        short: true,
      }),
      html`<div class="entry__image-wrapper">
        ${image
          ? html`<img
              loading="lazy"
              src="${image}"
              alt="${title}"
              itemprop="image"
              class="entry__image"
            />`
          : null}
      </div>`,
      html`<div class="entry__text">
        <h1 class="entry__headline" itemprop="name headline">
          ${decode(title)}
        </h1>
        ${hideDescription
          ? null
          : html`<p class="entry__description" itemprop="description">
              ${description}
            </p>`}
      </div>`
    );
