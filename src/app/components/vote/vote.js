import { pipe, map } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';

export const Vote =
  ({ form, votescount, id, hasVoted, justVoted, className }) =>
  ({ html, translate }) =>
    Form(
      {
        name: form,
        class: html`vote ${className}`,
      },
      html`<input type="hidden" name="id" value="${id}" />`,
      html`<button
        type="submit"
        title="${pipe(
          hasVoted,
          map((hasVotedValue) =>
            translate(hasVotedValue ? 'article.UNVOTE' : 'article.VOTE')
          )
        )}"
        class="button button--xs button--${pipe(
          justVoted,
          map((justVotedValue) => (justVotedValue ? 'resolved' : 'pristine'))
        )}"
      >
        <span
          class="button__icon icon icon--s ${pipe(
            hasVoted,
            map((hasVotedValue) =>
              hasVotedValue ? 'icon--voted' : 'icon--vote'
            )
          )}"
        ></span>
        <span
          class="vote__count"
          data-animate="${pipe(
            justVoted,
            map((justVotedValue) => (justVotedValue ? votescount : null))
          )}"
          >${votescount}</span
        >
      </button>`
    );
