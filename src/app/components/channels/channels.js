import { pipe, map } from 'create-async-generator';
import { Chips } from '../chips/chips.js';

export const Channels =
  ({ channels, moreLink, moreAmount, hideAvatar }) =>
  ({ html, routes }) =>
    Chips({
      className: 'channels',
      items: pipe(
        channels,
        map((channelsList) =>
          channelsList.map(({ name, avatar }) => ({
            route: routes.posts,
            params: {
              channel: name,
            },
            text: [
              hideAvatar
                ? null
                : html`<img src="${avatar}" class="channels__avatar" />`,
              html`<div class="channels__info">@${name}</div>`,
            ],
          }))
        )
      ),
      moreLink,
      moreAmount,
    });
