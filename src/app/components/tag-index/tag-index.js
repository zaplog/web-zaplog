import { Page } from '../page/page.js';
import { Tags } from '../tags/tags.js';
import { BackOfBookIndex } from '../back-of-book-index/back-of-book-index.js';

export const TagIndex = Page({
  className: 'page--full-width',
  search: false,
  main: BackOfBookIndex(
    {
      data: ({ Tags }) => Tags,
      indexBy: 'tag',
    },
    (items) =>
      Tags({
        tags: items,
      })
  ),
});
