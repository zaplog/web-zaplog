import { pipe, map } from 'create-async-generator';

export const BackOfBookIndex =
  ({ data, indexBy }, getContent) =>
  ({ html, translate }) =>
    html`<div class="back-of-book-index">
      <div class="back-of-book-index__links">
        <a class="link link--nav" href="#1">1</a>
        <a class="link link--nav" href="#2">2</a>
        <a class="link link--nav" href="#3">3</a>
        <a class="link link--nav" href="#4">4</a>
        <a class="link link--nav" href="#5">5</a>
        <a class="link link--nav" href="#6">6</a>
        <a class="link link--nav" href="#7">7</a>
        <a class="link link--nav" href="#8">8</a>
        <a class="link link--nav" href="#9">9</a>
        <a class="link link--nav" href="#0">0</a>
        <a class="link link--nav" href="#A">A</a>
        <a class="link link--nav" href="#B">B</a>
        <a class="link link--nav" href="#C">C</a>
        <a class="link link--nav" href="#D">D</a>
        <a class="link link--nav" href="#E">E</a>
        <a class="link link--nav" href="#F">F</a>
        <a class="link link--nav" href="#G">G</a>
        <a class="link link--nav" href="#H">H</a>
        <a class="link link--nav" href="#I">I</a>
        <a class="link link--nav" href="#J">J</a>
        <a class="link link--nav" href="#K">K</a>
        <a class="link link--nav" href="#L">L</a>
        <a class="link link--nav" href="#M">M</a>
        <a class="link link--nav" href="#N">N</a>
        <a class="link link--nav" href="#O">O</a>
        <a class="link link--nav" href="#P">P</a>
        <a class="link link--nav" href="#Q">Q</a>
        <a class="link link--nav" href="#R">R</a>
        <a class="link link--nav" href="#S">S</a>
        <a class="link link--nav" href="#T">T</a>
        <a class="link link--nav" href="#U">U</a>
        <a class="link link--nav" href="#V">V</a>
        <a class="link link--nav" href="#W">W</a>
        <a class="link link--nav" href="#X">X</a>
        <a class="link link--nav" href="#Y">Y</a>
        <a class="link link--nav" href="#Z">Z</a>
      </div>
      <form class="back-of-book-index__search trigger-find">
        <fieldset class="fieldset back-of-book-index__search-field">
          <input
            type="search"
            name="query"
            class="form-field form-field--inline"
            placeholder="${translate('SEARCH')}"
          />
          <button
            type="submit"
            class="button button--inline button--inline-last"
          >
            <span class="button__icon icon icon--search icon--s"></span>
          </button>
        </fieldset>
      </form>
      <ol class="back-of-book-index__list">
        ${pipe(
          data,
          map((items) => {
            const indexMap = items.reduce(
              (acc, curr) => ({
                ...acc,
                [curr[indexBy][0]]: [...(acc[curr[indexBy][0]] ?? []), curr],
              }),
              {}
            );
            return Object.keys(indexMap).map(
              (firstLetter) => html`<li>
                <header>
                  <h2
                    class="back-of-book-index__title"
                    id="${firstLetter.toUpperCase()}"
                  >
                    ${firstLetter.toUpperCase()}
                  </h2>
                </header>
                <div class="back-of-book-index__item">
                  ${getContent(async function* () {
                    yield indexMap[firstLetter];
                  })}
                </div>
              </li>`
            );
          })
        )}
      </ol>
    </div>`;
