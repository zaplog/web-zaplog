import { DateFormatter } from '../i18n/date-formatter.js';
import { IsoDateFormatter } from '../i18n/iso-date-formatter.js';
import { ShortDateFormatter } from '../i18n/short-date-formatter.js';

export const Time =
  ({ datetime, short, full, className }) =>
  ({ html }) =>
    html`<time
      class="${className}"
      datetime="${IsoDateFormatter({ dateString: datetime })}"
      title="${DateFormatter({ dateString: datetime })}"
      >${ShortDateFormatter({ full, short, dateString: datetime })}</time
    >`;
