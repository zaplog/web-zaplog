import { pipe, map } from 'create-async-generator';
import { NewArticleValues } from '../../resources/new-article-values.js';

export const MarkdownTextarea =
  ({ start, clean, className } = {}) =>
  ({ Textarea, translate }) =>
    Textarea(
      {
        class: `markdown-textarea ${
          clean ? 'markdown-textarea--clean' : ''
        } ${className}`,
        id: 'markdownField',
        name: 'markdown',
        placeholder: translate('newArticle.TEXT_PLACEHOLDER'),
        rows: '15',
        maxlength: 100000,
      },
      ({ translate }) =>
        pipe(
          NewArticleValues,
          map(
            ({ markdown }) =>
              markdown ??
              (start
                ? translate('login.MARKDOWN_START')
                : translate('login.MARKDOWN_TEMPLATE'))
          )
        )
    );
