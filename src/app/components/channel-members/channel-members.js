import { pipe, map } from 'create-async-generator';
import { Error } from '../error/error.js';
import { Notification } from '../notification/notification.js';
import { Page } from '../page/page.js';
import { ChannelHeader } from '../channel-header/channel-header.js';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';
import { ChannelResourceName } from '../../resources/channel.js';
import { ChannelLink } from '../channel-link/channel-link.js';

export const ChannelMembers = Page({
  className: 'channel-members page--column',
  search: false,
  main: [
    ({ html }) =>
      html`<div class="channel-members__header">${ChannelHeader}</div>`,
    async function* ({ deleteChannelMemberForm, translate }) {
      for await (const { error } of deleteChannelMemberForm) {
        yield Error({
          title: translate('channel.DELETE_CHANNEL_MEMBER_ERROR'),
          description: error,
        });
      }
    },
    ({ html, translate, ChannelMembers }) =>
      pipe(
        ChannelMembers,
        map((channelMembers) =>
          channelMembers.length === 0
            ? Notification({
                title: translate('channel.CHANNEL_MEMBERS_EMPTY_TITLE'),
                icon: 'empty',
                description: translate(
                  'channel.CHANNEL_MEMBERS_EMPTY_DESCRIPTION'
                ),
              })
            : html`<ol class="channel-members__members">
                ${channelMembers.map(
                  ({ name, avatar }) => html`<li class="channel-members__item">
                    ${Form(
                      {
                        name: Forms.DeleteChannelMemberForm,
                        class: 'channel-members__member',
                      },
                      pipe(
                        ChannelResourceName,
                        map(
                          (channelName) => html`<input
                            type="hidden"
                            name="channelName"
                            value="${channelName}"
                          />`
                        )
                      ),
                      html`<input
                        type="hidden"
                        name="channelMember"
                        value="${name}"
                      />`,
                      ChannelLink({
                        name,
                        avatar,
                        className: 'channel-members__channel',
                      }),
                      html`<button type="submit" class="button button--xs">
                        ${translate('DELETE')}
                      </button>`
                    )}
                  </li>`
                )}
              </ol>`
        )
      ),
    async function* ({ addChannelMemberForm, translate }) {
      for await (const { error } of addChannelMemberForm) {
        yield Error({
          title: translate('channel.ADD_CHANNEL_MEMBER_ERROR'),
          description: error,
        });
      }
    },
    ({ translate, ChannelInviteLinkSent }) =>
      pipe(
        ChannelInviteLinkSent,
        map((_channelInviteLinkSent) =>
          Notification({
            title: translate('channel.ADD_CHANNEL_MEMBER_SUCCESS'),
            icon: 'success',
          })
        )
      ),
    ({ html, Channel }) =>
      pipe(
        Channel,
        map(({ channel }) =>
          Form(
            { name: Forms.AddChannelMemberForm },

            ({ translate }) => html`<fieldset class="fieldset">
              <input
                type="email"
                name="email"
                class="form-field fieldset__full-width form-field--inline"
              />
              <input type="hidden" name="channelName" value="${channel.name}" />
              <button class="button button--inline" type="submit">
                ${translate('ADD')}
              </button>
            </fieldset>`
          )
        )
      ),
  ],
});
