import { Error } from '../error/error.js';
import { ArticleDelete } from '../article-delete/article-delete.js';
import { ArticleEdit } from '../article-edit/article-edit.js';
import { IsUserArticleOrUnpublished } from '../../resources/is-user-article-or-unpublished.js';
import { IsUserArticle } from '../../resources/is-user-article.js';

export const ArticlePublish = async function* ({
  router,
  routes,
  translate,
  ...rest
}) {
  for await (const { route } of router) {
    switch (route) {
      case routes.articleDelete:
        for await (const isUserArticle of IsUserArticle({
          router,
          routes,
          translate,
          ...rest,
        })) {
          yield isUserArticle ? ArticleDelete : null;
        }
        break;
      case routes.article:
        yield [
          async function* ({ publishArticleForm }) {
            for await (const { error } of publishArticleForm) {
              yield error
                ? Error({
                    title: translate('article.PUBLISH_ARTICLE_ERROR'),
                    description: error,
                  })
                : null;
            }
          },
          async function* (app) {
            for await (const isUserArticleOrUnpublished of IsUserArticleOrUnpublished(
              app
            )) {
              yield isUserArticleOrUnpublished ? ArticleEdit : null;
            }
          },
        ];
        break;
    }
  }
};
