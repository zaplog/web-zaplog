import { toUTCDate } from '../i18n/to-utc-date.js';
import { SiteTitle } from '../../resources/site-settings.js';

export const AtomEntry =
  ({ title, id, channelname, updatedatetime, description, xtext }) =>
  ({ html, routes, encode, decode }) =>
    html`<entry>
      <title>${encode(decode(title), { level: 'xml' })}</title>
      <author>
        <uri>${routes.home.getHref()}</uri>
        <name>${SiteTitle}</name>
      </author>
      <link
        rel="alternate"
        type="text/html"
        href="${routes.article.getHref({
          id,
          title,
          channel: channelname,
        })}"
      />
      <link
        rel="alternate"
        type="text/markdown"
        href="${routes.rawArticle.getHref({
          id,
          title,
          channel: channelname,
        })}"
      />
      <id
        >${routes.article.getHref({
          id,
          title,
          channel: channelname,
        })}</id
      >
      <updated>${toUTCDate(updatedatetime).toISOString()}</updated>
      <summary>${encode(decode(description), { level: 'xml' })}</summary>
      ${xtext
        ? `<content type="xhtml">
        <div xmlns="http://www.w3.org/1999/xhtml">${xtext}</div>
      </content>`
        : null}
    </entry>`;
