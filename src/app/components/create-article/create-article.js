import { Form } from '../../../lib/forms/form.js';
import { Error } from '../error/error.js';
import { Forms } from '../../forms.js';
import { NewArticleValues } from '../../resources/new-article-values.js';

export const CreateArticle =
  (props = {}, ...children) =>
  ({ html, createElement }) =>
    [
      Form(
        { ...props, name: Forms.CreateArticleForm },
        async function* ({ createArticleForm, translate }) {
          for await (const { error } of createArticleForm) {
            if (error) {
              yield Error({
                title: translate('newArticle.SAVE_ARTICLE_ERROR'),
                description: error,
              });
            }
          }
        },
        ...children,
        createElement(
          'div',
          { class: 'create-article__submit' },
          html`<button
            class="button"
            type="submit"
            name="action"
            value="create"
          >
            ${async function* (app) {
              for await (const { id, published } of NewArticleValues(app)) {
                yield id
                  ? ({ translate }) =>
                      translate(
                        published ? 'article.SAVE' : 'newArticle.SAVE_CONCEPT'
                      )
                  : ({ translate }) => translate('newArticle.PREVIEW_CONCEPT');
              }
            }}
          </button>`
        )
      ),
    ];
