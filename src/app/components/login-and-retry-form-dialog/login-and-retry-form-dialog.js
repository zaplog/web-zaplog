import { map, pipe } from 'create-async-generator';
import { Dialog } from '../dialog/dialog.js';
import { ExistingAccounts } from '../existing-accounts/existing-accounts.js';
import { NewAccount } from '../new-account/new-account.js';
import { Notification } from '../notification/notification.js';
import { Error } from '../error/error.js';
import { twofaLinkSent } from '../../effects/login-form.js';
import { RetryFormFields } from '../retry-form-fields/retry-form-fields.js';
import { RetryForm } from '../retry-form/retry-form.js';

export const LoginAndRetryFormDialog = ({
  error,
  html,
  translate,
  loginForm,
}) =>
  Dialog({
    header: translate('LOGIN_AND_RETRY'),
    contentClass: 'login-and-retry-form-dialog',
    content: [
      pipe(
        loginForm,
        map((loggedIn) =>
          loggedIn === twofaLinkSent
            ? Notification({
                icon: 'success',
                description: translate('login.SUCCESS'),
              })
            : [
                Error({
                  title: translate('login.ERROR'),
                  description: error,
                }),
                ExistingAccounts(
                  {
                    action: '',
                    replaceByGet: 'skip',
                  },
                  RetryFormFields
                ),
                NewAccount(
                  {
                    action: '',
                    replaceByGet: 'skip',
                  },
                  RetryFormFields
                ),
              ]
        )
      ),
    ],
    actions: RetryForm(
      null,
      html`<button type="submit" class="button button--primary">
        ${translate('RETRY')}
      </button>`
    ),
  });
