import { Error } from '../error/error.js';
import { Page } from '../page/page.js';
import { ChannelHeader } from '../channel-header/channel-header.js';
import { ChannelEditFields } from '../channel-edit-fields/channel-edit-fields.js';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';

export const ChannelEdit = Page({
  className: 'page--form channel-edit',
  search: false,
  main: Form(
    { name: Forms.ChannelEditForm },
    ({ html }) =>
      html`<div class="channel-edit__header">${ChannelHeader}</div>`,
    async function* ({ channelEditForm, translate }) {
      for await (const { error } of channelEditForm) {
        if (error) {
          yield Error({
            title: translate('channel.SAVE_CHANNEL_ERROR'),
            description: error,
          });
        }
      }
    },
    ChannelEditFields,
    ({ translate, html }) => html`<div class="button-row channel-edit__submit">
      <button type="submit" class="button">${translate('SAVE')}</button>
    </div>`
  ),
});
