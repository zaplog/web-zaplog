import { Activities } from '../activities/activities.js';
import { Page } from '../page/page.js';
import { ActivityLinks } from '../../resources/activities.js';

export const Discussion = Page({
  className: 'page--column',
  main: Activities({
    activityLinks: ActivityLinks,
  }),
});
