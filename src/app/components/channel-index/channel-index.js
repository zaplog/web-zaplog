import { BackOfBookIndex } from '../back-of-book-index/back-of-book-index.js';
import { Channels } from '../channels/channels.js';
import { Page } from '../page/page.js';

export const ChannelIndex = Page({
  className: 'page--full-width',
  search: false,
  main: BackOfBookIndex(
    {
      data: ({ Channels }) => Channels,
      indexBy: 'name',
    },
    (items) =>
      Channels({
        channels: items,
        hideAvatar: true,
      })
  ),
});
