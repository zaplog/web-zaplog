import { map, pipe } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';
import { RetryFormData } from '../../resources/retry-form-data.js';
import { RetryFormId } from '../../resources/retry-form-id.js';

export const RetryForm = (props = {}, ...children) =>
  Form(
    { ...props, name: RetryFormId },
    pipe(
      RetryFormData,
      map((formData) =>
        Object.entries(formData).map(
          ([key, value]) =>
            ({ html }) =>
              html`<input type="hidden" name="${key}" value="${value}" />`
        )
      )
    ),
    ...children
  );
