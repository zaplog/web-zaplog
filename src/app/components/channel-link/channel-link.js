import { RouterLink } from '../../../lib/router/router-link.js';

export const ChannelLink =
  ({ name, avatar, className }) =>
  ({ routes, html }) =>
    RouterLink(
      {
        route: routes.channel,
        params: { channel: name },
        classes: `channel link link--plain ${className}`,
      },
      html`<img
          src="${avatar}"
          class="channel__avatar"
          alt="${name}"
          title="${name}"
        />
        <span>@${name}</span>`
    );
