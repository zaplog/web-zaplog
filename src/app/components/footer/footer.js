import { AtomLink } from '../../resources/atom-link.js';

export const Footer = ({ html, translate }) =>
  html`<nav>
    <ul class="footer">
      ${async function* (app) {
        for await (const atomLink of AtomLink(app)) {
          yield atomLink
            ? html`<li class="footer__item">
                <a
                  href="${atomLink}"
                  class="icon icon--rss icon--link"
                  target="_blank"
                >
                </a>
              </li>`
            : null;
        }
      }}
      <li>
        <a
          href="https://gitlab.com/zaplog/api-zaplog/-/wikis/privacy-policy"
          target="_blank"
          class="footer__item link"
        >
          ${translate('footer.PRIVACY_POLICY')}</a
        >
      </li>
      <li>
        <a
          href="mailto:contact-project+zaplog-web-zaplog-29977491-issue-@incoming.gitlab.com"
          target="_blank"
          class="footer__item link"
        >
          ${translate('footer.REPORT_ISSUES')}
        </a>
      </li>
      <li>
        <a
          href="https://gitlab.com/zaplog"
          target="_blank"
          class="footer__item link"
        >
          ${translate('footer.DEVELOPMENT')}
        </a>
      </li>
      <li>
        <a
          href="https://gitlab.com/zaplog/api-zaplog/-/wikis/Zaplog-manual"
          target="_blank"
          class="footer__item link"
        >
          ${translate('footer.MANUAL')}</a
        >
      </li>
    </ul>
  </nav>`;
