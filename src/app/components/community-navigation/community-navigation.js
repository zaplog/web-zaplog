import { Navigation } from '../navigation/navigation.js';
import { Authenticated } from '../../resources/authenticated.js';
import { ChannelName } from '../../resources/channel-name.js';

export const CommunityNavigation = ({ html, routes, translate }) =>
  Navigation(
    { className: 'community-navigation' },
    {
      route: routes.posts,
      children: translate('pages.POSTS'),
      className: 'link link--nav',
    },
    {
      route: routes.discussion,
      children: translate('pages.DISCUSSION'),
      className: 'link link--nav',
    },
    async function* (app) {
      for await (const authenticated of Authenticated(app)) {
        if (authenticated) {
          yield {
            route: routes.channel,
            params: {
              channel: ChannelName,
            },
            children: translate('pages.MY_CHANNEL'),
            className: 'link link--nav',
          };
        } else {
          yield {
            route: routes.login,
            children: translate('pages.SIGN_IN'),
            className: 'link link--nav',
          };
        }
      }
    },
    {
      href: 'https://zaplog.gitlab.io/infra-zaplog/',
      target: '_blank',
      children: translate('START_YOUR_OWN'),
      className: 'link link--nav',
    },
    {
      href: 'https://zaplog.chat/#/room/#zapruder:zaplog.chat',
      target: '_blank',
      children: html`<img
        src="/assets/img/zaplog-pro-matrix.svg"
        class="community-navigation__matrix-link"
      />`,
      className: 'link link--nav',
    }
  );
