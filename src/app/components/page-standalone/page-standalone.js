import { RouterLink } from '../../../lib/router/router-link.js';
import { Footer } from '../footer/footer.js';
import {
  SiteAvatar,
  SiteDescription,
  SiteTitle,
} from '../../resources/site-settings.js';

export const PageStandalone =
  ({ className } = {}, ...children) =>
  ({ html, routes }) =>
    html`<div class="base page-standalone ${className}">
      <header class="page-standalone__header">
        ${RouterLink(
          {
            route: routes.home,
          },
          html`<img
            src="${SiteAvatar}"
            class="page-standalone__image"
            alt="${SiteTitle}"
            title="${SiteTitle}"
          />`
        )}
        <div class="page-standalone__site-title">${SiteTitle}</div>
        <div class="page-standalone__site-description">${SiteDescription}</div>
      </header>
      <main class="page-standalone__content">${children}</main>
      <footer class="page-standalone__footer">${Footer}</footer>
    </div>`;
