import { RouterLink } from '../../../lib/router/router-link.js';
import { Reactions } from '../reactions/reactions.js';
import { Error } from '../error/error.js';
import { Notification } from '../notification/notification.js';
import { ArticleComment } from '../article-comment/article-comment.js';
import { Authenticated } from '../../resources/authenticated.js';
import { IsArticlePublished } from '../../resources/article.js';
import { ArticleReactionItems } from '../../resources/article-reaction-items.js';

export const ArticleReactions = [
  async function* (app) {
    for await (const isArticlePublished of IsArticlePublished(app)) {
      yield isArticlePublished
        ? null
        : Notification({
            description: ({ translate }) =>
              translate('article.CONCEPT_COMMENT_WARNING'),
            icon: 'error',
          });
    }
  },
  ({ html }) => html`<section class="article-comments" id="comments">
    ${Reactions({
      items: ArticleReactionItems,
    })}
  </section>`,
  async function* ({ commentForm, translate }) {
    for await (const { error } of commentForm) {
      yield error
        ? Error({
            title: translate('article.ADD_COMMENT_ERROR'),
            description: error,
          })
        : null;
    }
  },
  async function* (app) {
    for await (const authenticated of Authenticated(app)) {
      if (authenticated) {
        yield ArticleComment;
      } else {
        yield;
        ({ html, routes, translate }) => html`<p>
          ${RouterLink(
            {
              route: routes.login,
              classes: 'link link--nav',
            },
            html`${translate('article.SIGN_IN_TO_COMMENT')}`
          )}
        </p>`;
      }
    }
  },
];
