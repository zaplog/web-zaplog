export const Intro = ({ html, translate }) => html`<section class="intro">
  <div class="intro__item">
    <div class="intro__icon">
      <div class="icon icon--l icon--success"></div>
    </div>
    <p class="intro__text">${translate('login.PUBLISH')}</p>
  </div>
  <div class="intro__item">
    <div class="intro__icon">
      <div class="icon icon--l icon--success"></div>
    </div>
    <p class="intro__text">${translate('login.DONATION')}</p>
  </div>
  <div class="intro__item">
    <div class="intro__icon">
      <div class="icon icon--l icon--success"></div>
    </div>
    <p class="intro__text">${translate('login.MAILINGLIST')}</p>
  </div>
  <div class="intro__item">
    <div class="intro__icon">
      <div class="icon icon--l icon--success"></div>
    </div>
    <p class="intro__text">${translate('login.CHAT')}</p>
  </div>
</section>`;
