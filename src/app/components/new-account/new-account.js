import { pipe, map } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';
import { MarkdownTextarea } from '../markdown-textarea/markdown-textarea.js';

export const NewAccount = (
  { action = null, replaceByGet = null, ...props } = {},
  ...children
) =>
  pipe(
    ({ StoredTwoFa }) => StoredTwoFa,
    map(
      (storedTwoFa) =>
        ({ html }) =>
          html`<section class="new-account">
            ${[
              storedTwoFa.length
                ? ({ translate, html }) => html`<p
                    class="new-account__other-account"
                  >
                    ${translate('login.OTHER_ACCOUNT')}
                  </p>`
                : null,
              ({ configuration, routes }) =>
                Form(
                  {
                    ...props,
                    action: action ?? routes.login.getHref(),
                    name:
                      configuration.community || storedTwoFa.length
                        ? Forms.LoginForm
                        : Forms.CreateArticleForm,
                  },
                  ...children,
                  ({ html }) =>
                    html`<input
                      type="hidden"
                      name="replaceByGet"
                      value="${replaceByGet}"
                    />`,
                  ({ translate, html }) => html`<fieldset class="fieldset">
                    <input
                      type="email"
                      name="email"
                      required
                      placeholder="${translate('login.TITLE')}"
                      class="form-field fieldset__full-width form-field--inline"
                    />
                    ${({ configuration, html }) =>
                      configuration.community || storedTwoFa.length
                        ? html`<button
                            class="button button--inline"
                            type="submit"
                            name="action"
                            value="existing"
                          >
                            ${translate('login.SUBMIT')}
                          </button>`
                        : null}
                  </fieldset>`,
                  ({ configuration, html }) =>
                    configuration.community || storedTwoFa.length
                      ? null
                      : html`<fieldset class="fieldset new-account__markdown">
                          ${MarkdownTextarea({
                            start: true,
                            className: 'fieldset__full-width',
                          })}
                        </fieldset>`,
                  ({ createElement }) =>
                    createElement(
                      'div',
                      { class: 'new-account__submit' },
                      ({ configuration }) =>
                        configuration.community || storedTwoFa.length
                          ? null
                          : ({ translate, html }) => [
                              html`<button
                                type="submit"
                                name="action"
                                value="create"
                                class="button new-account__submit-new"
                              >
                                ${translate('login.NEW_CHANNEL')}
                              </button>`,
                              html`<button
                                type="submit"
                                name="action"
                                value="existing"
                                class="button button--secondary new-account__submit-existing"
                              >
                                ${translate('login.EXISTING_CHANNEL')}
                              </button>`,
                            ]
                    )
                ),
            ]}
          </section>`
    )
  );
