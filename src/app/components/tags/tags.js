import { pipe, map } from 'create-async-generator';
import { Chips } from '../chips/chips.js';

export const Tags =
  ({ tags, moreLink, moreAmount, channel }) =>
  ({ routes, html, translate }) =>
    Chips({
      className: 'tags',
      items: pipe(
        tags,
        map((tagsList) => [
          ...tagsList.map(({ tag }) => ({
            route: routes.posts,
            params: {
              tag,
              channel,
            },
            text: `#${tag}`,
          })),
          {
            route: routes.posts,
            params: {
              tags: tagsList.map(({ tag }) => tag),
            },
            className: 'button button--xs',
            text: html`<span
                class="button__icon icon icon--search icon--xs"
              ></span>
              ${translate('POSTS')}`,
          },
        ])
      ),
      moreLink,
      moreAmount,
      itemprop: 'keywords',
    });
