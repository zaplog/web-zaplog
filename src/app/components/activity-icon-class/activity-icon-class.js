import { ActivityTypes } from '../../models.js';

export const ActivityIconClass = ({ type }) => {
  switch (type) {
    case ActivityTypes.InsertLink:
      return 'plus';
    case ActivityTypes.UpdateLink:
      return 'pencil';
    case ActivityTypes.InsertVote:
      return 'vote';
    case ActivityTypes.DeleteVote:
      return 'voted';
  }
};
