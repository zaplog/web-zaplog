import { Notification } from '../notification/notification.js';
import { RouterLink } from '../../../lib/router/router-link.js';
import {
  ArticleAuthor,
  ArticleId,
  ArticleTitle,
  IsArticlePublished,
} from '../../resources/article.js';
import { IsUserArticle } from '../../resources/is-user-article.js';
import { IsUserArticleUnpublished } from '../../resources/is-user-article-unpublished.js';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';

export const ArticleEdit = ({ html, translate, routes }) =>
  Notification(
    {
      icon: 'article',
      title: async function* (app) {
        for await (const isArticlePublished of IsArticlePublished(app)) {
          yield isArticlePublished
            ? translate('article.PUBLISHED')
            : translate('article.CONCEPT');
        }
      },
    },
    html`<section class="button-row">
      ${async function* (app) {
        for await (const isUserArticle of IsUserArticle(app)) {
          if (isUserArticle) {
            yield [
              RouterLink(
                {
                  route: routes.articleDelete,
                  params: {
                    id: ArticleId,
                    title: ArticleTitle,
                    channel: ArticleAuthor,
                  },
                  classes: 'button button--warning',
                },
                translate('DELETE')
              ),
              RouterLink(
                {
                  route: routes.editArticle,
                  params: {
                    id: ArticleId,
                    channel: ArticleAuthor,
                  },
                  classes: 'button',
                },
                translate('EDIT')
              ),
            ];
          } else {
            yield null;
          }
        }
      }} ${async function* (app) {
        for await (const isUserArticleUnpublished of IsUserArticleUnpublished(
          app
        )) {
          if (isUserArticleUnpublished) {
            yield Form(
              { name: Forms.PublishArticleForm },
              html`<input type="hidden" name="id" value="${ArticleId}" />
                <input type="hidden" name="title" value="${ArticleTitle}" />
                <input
                  type="hidden"
                  name="channelname"
                  value="${ArticleAuthor}"
                />
                <button
                  type="submit"
                  name="action"
                  class="button button--secondary"
                >
                  ${translate('PUBLISH')}
                </button>`
            );
          } else {
            yield null;
          }
        }
      }}
    </section>`
  );
