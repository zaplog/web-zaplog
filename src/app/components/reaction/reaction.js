import { pipe, map } from 'create-async-generator';
import { RouterLink } from '../../../lib/router/router-link.js';
import { Vote } from '../vote/vote.js';
import { Time } from '../time/time.js';
import { Forms } from '../../forms.js';

export const Reaction =
  ({
    id,
    name,
    avatar,
    description,
    createdatetime,
    linkid,
    title,
    channelLink,
    votescount,
    enableVote,
    showIcon,
    channelid,
  }) =>
  ({ html, routes, configuration, htmlSafe }) =>
    html`<div
      class="reaction panel ${configuration.adminChannelId === channelid
        ? 'reaction--collapsed'
        : null}"
      id="reaction-${id}"
    >
      <div class="reaction__data">
        <div class="reaction__headline">
          ${RouterLink(
            {
              route: channelLink
                ? configuration.community
                  ? routes.posts
                  : routes.channel
                : routes.article,
              params: channelLink
                ? {
                    channel: name,
                  }
                : {
                    id: linkid,
                    title,
                    commentId: id,
                  },
              classes: 'reaction__channel-link link',
            },
            avatar
              ? html`<img
                  src="${avatar}"
                  class="reaction__channel-avatar"
                  alt="${name}"
                />`
              : null,
            html`<span class="reaction__channel-name">@${name}</span>`
          )}
          ${enableVote
            ? pipe(
                ({ VotedReactionId }) => VotedReactionId,
                map((votedReactionId) =>
                  Vote({
                    className: 'reaction__vote',
                    votescount,
                    form: Forms.ReactionVoteForm,
                    id,
                    hasVoted: false,
                    justVoted: votedReactionId === id,
                  })
                )
              )
            : null}
          ${linkid
            ? RouterLink(
                {
                  route: routes.article,
                  params: {
                    id: linkid,
                    title,
                    commentId: `reaction-${id}`,
                    channel: name,
                  },
                  classes: 'reaction__link link',
                },
                html`<span class="icon icon--expand reaction__expand"></span
                  ><span class="reaction__id">#${id}</span>`
              )
            : null}
          ${Time({
            className: 'reaction__time',
            datetime: createdatetime,
            short: true,
          })}
          ${showIcon
            ? html`<span class="icon icon--s icon--comment"></span>`
            : null}
        </div>
        <div class="reaction__markdown markdown">${htmlSafe(description)}</div>
      </div>
    </div>`;
