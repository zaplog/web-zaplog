export const Notification =
  ({ title, description, icon, type, className }, ...children) =>
  ({ html }) =>
    html`<section
      class="notification ${type ? `notification--${type}` : ''} ${className}"
    >
      <span class="icon icon--l icon--${icon} notification__icon"></span>
      <section class="notification__info">
        <h2 class="notification__title">${title}</h2>
        <p class="notification__description">${description}</p>
      </section>
      ${children}
    </section>`;
