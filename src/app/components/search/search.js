import { pipe, map } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';
import { SearchValue } from '../../resources/search-value.js';

export const Search = ({ html, translate, routes }) =>
  Form(
    {
      name: 'search',
      method: 'get',
      action: routes.posts.getHref(),
      class: 'search',
    },
    html`<fieldset class="fieldset">
      <input
        type="search"
        name="query"
        value="${SearchValue}"
        placeholder="${translate('SEARCH')}"
        size="1"
        class="search__input form-field fieldset__full-width form-field--inline"
      />
      <fieldset class="button-row button-row--inline">
        <button
          type="submit"
          title="${translate('SEARCH')}"
          class="button search__button button--inline button--inline-last"
          name="action"
          value="search"
        >
          <span class="icon icon--search icon--s button__icon"></span>
        </button>
        ${pipe(
          SearchValue,
          map((searchValue) => {
            if (searchValue) {
              return html`<button
                type="submit"
                title="${translate('CLEAR')}"
                class="button search__button button--inline button--secondary button--secondary-inline"
                name="action"
                value="clear"
              >
                <span class="icon icon--close icon--s button__icon"></span>
              </button>`;
            }
          })
        )}
      </fieldset>
    </fieldset>`
  );
