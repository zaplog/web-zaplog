import { RouterLink } from '../../../lib/router/router-link.js';
import { ChannelNavigation } from '../channel-navigation/channel-navigation.js';
import { CommunityNavigation } from '../community-navigation/community-navigation.js';
import { IsMyChannelPage } from '../../resources/channel.js';
import { SiteAvatar, SiteTitle } from '../../resources/site-settings.js';
import { map, pipe } from 'create-async-generator';

export const Header = ({ html, configuration, routes }) =>
  html`<div class="header" id="nav">
    ${RouterLink(
      {
        route: routes.home,
        classes: 'header__logo',
      },
      html`<img
        src="${SiteAvatar}"
        class="header__image"
        alt="${SiteTitle}"
        title="${SiteTitle}"
      />`
    )}
    ${pipe(
      IsMyChannelPage,
      map((isMyChannel) => {
        if (!isMyChannel && !configuration.community) {
          return null;
        }
        return html`<div class="header__hamburger">
          <a
            class="header__hamburger-link header__hamburger-link--open"
            href="#nav"
            >nav</a
          >
          <a
            class="header__hamburger-link header__hamburger-link--close"
            href="#"
            >nav</a
          >
        </div>`;
      })
    )}
    ${pipe(
      IsMyChannelPage,
      map((isMyChannel) => {
        if (isMyChannel) {
          return ChannelNavigation;
        } else if (configuration.community) {
          return CommunityNavigation;
        }
      })
    )}
  </div>`;
