import { Navigation } from '../navigation/navigation.js';
import { ChannelResourceName } from '../../resources/channel.js';

export const ChannelNavigation = ({ routes, translate }) =>
  Navigation(
    { className: 'channel-navigation' },
    {
      route: routes.channel,
      params: {
        channel: ChannelResourceName,
      },
      children: translate('pages.MY_CHANNEL'),
      className: 'link link--nav',
    },
    {
      route: routes.channelEdit,
      params: { channel: ChannelResourceName },
      children: translate('channel.EDIT_CHANNEL'),
      className: 'link link--nav',
    },
    {
      route: routes.channelMembers,
      params: { channel: ChannelResourceName },
      children: translate('channel.CHANNEL_MEMBERS'),
      className: 'link link--nav',
    },
    {
      route: routes.channelMemberships,
      params: { channel: ChannelResourceName },
      children: translate('channel.CHANNEL_MEMBERSHIPS'),
      className: 'link link--nav',
    },
    {
      route: routes.logout,
      children: translate('pages.SIGN_OUT'),
      className: 'link link--nav',
    },
    {
      route: routes.newArticle,
      params: {
        channel: ChannelResourceName,
      },
      className: 'button button--s channel-navigation__add-article',
      activeClass: 'button--active',
      children: translate('pages.ADD_ARTICLE'),
    }
  );
