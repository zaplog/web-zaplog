import { Entries } from '../entries/entries.js';
import { Reactions } from '../reactions/reactions.js';
import { Tags } from '../tags/tags.js';
import { Channels } from '../channels/channels.js';
import { Page } from '../page/page.js';
import {
  FrontpageTrendingChannels,
  FrontpageTrendinglinks,
  FrontpageTrendingReactions,
  FrontpageTrendingTags,
} from '../../resources/frontpage.js';
import {
  StatisticsNumchannels,
  StatisticsNumtags,
} from '../../resources/statistics.js';

export const Frontpage = Page({
  className: 'frontpage',
  main: [
    ({ html, routes }) => html`<ul>
      <li class="frontpage__tags panel">
        ${Tags({
          tags: FrontpageTrendingTags,
          moreLink: routes.index.getHref(),
          moreAmount: StatisticsNumtags,
        })}
      </li>
      <li class="frontpage__channels panel">
        ${Channels({
          channels: FrontpageTrendingChannels,
          moreLink: routes.channelIndex.getHref(),
          moreAmount: StatisticsNumchannels,
        })}
      </li>
    </ul>`,
    Entries({
      links: FrontpageTrendinglinks,
    }),
  ],
  aside: ({ configuration }) =>
    configuration.community
      ? Reactions({
          size: configuration.asideSize,
          items: FrontpageTrendingReactions,
        })
      : null,
});
