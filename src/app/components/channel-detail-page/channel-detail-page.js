import { Entries } from '../entries/entries.js';
import { Pagination } from '../pagination/pagination.js';
import { Reactions } from '../reactions/reactions.js';
import { Tags } from '../tags/tags.js';
import { Page } from '../page/page.js';
import { ChannelDetailUnpublished } from '../channel-detail-unpublished/channel-detail-unpublished.js';
import { ChannelDetailEmpty } from '../channel-detail-empty/channel-detail-empty.js';
import {
  ChannelResourceName,
  ChannelTags,
  IsUserChannel,
} from '../../resources/channel.js';
import { ChannelLinks } from '../../resources/channel-links.js';
import { IsChannelEmpty } from '../../resources/is-channel-empty.js';

export const ChannelDetailPage = Page({
  className: ({ configuration }) =>
    `channel-detail-page ${configuration.community ? '' : 'page--column'}`,
  main: [
    ({ html }) =>
      html`<div class="panel channel-detail-page__tags">
        ${Tags({
          channel: ChannelResourceName,
          tags: ChannelTags,
        })}
      </div>`,
    async function* (app) {
      for await (const isUserChannel of IsUserChannel(app)) {
        yield isUserChannel ? ChannelDetailUnpublished : null;
      }
    },
    async function* (app) {
      for await (const isChannelEmpty of IsChannelEmpty(app)) {
        yield isChannelEmpty
          ? ({ html }) => html`<div class="channel-detail-page__new-channel">
              ${ChannelDetailEmpty}
            </div>`
          : [
              ({ html }) => html`<div class="channel-detail-page__entries">
                ${Entries({
                  links: ChannelLinks,
                })}
              </div>`,
              Pagination,
            ];
      }
    },
  ],
  aside: ({ configuration }) =>
    configuration.community
      ? Reactions({
          size: configuration.asideSize,
          items: ({ ChannelReactions }) => ChannelReactions,
        })
      : null,
});
