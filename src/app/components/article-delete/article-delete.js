import { Form } from '../../../lib/forms/form.js';
import { RouterLink } from '../../../lib/router/router-link.js';
import { Forms } from '../../forms.js';
import { Notification } from '../notification/notification.js';
import {
  ArticleAuthor,
  ArticleId,
  ArticleTitle,
} from '../../resources/article.js';

export const ArticleDelete = ({ translate }) =>
  Notification(
    {
      icon: 'article',
      title: translate('DELETE'),
      description: translate('UNRECOVERABLE_WARNING'),
      type: 'error',
    },
    ({ html, translate, routes }) =>
      html`${Form(
        { name: Forms.DeleteArticleForm },
        html`<section class="button-row">
          ${RouterLink(
            {
              route: routes.article,
              params: {
                id: ArticleId,
                title: ArticleTitle,
                channel: ArticleAuthor,
              },
              classes: 'button button--secondary button--warning',
            },
            translate('CANCEL')
          )}
          <input type="hidden" name="id" value="${ArticleId}" />
          <button type="submit" name="action" class="button button--warning">
            ${translate('DELETE')}
          </button>
        </section>`
      )}`
  );
