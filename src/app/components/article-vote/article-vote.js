import { Vote } from '../vote/vote.js';
import { HasUserJustVotedArticle } from '../../resources/has-user-just-voted-article.js';
import { HasUserVotedArticle } from '../../resources/has-user-voted-article.js';
import { Forms } from '../../forms.js';
import { ArticleId, ArticleVotescount } from '../../resources/article.js';

export const ArticleVote = Vote({
  form: Forms.VoteForm,
  votescount: ArticleVotescount,
  id: ArticleId,
  hasVoted: HasUserVotedArticle,
  justVoted: HasUserJustVotedArticle,
});
