import { pipe, map } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';

export const ExistingAccounts = (
  { action = null, replaceByGet = null, ...props } = {},
  ...children
) =>
  pipe(
    ({ StoredTwoFa }) => StoredTwoFa,
    map((storedTwoFa) =>
      storedTwoFa.map(
        ({ value }) =>
          ({ routes }) =>
            Form(
              {
                ...props,
                name: Forms.LoginForm,
                action: action ?? routes.login.getHref(),
              },
              ...children,
              ({ html, translate }) =>
                html`<fieldset class="existing-accounts__item">
                  <input
                    type="hidden"
                    name="replaceByGet"
                    value="${replaceByGet}"
                  />
                  <input type="hidden" name="email" value="${value}" />
                  <button
                    type="submit"
                    class="button button--secondary existing-accounts__button"
                  >
                    <span class="existing-accounts__text">${value}</span>
                    ${translate('login.SUBMIT')}
                  </button>
                </fieldset>`
            )
      )
    )
  );
