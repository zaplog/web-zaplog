import { pipe, map } from 'create-async-generator';
import { RouterLink } from '../../../lib/router/router-link.js';

export const Navigation =
  ({ className }, ...items) =>
  ({ html }) =>
    html`<ol class="navigation ${className}">
      ${items.map((item) =>
        pipe(
          item,
          map(
            ({ children, className, activeClass, ...params }) => html`<li>
              ${RouterLink(
                {
                  classes: `navigation__item ${className}`,
                  activeClass: activeClass ?? 'link--active',
                  ...params,
                },
                children
              )}
            </li>`
          )
        )
      )}
    </ol>`;
