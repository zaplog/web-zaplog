import { Header } from '../header/header.js';
import { Footer } from '../footer/footer.js';
import { Search } from '../search/search.js';

export const Page =
  ({ search, main, aside, className }) =>
  ({ html }) =>
    html`<div class="base page ${className}">
      <header class="page__header">${Header}</header>
      ${search !== false
        ? html`<section class="page__search">${Search}</section>`
        : null}
      <aside class="page__aside">${aside}</aside>
      <main class="page__content">${main}</main>
      <footer class="page__footer">${Footer}</footer>
    </div>`;
