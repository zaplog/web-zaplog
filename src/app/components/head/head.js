import { ArticleMeta } from '../article-meta/article-meta.js';
import { AtomLink } from '../../resources/atom-link.js';
import { CanonicalUrl } from '../../resources/canonical-url.js';
import { MetaDescription } from '../../resources/meta-description.js';
import { MetaImage } from '../../resources/meta-image.js';
import { MetaTitle } from '../../resources/meta-title.js';
import { SiteTitle } from '../../resources/site-settings.js';
import { UserLanguage } from '../../resources/user-locale.js';

export const Head =
  (_props, ...children) =>
  ({ html, location, configuration }) =>
    html`<meta charset="utf-8" />
      <meta name="language" content="${UserLanguage}" />
      <meta http-equiv="content-type" content="text/html" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content="${SiteTitle}" />
      <meta name="twitter:site" content="${SiteTitle}" />
      <meta property="og:url" content="${CanonicalUrl}" />
      <meta name="twitter:url" content="${CanonicalUrl}" />
      <meta name="twitter:card" content="summary_large_image" />
      <meta property="og:title" content="${MetaTitle}" />
      <meta name="twitter:title" content="${MetaTitle}" />
      <meta property="og:image" content="${MetaImage}" />
      <meta name="twitter:image" content="${MetaImage}" />
      <meta name="description" content="${MetaDescription}" />
      <meta property="og:description" content="${MetaDescription}" />
      <meta name="twitter:description" content="${MetaDescription}" />
      <title>${MetaTitle}</title>
      <link rel="canonical" href="${CanonicalUrl}" />
      <link rel="alternate" type="application/atom+xml" href="${AtomLink}" />
      <link rel="icon" href="/favicon.ico" type="image/x-icon" />
      ${[
        async function* ({ router, error, routes }) {
          if (!error) {
            for await (const { route } of router) {
              if (route === routes.article) {
                yield ArticleMeta;
              } else {
                yield null;
              }
            }
          }
        },
        html`<link rel="stylesheet" href="/styles/main.css" />`,
        html`<link
          rel="stylesheet"
          href="/styles/${configuration.theme}.css"
        />`,
        configuration.watch
          ? html`<script src="${location.protocol}//${location.hostname}:35729/livereload.js"></script>`
          : null,
        html`<script type="module" src="/browser/markdown.js"></script>`,
        html`<script type="module" src="/browser/trigger-find.js"></script>`,
        html`<script
          type="module"
          src="/browser/zaplog-markdown-textarea/zaplog-markdown-textarea.js"
        ></script>`,
        ...children,
      ]}`;
