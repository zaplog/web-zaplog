import { ChannelEditAdmin } from '../channel-edit-admin/channel-edit-admin.js';
import {
  ChannelAlgorithm,
  ChannelBio,
  ChannelBitcoinaddress,
  ChannelHeader,
  ChannelLanguage,
  ChannelResourceName,
  IsAdminChannel,
} from '../../resources/channel.js';

export const ChannelEditFields = ({ html, translate, Textarea }) =>
  html`<dl class="description-list">
    <dt class="description-list__term">
      <label for="nameField">${translate('channel.NAME')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      <input
        class="form-field fieldset__full-width"
        id="nameField"
        type="text"
        name="name"
        required
        pattern="^[a-zA-Z0-9_-]+$"
        title="Channel name should consist of alphanumeric characters, hyphens or underscores"
        value="${ChannelResourceName}"
        placeholder="${translate('channel.NAME')}"
      />
    </dd>
    <dt class="description-list__term">
      <label for="avatarField">${translate('channel.AVATAR_URL')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      <input
        class="form-field fieldset__full-width"
        id="avatarField"
        type="url"
        name="avatar"
        placeholder="${translate('channel.AVATAR_URL')}"
      />
    </dd>
    <dt class="description-list__term">
      <label for="headerField">${translate('channel.HEADER_URL')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      <input
        class="form-field fieldset__full-width"
        id="headerField"
        type="url"
        name="header"
        value="${ChannelHeader}"
        placeholder="${translate('channel.HEADER_URL')}"
      />
    </dd>
    <dt class="description-list__term">
      <label for="bitcoinaddressField"
        >${translate('channel.BITCOIN_ADDRESS')}</label
      >
    </dt>
    <dd class="description-list__description fieldset">
      <input
        class="form-field fieldset__full-width"
        id="bitcoinaddressField"
        type="text"
        name="bitcoinaddress"
        value="${ChannelBitcoinaddress}"
        placeholder="${translate('channel.BITCOIN_ADDRESS')}"
      />
    </dd>
    ${async function* (app) {
      for await (const isAdminChannel of IsAdminChannel(app)) {
        yield isAdminChannel
          ? ChannelEditAdmin
          : html`<input
                type="hidden"
                name="algorithm"
                value="${ChannelAlgorithm}"
              />
              <input
                type="hidden"
                name="language"
                value="${ChannelLanguage}"
              /> `;
      }
    }}
    <dt class="description-list__term">
      <label for="bioField">${translate('channel.BIO')}</label>
    </dt>
    <dd class="description-list__description fieldset">
      ${Textarea(
        {
          class: 'form-field form-field--textarea fieldset__full-width',
          id: 'bioField',
          name: 'bio',
          placeholder: translate('channel.BIO'),
          rows: '10',
          maxlength: 255,
        },
        ChannelBio
      )}
    </dd>
  </dl>`;
