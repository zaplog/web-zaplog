import { RouterLink } from '../../../lib/router/router-link.js';
import { Notification } from '../notification/notification.js';
import { ChannelName } from '../../resources/channel-name.js';

export const ChannelDetailEmpty = [
  Notification({
    icon: 'success',
    description: ({ translate }) => translate('channel.WELCOME_INTRO'),
  }),
  ({ html, routes, translate }) =>
    html`<ul class="bullet-list">
      <li class="bullet-list__item">
        ${translate('channel.WELCOME_1', {
          channelLink: RouterLink(
            {
              route: routes.channelEdit,
              params: { channel: ChannelName },
              classes: 'link',
            },
            translate('channel.YOUR_CHANNEL')
          ),
        })}
      </li>
      <li class="bullet-list__item">${translate('channel.WELCOME_2')}</li>
      <li class="bullet-list__item">
        ${translate('channel.WELCOME_3', {
          markdownLink: html`<a
            target="_blank"
            href="https://gitlab.com/zaplog/api-zaplog/-/wikis/markdown-cheatsheet"
            class="link"
          >
            ${translate('channel.MARKDOWN_FORMATTING')}
          </a>`,
        })}
      </li>
      <li class="bullet-list__item">${translate('channel.WELCOME_4')}</li>
      <li class="bullet-list__item">
        ${translate('channel.WELCOME_5', {
          externalEmbeds: html`<a
            target="_blank"
            href="https://gitlab.com/zaplog/api-zaplog/-/wikis/images-videos"
            class="link"
          >
            ${translate('channel.EXTERNAL_PLATFORM')}
          </a>`,
        })}
      </li>
      <li class="bullet-list__item">${translate('channel.WELCOME_6')}</li>
    </ul>`,
];
