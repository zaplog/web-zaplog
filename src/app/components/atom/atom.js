import { AtomFeed } from '../atom-feed/atom-feed.js';
import {
  ChannelLinks,
  ChannelLinksLastModifiedIsoString,
} from '../../resources/channel-links.js';
import {
  FrontpageLastModifiedIsoString,
  FrontpageTrendinglinks,
} from '../../resources/frontpage.js';
import {
  LinksItems,
  LinksLastModifiedIsoString,
} from '../../resources/links.js';

export const Atom = async function* ({ routes, router }) {
  for await (const { params, route } of router) {
    switch (route) {
      case routes.homeAtom:
        yield AtomFeed({
          htmlUrl: `${routes.home.getHref()}`,
          pagination: false,
          entries: FrontpageTrendinglinks,
          lastModified: FrontpageLastModifiedIsoString,
        });
        break;
      case routes.postsAtom:
        yield AtomFeed({
          htmlUrl: routes.posts.getHref(params),
          pagination: true,
          entries: LinksItems,
          lastModified: LinksLastModifiedIsoString,
        });
        break;
      case routes.channelAtom:
        yield AtomFeed({
          htmlUrl: `${routes.channel.getHref({
            channel: params.channel,
          })}`,
          pagination: true,
          entries: ChannelLinks,
          lastModified: ChannelLinksLastModifiedIsoString,
        });
        break;
    }
  }
};
