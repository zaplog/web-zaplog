import { pipe, map } from 'create-async-generator';
import { Reaction } from '../reaction/reaction.js';

export const Reactions =
  ({ size, items }) =>
  ({ html }) =>
    html`<ol class="reactions">
      ${pipe(
        items,
        map((itemsList) =>
          itemsList.slice(0, size ?? itemsList.length).map(
            ({ channelid, ...rest }) =>
              html`<li class="reactions__item">
                ${Reaction({
                  channelid,
                  ...rest,
                })}
              </li>`
          )
        )
      )}
    </ol>`;
