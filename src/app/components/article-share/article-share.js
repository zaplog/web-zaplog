import { ArticleShareUrl } from '../../resources/article.js';

export const ArticleShare = ({ html, Script, htmlSafe }) => html`<textarea
    class="article-share__hidden-text-field"
    id="copy_text"
  >
${ArticleShareUrl}</textarea
  >
  ${Script(
    null,
    htmlSafe(`function copyToClipboard() {
          var copyText = document.getElementById('copy_text');
          copyText.select();
          document.execCommand('copy');
          alert('Copied to clipboard');
        }`)
  )}
  <a
    href="javascript:void(0)"
    onclick="copyToClipboard()"
    class="icon icon--s icon--clipboard article-share__item icon--link"
    target="_blank"
  ></a>
  <a
    href="https://api.whatsapp.com/send?text=${ArticleShareUrl}"
    class="icon icon--s icon--whatsapp article-share__item icon--link"
    target="_blank"
  ></a>
  <a
    href="https://www.facebook.com/sharer.php?u=${ArticleShareUrl}"
    class="icon icon--s icon--facebook article-share__item icon--link"
    target="_blank"
  ></a>
  <a
    href="https://www.twitter.com/share?url=${ArticleShareUrl}"
    class="icon icon--s icon--twitter article-share__item icon--link"
    target="_blank"
  ></a>
  <a
    href="https://telegram.me/share/url?url=${ArticleShareUrl}"
    class="icon icon--s icon--telegram article-share__item icon--link"
    target="_blank"
  ></a>`;
