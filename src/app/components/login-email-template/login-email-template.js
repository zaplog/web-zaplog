import { EmailTemplate } from '../email-template/email-template.js';

export const LoginEmailTemplate =
  ({ subject, loginurl }) =>
  ({ html, translate }) =>
    EmailTemplate(
      {
        subject,
      },
      html`<table
        width="100%"
        cellpadding="0"
        cellspacing="0"
        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"
      >
        <tr
          style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"
        >
          <td
            class="content-block"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
            valign="top"
          >
            ${translate('emailTemplate.login.LOGIN_DESCRIPTION')}
          </td>
        </tr>
        <tr
          style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"
        >
          <td
            class="content-block"
            itemprop="handler"
            itemscope
            itemtype="http://schema.org/HttpActionHandler"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
            valign="top"
          >
            <a
              href="${loginurl}{{logintoken}}"
              class="btn-primary"
              itemprop="url"
              style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;"
            >
              ${translate('emailTemplate.login.LOGIN_ACTION')}
            </a>
          </td>
        </tr>
        <tr
          style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"
        >
          <td
            class="content-block"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
            valign="top"
          >
            ${translate('emailTemplate.login.EMAIL_EXPIRY')}
          </td>
        </tr>
        <tr
          style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"
        >
          <td
            class="content-block"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
            valign="top"
          >
            <h1>${translate('emailTemplate.login.TERMS_AND_INSTRUCTIONS')}</h1>
            <ul>
              <li>${translate('emailTemplate.login.TERM1')}</li>

              <li>${translate('emailTemplate.login.TERM2')}</li>
              <li>${translate('emailTemplate.login.TERM3')}</li>
              <li>
                ${translate('emailTemplate.login.TERM4', {
                  communityLink: html`<a
                    href="https://matrix.to/#/#zaplog.pro:zaplog.chat"
                    >https://matrix.to/#/#zaplog.pro:zaplog.chat</a
                  >`,
                })}
              </li>
              <li>${translate('emailTemplate.login.TERM5')}</li>
              <li>${translate('emailTemplate.login.TERM6')}</li>
              <li>${translate('emailTemplate.login.TERM7')}</li>
              <li>
                <b>
                  ${translate('emailTemplate.login.TERM8', {
                    privacyPolicyLink: html`<a
                      href="https://gitlab.com/zaplog/api-zaplog/-/wikis/privacy-policy"
                      >privacy policy</a
                    >`,
                  })}
                </b>
              </li>
            </ul>
          </td>
        </tr>
      </table>`
    );
