export const Dialog =
  ({
    backdropClass,
    dialogClass,
    headerClass,
    contentClass,
    actionsClass,
    header,
    content,
    actions,
  }) =>
  ({ html }) =>
    html`<div class="dialog ${backdropClass} base">
      <dialog class="dialog__container ${dialogClass}" open>
        <div class="dialog__header ${headerClass}">${header}</div>
        <div class="dialog__content ${contentClass}">${content}</div>
        <div class="button-row dialog__actions ${actionsClass}">${actions}</div>
      </dialog>
    </div>`;
