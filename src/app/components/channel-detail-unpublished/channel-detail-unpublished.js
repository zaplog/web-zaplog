import { Form } from '../../../lib/forms/form.js';
import { RouterLink } from '../../../lib/router/router-link.js';
import { TransferConceptDialog } from '../transfer-concept-dialog/transfer-concept-dialog.js';
import { Forms } from '../../forms.js';
import { Concepts } from '../../resources/concepts.js';
import { ChannelLink } from '../channel-link/channel-link.js';

export const ChannelDetailUnpublished = ({ html, routes }) => html`<div
  class="channel-detail-unpublished"
>
  ${async function* ({ transferConceptSelectChannel }) {
    for await (const linkId of transferConceptSelectChannel) {
      yield linkId ? TransferConceptDialog({ linkId }) : null;
    }
  }}
  <ol>
    ${async function* (app) {
      for await (const concepts of Concepts(app)) {
        yield concepts.map(
          ({ id, title, channelname, channelavatar, own }) => html`<li
            class="channel-detail-unpublished__item"
          >
            ${RouterLink(
              {
                route: routes.article,
                params: {
                  id,
                  title,
                  channel: channelname,
                },
                classes: 'link link--plain channel-detail-unpublished__link',
              },
              html`<span class="channel-detail-unpublished__prefix">
                  <span
                    class="icon icon--s icon--article channel-detail-unpublished__icon"
                  ></span
                  >${({ translate }) => translate('article.CONCEPT')}</span
                >
                <span class="channel-detail-unpublished__title">${title}</span>`
            )}
            ${own
              ? Form(
                  {
                    name: Forms.TransferConceptSelectChannel,
                    class: 'channel-detail-unpublished__form',
                  },

                  ({ translate }) =>
                    html`<input type="hidden" name="linkId" value="${id}" />
                      <button type="submit" class="button button--xs">
                        ${translate('article.TRANSFER')}
                      </button>`
                )
              : ChannelLink({ name: channelname, avatar: channelavatar })}
          </li>`
        );
      }
    }}
  </ol>
</div>`;
