import { pipe, map } from 'create-async-generator';
import { Notification } from '../notification/notification.js';
import { Page } from '../page/page.js';
import { ChannelHeader } from '../channel-header/channel-header.js';
import { ChannelLink } from '../channel-link/channel-link.js';
import { Form } from '../../../lib/forms/form.js';
import { Forms } from '../../forms.js';
import {
  ChannelMemberships,
  ChannelResourceName,
} from '../../resources/channel.js';
import { Entries } from '../entries/entries.js';
import { Pagination } from '../pagination/pagination.js';

export const ChannelMembershipsPage = Page({
  className: 'channel-memberships page--column',
  search: false,
  main: [
    ({ html }) =>
      html`<div class="channel-members__header">${ChannelHeader}</div>`,
    async function* ({ html, translate, ...rest }) {
      for await (const channelMemberships of ChannelMemberships({
        html,
        translate,
        ...rest,
      })) {
        yield channelMemberships.length === 0
          ? Notification({
              title: translate('channel.CHANNEL_MEMBERSHIPS_EMPTY_TITLE'),
              icon: 'empty',
              description: translate(
                'channel.CHANNEL_MEMBERSHIPS_EMPTY_DESCRIPTION'
              ),
            })
          : html`<ol class="channel-memberships__memberships">
                ${channelMemberships.map(
                  ({ name, avatar }) => html`<li
                    class="channel-memberships__item"
                  >
                    ${Form(
                      {
                        name: Forms.DeleteChannelMembershipForm,
                        class: 'channel-memberships__membership',
                      },
                      pipe(
                        ChannelResourceName,
                        map(
                          (channelName) => html`<input
                            type="hidden"
                            name="channelName"
                            value="${channelName}"
                          />`
                        )
                      ),
                      html`<input
                        type="hidden"
                        name="channelMembership"
                        value="${name}"
                      />`,
                      ChannelLink({
                        name,
                        avatar,
                        className: 'channel-memberships__channel',
                      }),
                      html`<button type="submit" class="button button--xs">
                        ${translate('DELETE')}
                      </button>`
                    )}
                  </li>`
                )}
              </ol>
              ${Entries({
                links: ({ MembershipsLinks }) => MembershipsLinks,
              })}
              ${Pagination}`;
      }
    },
  ],
});
