import { pipe, map } from 'create-async-generator';
import { Entry } from '../entry/entry.js';

export const Entries =
  ({ links, hideDescription, className }) =>
  ({ html }) =>
    html`<ol class="entries ${className}">
      ${pipe(
        links,
        map((linksList) =>
          linksList.map(
            (link) => html`<li class="entries__item">
              <article
                itemscope
                itemtype="http://schema.org/Article"
                class="entries__article"
              >
                ${Entry({ ...link, hideDescription })}
              </article>
            </li>`
          )
        )
      )}
    </ol>`;
