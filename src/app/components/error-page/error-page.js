import { PageStandalone } from '../page-standalone/page-standalone.js';
import { Error } from '../error/error.js';
import { Errors } from '../../models.js';

export const ErrorPage = PageStandalone(
  {},
  ({ error, translate, configuration }) => {
    switch (error.message) {
      case Errors.MembersonlyArticle:
        return Error({
          title: translate('article.MEMBERSHIP_REQUIRED_TITLE'),
          description: translate('article.MEMBERSHIP_REQUIRED_DESCRIPTION'),
        });
      default:
        return configuration.production ? error : error.stack;
    }
  }
);
