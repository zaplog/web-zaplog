import { pipe, map } from 'create-async-generator';
import { Form } from '../../../lib/forms/form.js';
import { RouterLink } from '../../../lib/router/router-link.js';
import { ChannelMemberships } from '../../resources/channel.js';
import { Notification } from '../notification/notification.js';
import { Forms } from '../../forms.js';
import { ChannelName } from '../../resources/channel-name.js';
import { Dialog } from '../dialog/dialog.js';

export const TransferConceptDialog =
  ({ linkId }) =>
  ({ translate }) =>
    Dialog({
      dialogClass: 'transfer-concept-dialog',
      header: translate('article.TRANSFER_ARTICLE_TO'),
      actions: ({ routes, translate }) =>
        RouterLink(
          {
            route: routes.channel,
            params: {
              channel: ChannelName,
            },
            classes: 'button button--warning',
          },
          translate('CANCEL')
        ),
      content: ({ html, translate }) =>
        pipe(
          ChannelMemberships,
          map((channelMemberships) =>
            channelMemberships.length === 0
              ? Notification({
                  title: translate('channel.CHANNEL_MEMBERSHIPS_EMPTY_TITLE'),
                  icon: 'empty',
                  description: translate(
                    'channel.CHANNEL_MEMBERSHIPS_EMPTY_DESCRIPTION'
                  ),
                })
              : html`<ol class="transfer-concept-dialog__memberships">
                  ${channelMemberships.map(
                    ({ name, avatar }) => html`<li
                      class="transfer-concept-dialog__item"
                    >
                      ${Form(
                        {
                          name: Forms.TransferConcept,
                          class: 'transfer-concept-dialog__membership',
                        },
                        html`<input
                          type="hidden"
                          name="linkId"
                          value=${linkId}
                        />`,
                        html`<input
                          type="hidden"
                          name="channelName"
                          value="${name}"
                        />`,
                        html`<button
                          type="submit"
                          class="transfer-concept-dialog__channel channel button button--xs button--unstyled"
                        >
                          <img
                            src="${avatar}"
                            class="channel__avatar"
                            alt="${name}"
                            title="${name}"
                          />
                          <span>@${name}</span>
                        </button>`
                      )}
                    </li>`
                  )}
                </ol>`
          )
        ),
    });
