import { render } from '@async-to-html/render/render.js';
import { STRING_RENDERERS } from '@async-to-html/string/renderers/get-default-renderers.js';
import { createAsyncGenerator } from 'create-async-generator';
import { encode } from 'html-entities';
import { isAsyncIterator } from '@async-to-html/render/matchers/is-async-iterator.js';
import { isPromise } from '@async-to-html/render/matchers/is-promise.js';
import { isFunction } from '@async-to-html/render/matchers/is-function.js';
import { renderPromiseFactory } from '@async-to-html/render/renderers/render-promise-factory.js';
import { renderFunctionFactory } from '@async-to-html/render/renderers/render-function-factory.js';
import { renderAsyncIteratorFactory } from '@async-to-html/render/renderers/render-async-iterator-factory.js';

export const OBJECT_RENDERERS = [
  [isAsyncIterator, renderAsyncIteratorFactory],
  [isPromise, renderPromiseFactory],
  [isFunction, renderFunctionFactory],
  [() => true, (item) => item],
];

export const middleware =
  ({ configuration, App, Headers, StatusCode, Entrypoint }) =>
  (req, res, next) => {
    const runAsync = async (asyncFn) => {
      await asyncFn();
    };

    const runState = async (stateFn, x) => {
      let latestResult;
      if (!res.headersSent) {
        for await (const result of createAsyncGenerator(stateFn)({ ...x })) {
          latestResult = result;
          if (res.headersSent) {
            return;
          }
        }
      }
      return {
        async *[Symbol.asyncIterator]() {
          if (latestResult) {
            yield latestResult;
          }
        },
      };
    };

    const renderApp = async ({ error } = {}) => {
      const app = await App({
        req,
        res,
        configuration,
        runAsync,
        runState,
        error: error ?? null,
      });
      if (res.headersSent) {
        return {};
      }

      const html = await render(Entrypoint, {
        args: [app],
        safe: true,
        renderers: STRING_RENDERERS,
        encode,
      });
      const headers = await render(Headers, {
        args: [app],
        renderers: OBJECT_RENDERERS,
      });
      const statusCode = await render(StatusCode, {
        args: [app],
        renderers: OBJECT_RENDERERS,
      });
      return { html, headers, statusCode };
    };

    (async () => {
      try {
        const { html, headers, statusCode } = await renderApp();
        if (statusCode) {
          res.status(statusCode);
        }
        if (headers) {
          res.set(headers);
        }
        if (html) {
          res.send(html);
        }
      } catch (error) {
        const { html, headers, statusCode } = await renderApp({
          error,
        });
        if (statusCode) {
          res.status(statusCode);
        }
        if (headers) {
          res.set(headers);
        }
        if (html) {
          res.send(html);
        }
      }
    })()
      .then(() => next())
      .catch((error) => {
        console.error(new Date().toString(), error);
        console.log('request user-agent', req.get('User-Agent'));
        console.log('request url', req.originalUrl);
        console.log('request referrer', req.get('Referrer'));
        next(error);
      });
  };
