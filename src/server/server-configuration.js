import { argv } from 'node:process';
import { join } from 'path';
import dotenv from 'dotenv';

const args = argv.slice(2);
const watch = args.indexOf('--watch') !== -1;
const browserRendering = args.indexOf('--browserRendering') !== -1;
dotenv.config();
const host = process.env.ZAPLOG_FRONTEND_HOST;
const port = process.env.ZAPLOG_FRONTEND_PORT;
const rootPath = process.env.ZAPLOG_PATH
  ? join(process.cwd(), process.env.ZAPLOG_PATH)
  : process.cwd();

export const serverConfiguration = {
  host,
  port,
  rootPath,
  apiUrl: process.env.ZAPLOG_API_URL,
  apiKey: process.env.ZAPLOG_API_KEY,
  availableLanguages: process.env.ZAPLOG_AVAILABLE_LANGUAGES.split(','),
  theme: process.env.ZAPLOG_THEME ? process.env.ZAPLOG_THEME : 'osmode',
  production: process.env.NODE_ENV === 'production',
  i18nPath: join(rootPath, 'src', 'assets', 'i18n'),
  configSet: false,
  channelSubdomain: process.env.ZAPLOG_CHANNEL_SUBDOMAIN === 'true',
  community: process.env.ZAPLOG_COMMUNITY === 'true',
  adminChannelId: 1,
  watch,
  browserRendering,
};
