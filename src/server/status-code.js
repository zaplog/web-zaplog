const getErrorStatusCode = (error) => {
  const statusCode = error?.status ?? error?.statusCode ?? null;
  return statusCode < 400 || statusCode > 599 ? 500 : statusCode;
};

export const StatusCode = async ({
  router,
  error,
  addChannelMemberForm,
  channelEditForm,
  commentForm,
  createArticleForm,
  publishArticleForm,
  myChannelRoute,
}) => {
  if (error) {
    return getErrorStatusCode(error);
  }
  for await (const addChannelMember of addChannelMemberForm) {
    if (addChannelMember.error) {
      return getErrorStatusCode(addChannelMember.error);
    }
  }
  for await (const channelEdit of channelEditForm) {
    if (channelEdit.error) {
      return getErrorStatusCode(channelEdit.error);
    }
  }
  for await (const comment of commentForm) {
    if (comment.error) {
      return getErrorStatusCode(comment.error);
    }
  }
  for await (const createArticle of createArticleForm) {
    if (createArticle.error) {
      return getErrorStatusCode(createArticle.error);
    }
  }
  for await (const publishArticle of publishArticleForm) {
    if (publishArticle.error) {
      return getErrorStatusCode(publishArticle.error);
    }
  }
  for await (const myChannel of myChannelRoute) {
    if (myChannel) {
      return 401;
    }
  }
  for await (const { route } of router) {
    if (!route) {
      return 404;
    }
  }
  return 200;
};
