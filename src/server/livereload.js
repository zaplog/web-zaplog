import livereload from 'livereload';

export default ({ path }) => {
  const livereloadServer = livereload.createServer({
    exts: [
      'js',
      'html',
      'css',
      'png',
      'gif',
      'jpg',
      'ico',
      'svg',
      'woff',
      'woff2',
    ],
    delay: 1000,
  });
  livereloadServer.watch(path);
  livereloadServer.server.once('connection', () => {
    setTimeout(() => {
      livereloadServer.refresh('/');
    }, 100);
  });
};
