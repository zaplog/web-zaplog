export const ContentType = async function* ({ routes, router }) {
  for await (const { route } of router) {
    switch (route) {
      case routes.homeAtom:
      case routes.postsAtom:
      case routes.channelAtom:
        yield 'application/atom+xml';
        break;
      case routes.rawArticle:
        yield 'text/markdown';
        break;
      default:
        yield 'text/html';
        break;
    }
  }
};
