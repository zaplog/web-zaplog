import { join } from 'path';
import { readFile } from 'fs/promises';
import { URL, URLSearchParams } from 'url';
import { encode, decode } from 'html-entities';
import { html as htmlFactory } from '@async-to-html/string/html.js';
import { createElement as createElementFactory } from '@async-to-html/string/create-element.js';
import { safeHtml } from '@async-to-html/render/safe-html/safe-html.js';
import { createCacheControlFetch } from 'cache-control-fetch';
import { parseAcceptLanguageHeader } from 'parse-accept-language-header';
import { ExpressCookies } from 'web-storage-express-cookies';
import { ExpressRouter } from '../lib/express-router/express-router.js';
import * as FormsModule from '../lib/express-forms/index.js';
import { createLink } from '../lib/express-router/create-link.js';
import { App } from '../app/app.js';
import { getTld } from '../app/routing/get-tld.js';

const createElement = createElementFactory();

export const ServerApp = ({ req, res, configuration, ...props }) => {
  res.vary('accept-language');
  const location = new URL(
    `${req.protocol}://${req.get('host')}${req.originalUrl}`
  );
  return App({
    ...props,
    encode,
    decode,
    ...FormsModule,
    html: htmlFactory(),
    createElement,
    htmlSafe: safeHtml,
    Textarea: (props, value) => createElement('textarea', props, value),
    Script: (props, script) => createElement('script', props, script),
    // TODO: make sure req and res do not need to be passed around
    req,
    res,
    configuration,
    location,
    ...createCacheControlFetch({
      fetch,
      Request: global.Request,
      globalOptions: { highWaterMark: 10 * 1024 * 1024 * 1024 * 1024 },
    }),
    URL,
    URLSearchParams,
    Request: global.Request,
    console,
    Intl,
    context: global,
    acceptLanguage: parseAcceptLanguageHeader(req.get('accept-language')),
    referrer: req.get('Referrer') ? new URL(req.get('Referrer')) : null,
    translations: (async () => {
      const languages = await Promise.all(
        configuration.availableLanguages.map(async (language) => ({
          [language]: JSON.parse(
            await readFile(
              `${join(configuration.i18nPath, language)}.json`,
              'utf8'
            )
          ),
        }))
      );
      return languages.reduce(
        (acc, curr) => ({
          ...acc,
          ...curr,
        }),
        {}
      );
    })(),
    createLink,
    router: new ExpressRouter({ res, location, URL }),
    storage: new ExpressCookies({
      req,
      res,
      setVaryHeader: true,
      options: {
        domain: `${configuration.channelSubdomain ? '.' : ''}${getTld(
          location,
          false
        )}`,
        path: '/',
        httpOnly: false,
        secure: false,
      },
    }),
  });
};
