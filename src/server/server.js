import { setMaxListeners } from 'node:events';
import { join } from 'path';
import express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import { createProxyMiddleware } from 'http-proxy-middleware';
import { middleware } from './middleware.js';
import { serverConfiguration } from './server-configuration.js';
import { ServerApp } from './server-app.js';
import { Entrypoint } from './entrypoint.js';
import { Headers } from './headers.js';
import { StatusCode } from './status-code.js';

const staticOptions = {
  maxAge: 1800000,
};
export const app = express();
const src = join(serverConfiguration.rootPath, 'src');
// TODO: investigate
setMaxListeners(100);

if (serverConfiguration.watch) {
  import('./livereload.js').then((module) => {
    module.default({ path: join(serverConfiguration.rootPath, 'src') });
  });
}

if (serverConfiguration.browserRendering) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: serverConfiguration.apiUrl,
      changeOrigin: true,
      secure: false,
      logLevel: 'debug',
      pathRewrite: {
        '^/api': '',
      },
      headers: {
        ['X-Api-Key']: serverConfiguration.apiKey,
      },
    })
  );
}
app.set('trust proxy', true);
app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(
  '/favicon.ico',
  express.static(join(src, 'assets', 'favicon.ico'), staticOptions)
);
app.use(
  '/bootstrap-icons',
  express.static(
    join(serverConfiguration.rootPath, 'node_modules', 'bootstrap-icons'),
    staticOptions
  )
);
app.use('/app', express.static(join(src, 'app'), staticOptions));
app.use('/browser', express.static(join(src, 'browser'), staticOptions));
app.use('/assets', express.static(join(src, 'assets'), staticOptions));
app.use('/styles', express.static(join(src, 'styles'), staticOptions));
app.get('/robots.txt', (_req, res) => {
  if (serverConfiguration.production) {
    res.send(
      `User-agent: *
Disallow:`
    );
  } else {
    res.send(
      `User-agent: *
Disallow: /`
    );
  }
});

if (serverConfiguration.browserRendering) {
  const html = `<script src="/browser/output/browser.js" type="module"></script>`;
  app.use('/lib', express.static(join(src, 'lib'), staticOptions));
  app.get('/*', (_req, res) => {
    res.send(html);
  });
} else {
  app.use(
    middleware({
      configuration: serverConfiguration,
      App: ServerApp,
      Entrypoint,
      Headers,
      StatusCode,
    })
  );
}

export const server = app.listen(
  serverConfiguration.port,
  serverConfiguration.host,
  () => {
    console.log(
      `Zaplog frontend listening at http://${serverConfiguration.host}:${serverConfiguration.port}`
    );
  }
);
