import { Atom } from '../app/components/atom/atom.js';
import { Head } from '../app/components/head/head.js';
import { Body } from '../app/components/body/body.js';
import { ArticleMarkdown } from '../app/resources/article.js';
import { UserLanguage } from '../app/resources/user-locale.js';

export const Entrypoint = async function* ({ html, router, routes }) {
  for await (const { route } of router) {
    switch (route) {
      case routes.homeAtom:
      case routes.postsAtom:
      case routes.channelAtom:
        yield Atom;
        break;
      case routes.rawArticle:
        yield ArticleMarkdown;
        break;
      default:
        yield html`<!DOCTYPE html>
          <html lang="${UserLanguage}">
            <head>
              ${Head()}
            </head>
            <body class="body background">
              ${Body()}
            </body>
          </html>`;
        break;
    }
  }
};
