import { combineObject } from '../lib/combine-object.js';
import { ContentType } from './content-type.js';

export const Headers = ({ configuration, getCacheControlHeader }) =>
  combineObject({
    'Content-Type': ContentType,
    'Cache-Control': getCacheControlHeader(),
    ...(configuration.production
      ? {
          'Strict-Transport-Security': 'max-age=63072000',
          'Content-Security-Policy':
            "default-src https:; frame-ancestors 'self'; img-src blob: data: https:",
          'X-Content-Type-Options': 'nosniff',
          'X-Frame-Options': 'SAMEORIGIN',
          'X-XSS-Protection': '1; mode=block',
        }
      : null),
  });
