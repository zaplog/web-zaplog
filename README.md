# Zaplog Web Frontend

This front-end is built for the ZapLog REST-API at: https://github.com/zaplogv2/api.zaplog

This application is a prototype. It runs server-side Node.js pages. As conventional / coder-accessible as possible.

The purpose of the prototype is to rebuild the original ZapLog (which was PHP/SQL coded in Expression Engine templates)
in modern technology and concepts. Faster, more flexible, less overhead. Zaplog was an award winning crowdsourced
news site with an algorithmic frontpage that successfully ran from 2006 to 2018 consistently averaging 150 to 200k unique visitors/month.

An archived version of the original ZapLog can be found here:

https://web.archive.org/web/20150318093136/http://zaplog.nl/

This WEB/API combination is explicitly not meant to be a general purpose CMS. No fancy pancy but functional and simple.

### Design documentation

See: https://github.com/zaplogv2/doc.zaplog

### Installation

Copy `.env.example` to `.env`

```
npm install
npm run prepare
```

### Usage

- Run `npm start`

