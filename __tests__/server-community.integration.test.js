import { jest } from '@jest/globals';
import dotenv from 'dotenv';
import supertest from 'supertest';
import { Forms } from '../src/app/forms.js';
import getCurrentDateModule from '../src/app/components/i18n/get-current-date.js';
import getTimeZoneModule from '../src/app/components/i18n/get-time-zone.js';
import { Routes } from '../src/app/routing/routes.js';
import { startMockApi, stopMockApi, apiAddress } from '../__mocks__/api.js';

describe('testing GET endpoints', () => {
  const host = 'testingexample.com';
  const baseConfig = {
    ZAPLOG_FRONTEND_HOST: '127.0.0.1',
    ZAPLOG_FRONTEND_PORT: '65532',
    ZAPLOG_API_URL: 'http://localhost:8080/Api.php',
    ZAPLOG_GOTO_URL: 'http://localhost:8080/Api.php/goto?urlencoded=',
    ZAPLOG_DEFAULT_LANGUAGE: 'nl',
    ZAPLOG_AVAILABLE_LANGUAGES: 'en,nl',
    ZAPLOG_TITLE: 'Mindmeld',
    ZAPLOG_IMAGE: '/assets/img/mindmeld-logo-square-web.png',
    ZAPLOG_CHANNEL_SUBDOMAIN: 'true',
    ZAPLOG_COMMUNITY: 'true',
    ZAPLOG_FAVICON: '/favicon.ico',
    // TODO: because the server is not started in the root we need to do this, maybe workaround?
    ZAPLOG_PATH: '/',
    NODE_ENV: 'production',
  };
  let routes;
  let server;
  let app;

  beforeAll(async () => {
    await startMockApi();
    const { address, port } = await apiAddress;
    baseConfig.ZAPLOG_API_URL = `http://${address}:${port}`;
    baseConfig.ZAPLOG_GOTO_URL = `http://${address}:${port}/goto?urlencoded=`;
    dotenv.config = () => {
      process.env = baseConfig;
    };
    const getCurrentDateSpy = jest.spyOn(
      getCurrentDateModule,
      'getCurrentDate'
    );
    getCurrentDateSpy.mockImplementation(
      () => new Date(2021, 10, 22, 21, 21, 21)
    );
    const getTimeZoneSpy = jest.spyOn(getTimeZoneModule, 'getTimeZone');
    getTimeZoneSpy.mockImplementation(() => 'America/New_York');
    const module = await import('../src/server/server.js');
    server = module.server;
    app = supertest(module.app);
    routes = Routes({
      channelHostname: `:channel.${host}`,
      channelPathnamePrefix: '',
      communityHostname: host,
      URLSearchParams: global.URLSearchParams,
      protocol: 'http:',
      configuration: {
        community: true,
        channelSubdomain: true,
      },
    });
  });

  afterAll(async () => {
    await stopMockApi();
    await new Promise((resolve) => {
      server.close(() => {
        console.log('Zaplog frontend stopped');
        resolve();
      });
    });
  });

  describe('in the community version', () => {
    describe('atom feeds and markdown article', () => {
      it('should show the home atom feed', async () => {
        const res = await app
          .get(routes.homeAtom.getPathname())
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should show the posts atom feed', async () => {
        const res = await app
          .get(routes.postsAtom.getPathname())
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should show the channel atom feed', async () => {
        const res = await app
          .get(routes.channelAtom.getPathname())
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should show the article markdown', async () => {
        const res = await app
          .get(
            routes.rawArticle.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });
    });

    describe('as an unauthenticated user', () => {
      it('should reset the twofa cookie when it is not json parseable', async () => {
        const res = await app
          .get(routes.login.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__ZaplogUserTwoFA=j%3A%5B%5D; Domain=.${host}; Path=/`,
          ]);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%5D; Domain=.${host}; Path=/`,
        ]);
      });

      it('should respond with the home page', async () => {
        const res = await app.get(routes.home.getPathname()).set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the posts page', async () => {
        const res = await app.get(routes.posts.getPathname()).set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the second posts page', async () => {
        const res = await app
          .get(routes.posts.getPathname({ page: 2 }))
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page for an empty article', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 39358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the new article page', async () => {
        console.log(routes.newArticle.getPathname());
        const res = await app
          .get(routes.newArticle.getPathname())
          .set('Host', `mockedchannel.${host}`);
        expect(res.status).toBe(401);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the edit article page', async () => {
        const res = await app
          .get(routes.editArticle.getPathname({ id: 29358 }))
          .set('Host', `mockedchannel.${host}`);
        expect(res.status).toBe(401);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the tag index page', async () => {
        const res = await app.get(routes.index.getPathname()).set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel index page', async () => {
        const res = await app
          .get(routes.channelIndex.getPathname())
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the discussion page', async () => {
        const res = await app
          .get(routes.discussion.getPathname())
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the second discussion page', async () => {
        const res = await app
          .get(routes.discussion.getPathname({ page: 2 }))
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-1260' }))
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the second channel page', async () => {
        const res = await app
          .get(
            routes.channel.getPathname({
              channel: 'test-channel-1260',
              page: 2,
            })
          )
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page for an empty channel', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-2260' }))
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel edit page', async () => {
        const res = await app
          .get(routes.channelEdit.getPathname())
          .set('Host', `mockedchannel.${host}`);
        expect(res.status).toBe(401);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the tag page', async () => {
        const res = await app
          .get(`${routes.posts.getPathname()}?query=%239-11`)
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the second tag page', async () => {
        const res = await app
          .get(`${routes.posts.getPathname({ page: 2 })}?query=%239-11`)
          .set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the login page', async () => {
        const res = await app.get(routes.login.getPathname()).set('Host', host);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the login page for a user that was previously logged in', async () => {
        const res = await app
          .get(routes.login.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22test%40testuser.com%22%7D%5D; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should redirect back to the home page when trying to logout', async () => {
        const res = await app
          .get(routes.logout.getPathname())
          .set('Host', host);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.home.getHref());
      });

      it('should redirect with a valid token when trying to do a 2fa with a valid token', async () => {
        const res = await app
          .get(routes.twofa.getPathname({ token: 'valid-mock-2fa-token' }))
          .set('Host', host);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.discussion.getHref());
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
          `__cookie-storage__ZaplogChannelName=test-channel; Domain=.${host}; Path=/`,
          `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
        ]);
      });

      it('should redirect with a not found page', async () => {
        const res = await app.get('/non-existing-page').set('Host', host);
        expect(res.status).toBe(404);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page not found', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-1261' }))
          .set('Host', `test-channel-1261.${host}`);
        expect(res.status).toBe(404);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the tag page not found', async () => {
        const res = await app
          .get(`${routes.posts.getPathname()}?query=%239-10`)
          .set('Host', host);
        expect(res.status).toBe(404);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page not found', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29359,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', host);
        expect(res.status).toBe(404);
        expect(res.text).toMatchSnapshot();
      });

      it('should show the login page when trying to do a 2fa with an invalid token', async () => {
        const res = await app
          .get(routes.twofa.getPathname({ token: 'invalid-mock-2fa-token' }))
          .set('Host', host);
        expect(res.status).toBe(401);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with a 401 when trying to vote', async () => {
        const res = await app
          .post(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.VoteForm })
          .send({ id: '29358' });
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__X-Session-Token=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelName=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelId=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
        ]);
        expect(res.status).toBe(401);
        expect(res.text).toMatchSnapshot();
      });

      it('should log the user in', async () => {
        const res = await app
          .post(routes.login.getPathname())
          .set('Host', host)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'test@testuser.com',
            action: 'create',
          });
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.login.getHref());
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22test%40testuser.com%22%7D%5D; Domain=.${host}; Path=/`,
          `__cookie-storage__twofaLinkSent=twofaLinkSent; Domain=.${host}; Path=/`,
        ]);
      });

      it('should log the user in with an existing account', async () => {
        const res = await app
          .post(routes.login.getPathname())
          .set('Host', host)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'test@testuser.com',
            action: 'existing',
          });
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.login.getHref());
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22test%40testuser.com%22%7D%5D; Domain=.${host}; Path=/`,
          `__cookie-storage__twofaLinkSent=twofaLinkSent; Domain=.${host}; Path=/`,
        ]);
      });

      it('should show the error when logging in fails', async () => {
        const res = await app
          .post(routes.login.getPathname())
          .set('Host', host)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'error@testuser.com',
          });
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22error%40testuser.com%22%7D%5D; Domain=.${host}; Path=/`,
        ]);
      });
    });

    describe('as an authenticated user', () => {
      it('should respond with the article page', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page for a concept', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 49358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page for a user that is not the author', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=2260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page for a concept and a user that is not the author', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 49358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=2260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the article page for an unpublished article', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 49358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the new article page', async () => {
        const res = await app
          .get(routes.newArticle.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with a channel page', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-1260' }))
          .set('Host', `test-channel-1260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1261; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1261; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-1260' }))
          .set('Host', `test-channel-1260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page for an empty channel', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-2260' }))
          .set('Host', `test-channel-2260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=2260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page for a user that has unpublished articles', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-3260' }))
          .set('Host', `test-channel-3260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel page for a signed user that has unpublished articles', async () => {
        const res = await app
          .get(routes.channel.getPathname({ channel: 'test-channel-3260' }))
          .set('Host', `test-channel-3260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=3260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-3260; Domain=.${host}; Path=/`,
          ]);
        // expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel edit page', async () => {
        const res = await app
          .get(routes.channelEdit.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel edit page for an admin', async () => {
        const res = await app
          .get(routes.channelEdit.getPathname())
          .set('Host', `admin.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=admin; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel edit page for an empty channel', async () => {
        const res = await app
          .get(routes.channelEdit.getPathname())
          .set('Host', `test-channel-2260.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=2260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should redirect back to the home page when trying to login', async () => {
        const res = await app
          .get(routes.login.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.home.getHref());
      });

      it('should redirect back to the home page after logout', async () => {
        const res = await app
          .get(routes.logout.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__X-Session-Token=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelName=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelId=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
        ]);
        expect(res.headers.location).toBe(routes.home.getHref());
      });

      it('should redirect back to the home page after logout failed', async () => {
        const res = await app
          .get(routes.logout.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=invalid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__X-Session-Token=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelName=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
          `__cookie-storage__ZaplogChannelId=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
        ]);
        expect(res.headers.location).toBe(routes.home.getHref());
      });

      it('should redirect back to the home page when trying to do a 2fa', async () => {
        const res = await app
          .get(routes.twofa.getPathname({ token: 'valid-mock-2fa-token' }))
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.discussion.getHref());
      });

      it('should redirect back to the article page when trying to vote', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.VoteForm })
          .send({ id: '29358' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__votedLinkId=29358; Domain=.${host}; Path=/`,
        ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toContain(route);
      });

      it('should show an error state when trying to vote fails', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.VoteForm })
          .send({ id: '39358' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
      });

      it('should redirect back to the article page when trying to add a comment', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.CommentForm })
          .send({ action: 'add' })
          .send({ link_id: '29358' })
          .send({ comment: 'test comment' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toContain(`${route}#reaction-9876`);
      });

      it('should show the error when trying to add a comment fails', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', `g-b-wolf.${host}`)
          .type('form')
          .send({ form_id: Forms.CommentForm })
          .send({ action: 'add' })
          .send({ link_id: '29358' })
          .send({ comment: 'failing comment' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
      });

      it('should redirect back to the article page when trying to preview a comment', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', `g-b-wolf.${host}`)
          .type('form')
          .send({ form_id: Forms.CommentForm })
          .send({ action: 'preview' })
          .send({ link_id: '29358' })
          .send({ comment: 'test comment' })
          .set('cookie', [
            '__cookie-storage__X-Session-Token=valid-mock-session-token; Path=/',
            '__cookie-storage__ZaplogChannelId=1260; Path=/',
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should create a new concept', async () => {
        const res = await app
          .post(routes.newArticle.getPathname())
          .set('Host', `test-channel-2160.${host}`)
          .type('form')
          .send({ form_id: Forms.CreateArticleForm })
          .send({ id: 29358 })
          .send({ title: 'Mocked title' })
          .send({ markdown: 'Mocked description' })
          .send({ url: 'https://www.mocksite.com/section/page' })
          .send({ copyright: 'No Rights Apply' })
          .send({ tags: 'mocked keywords' })
          .send({ action: 'create' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2160; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.article.getHref({
            channel: 'test-channel-2160',
            id: 123,
            title: 'Mocked title',
          })
        );
      });

      it('should create a new concept and show the copyright checkbox', async () => {
        const res = await app
          .post(routes.newArticle.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .type('form')
          .send({ form_id: Forms.CreateArticleForm })
          .send({ id: 29358 })
          .send({ title: 'Mocked title' })
          .send({
            markdown:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          })
          .send({ url: 'https://www.mocksite.com/section/page' })
          .send({ copyright: 'No Rights Apply' })
          .send({ tags: 'mocked keywords' })
          .send({ action: 'create' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2160; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should create a new concept when marking the copyright checkbox', async () => {
        const res = await app
          .post(routes.newArticle.getPathname())
          .set('Host', `test-channel-2160.${host}`)
          .type('form')
          .send({ form_id: Forms.CreateArticleForm })
          .send({ id: 29358 })
          .send({ title: 'Mocked title' })
          .send({
            markdown:
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
          })
          .send({ url: 'https://www.mocksite.com/section/page' })
          .send({ copyright: 'No Rights Apply' })
          .send({ copyrightCheck: 'on' })
          .send({ tags: 'mocked keywords' })
          .send({ action: 'create' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-2160; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.article.getHref({
            channel: 'test-channel-2160',
            id: 123,
            title: 'Mocked title',
          })
        );
      });

      it('should publish an article', async () => {
        const route = routes.article.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.PublishArticleForm })
          .send({ id: 29358 })
          .send({ title: 'Mocked title' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.article.getHref({
            id: 29358,
            title: 'Mocked title',
          })
        );
      });

      it('should show an error when publishing an article fails', async () => {
        const route = routes.article.getPathname({
          id: 39358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', `g-b-wolf.${host}`)
          .type('form')
          .send({ form_id: Forms.PublishArticleForm })
          .send({ id: 39358 })
          .send({ title: 'Mocked title error' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
      });

      it('should show a warning when trying to delete an article', async () => {
        const route = routes.articleDelete.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .get(route)
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should delete an article', async () => {
        const route = routes.articleDelete.getPathname({
          id: 29358,
          title: 'drs-karen-hamaker-zondag',
        });
        const res = await app
          .post(route)
          .set('Host', host)
          .type('form')
          .send({ form_id: Forms.DeleteArticleForm })
          .send({ id: '39358' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.channel.getHref({
            channel: 'test-channel-1260',
          })
        );
      });

      it('should create a new article', async () => {
        const res = await app
          .post(routes.newArticle.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .type('form')
          .send({ form_id: Forms.CreateArticleForm })
          .send({ title: 'Mocked title' })
          .send({ markdown: 'Mocked description' })
          .send({ language: 'nl' })
          .send({ url: 'https://www.mocksite.com/section/page' })
          .send({ mimetype: 'text/html' })
          .send({ copyright: 'No Rights Apply' })
          .send({ tags: 'mocked keywords' })
          .send({ image: 'https://www.mocksite.com/section/image.jpg' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.article.getHref({
            channel: 'test-channel-1260',
            id: 123,
            title: 'Mocked title',
          })
        );
      });

      it('should show an error when creating a new article fails', async () => {
        const res = await app
          .post(routes.newArticle.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .type('form')
          .send({ form_id: Forms.CreateArticleForm })
          .send({ markdown: 'Mocked error markdown' })
          .send({ copyright: 'No Rights Apply' })
          .send({ tags: 'mocked keywords' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the channel detail page on channel save', async () => {
        const res = await app
          .post(routes.channelEdit.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .type('form')
          .send({ form_id: Forms.ChannelEditForm })
          .send({ name: 'mocked-channel-name' })
          .send({ avatar: 'https://www.mocksite.com/section/image.jpg' })
          .send({ bio: 'Mocked bio' })
          .send({ channelId: '1260' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(
          routes.channel.getHref({ channel: 'mocked-channel-name' })
        );
      });

      it('should respond with the channel edit page showing an error', async () => {
        const res = await app
          .post(routes.channelEdit.getPathname())
          .set('Host', `test-channel-1260.${host}`)
          .type('form')
          .send({ form_id: Forms.ChannelEditForm })
          .send({ name: 'mocked-erroring-channel-name' })
          .send({ avatar: 'https://www.mocksite.com/section/image.jpg' })
          .send({ bio: 'Mocked bio' })
          .send({ channelId: '1260' })
          .set('cookie', [
            `__cookie-storage__X-Session-Token=valid-mock-session-token; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelId=1260; Domain=.${host}; Path=/`,
            `__cookie-storage__ZaplogChannelName=test-channel-1260; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
      });
    });

    describe('when a form was previously submitted', () => {
      it('should respond with the article page after voting', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${host}`)
          .set('cookie', [
            `__cookie-storage__votedLinkId=29358; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__votedLinkId=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
        ]);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the login page after logging in', async () => {
        const res = await app
          .get(routes.login.getPathname())
          .set('Host', host)
          .set('cookie', [
            `__cookie-storage__twofaLinkSent=twofaLinkSent; Domain=.${host}; Path=/`,
          ]);
        expect(res.status).toBe(200);
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__twofaLinkSent=; Domain=.${host}; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`,
        ]);
        expect(res.text).toMatchSnapshot();
      });
    });
  });
});
