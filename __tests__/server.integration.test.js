import { jest } from '@jest/globals';
import dotenv from 'dotenv';
import supertest from 'supertest';
import { Forms } from '../src/app/forms.js';
import getCurrentDateModule from '../src/app/components/i18n/get-current-date.js';
import getTimeZoneModule from '../src/app/components/i18n/get-time-zone.js';
import { Routes } from '../src/app/routing/routes.js';
import { startMockApi, stopMockApi, apiAddress } from '../__mocks__/api.js';

describe('testing GET endpoints', () => {
  const productHostname = 'publicationexample.com';
  const baseConfig = {
    ZAPLOG_FRONTEND_HOST: '127.0.0.1',
    ZAPLOG_FRONTEND_PORT: '65432',
    ZAPLOG_API_URL: 'http://localhost:8080/Api.php',
    ZAPLOG_GOTO_URL: 'http://localhost:8080/Api.php/goto?urlencoded=',
    ZAPLOG_DEFAULT_LANGUAGE: 'nl',
    ZAPLOG_AVAILABLE_LANGUAGES: 'en,nl',
    ZAPLOG_TITLE: 'Mindmeld',
    ZAPLOG_IMAGE: '/assets/img/mindmeld-logo-square-web.png',
    ZAPLOG_CHANNEL_SUBDOMAIN: 'true',
    ZAPLOG_COMMUNITY: 'true',
    ZAPLOG_FAVICON: '/favicon.ico',
    // TODO: because the server is not started in the root we need to do this, maybe workaround?
    ZAPLOG_PATH: '/',
    NODE_ENV: 'production',
  };
  let routes;
  let server;
  let app;

  beforeAll(async () => {
    await startMockApi();
    const { address, port } = await apiAddress;
    baseConfig.ZAPLOG_API_URL = `http://${address}:${port}`;
    baseConfig.ZAPLOG_GOTO_URL = `http://${address}:${port}/goto?urlencoded=`;
    const getCurrentDateSpy = jest.spyOn(
      getCurrentDateModule,
      'getCurrentDate'
    );
    getCurrentDateSpy.mockImplementation(
      () => new Date(2021, 10, 22, 21, 21, 21)
    );
    const getTimeZoneSpy = jest.spyOn(getTimeZoneModule, 'getTimeZone');
    getTimeZoneSpy.mockImplementation(() => 'America/New_York');
  });

  afterAll(async () => {
    await stopMockApi();
    await new Promise((resolve) => {
      server.close(() => {
        console.log('Zaplog frontend stopped');
        resolve();
      });
    });
  });

  describe('in the non-community version', () => {
    beforeAll(async () => {
      dotenv.config = () => {
        process.env = { ...baseConfig, ZAPLOG_COMMUNITY: 'false' };
      };
      const module = await import('../src/server/server.js');
      server = module.server;
      app = supertest(module.app);
      routes = Routes({
        channelHostname: `:channel.${productHostname}`,
        channelPathnamePrefix: '',
        communityHostname: productHostname,
        URLSearchParams: global.URLSearchParams,
        protocol: 'http:',
        configuration: {
          community: false,
          channelSubdomain: true,
        },
      });
    });

    describe('as an unauthenticated user', () => {
      it('should respond with the article page', async () => {
        const res = await app
          .get(
            routes.article.getPathname({
              id: 29358,
              title: 'drs-karen-hamaker-zondag',
            })
          )
          .set('Host', `g-b-wolf.${productHostname}`);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should respond with the login page', async () => {
        const res = await app
          .get(routes.home.getPathname())
          .set('Host', productHostname);
        expect(res.status).toBe(200);
        expect(res.text).toMatchSnapshot();
      });

      it('should log the user in', async () => {
        const res = await app
          .post(routes.home.getPathname())
          .set('Host', productHostname)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'test@testuser.com',
            action: 'create',
          });
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.login.getHref());
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22test%40testuser.com%22%7D%5D; Domain=.${productHostname}; Path=/`,
          `__cookie-storage__twofaLinkSent=twofaLinkSent; Domain=.${productHostname}; Path=/`,
        ]);
      });

      it('should log the user in with an existing account', async () => {
        const res = await app
          .post(routes.home.getPathname())
          .set('Host', productHostname)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'test@testuser.com',
            action: 'existing',
          });
        expect(res.status).toBe(303);
        expect(res.headers.location).toBe(routes.login.getHref());
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22test%40testuser.com%22%7D%5D; Domain=.${productHostname}; Path=/`,
          `__cookie-storage__twofaLinkSent=twofaLinkSent; Domain=.${productHostname}; Path=/`,
        ]);
      });

      it('should show the error when logging in fails', async () => {
        const res = await app
          .post(routes.home.getPathname())
          .set('Host', productHostname)
          .type('form')
          .send({
            form_id: Forms.CreateArticleForm,
            email: 'error@testuser.com',
          });
        expect(res.status).toBe(400);
        expect(res.text).toMatchSnapshot();
        expect(res.headers['set-cookie']).toEqual([
          `__cookie-storage__ZaplogUserTwoFA=%5B%7B%22type%22%3A%22email%22%2C%22value%22%3A%22error%40testuser.com%22%7D%5D; Domain=.${productHostname}; Path=/`,
        ]);
      });
    });
  });
});
