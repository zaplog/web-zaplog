FROM node:19
ENV NODE_ENV production
WORKDIR /app
COPY . .
RUN npm ci
EXPOSE 3000
CMD [ "npm", "start" ]
