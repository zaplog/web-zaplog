import express from 'express';
import { apiMiddleware } from './api-middleware.js';

const app = express();
let resolveApiAddress;
let server;
export const apiAddress = new Promise((resolve) => {
  resolveApiAddress = resolve;
});

app.use(express.urlencoded({ extended: true }));
app.use(apiMiddleware);

export const startMockApi = () =>
  new Promise((resolve) => {
    server = app.listen(0, '127.0.0.1', () => {
      console.log(
        `Mock api server listening at http://${server.address().address}:${
          server.address().port
        }`
      );
      resolveApiAddress(server.address());
      resolve();
    });
  });

export const stopMockApi = () =>
  new Promise((resolve) => {
    server.close(() => {
      console.log('Mock api server stopped');
      resolve();
    });
  });
