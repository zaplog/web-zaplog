import {
  statistics,
  links,
  frontpage,
  link,
  emptyLink,
  linkReactions,
  channelReactions,
  tags,
  channels,
  activities,
  sessions,
  activeChannels,
  activeTags,
  discussion,
  channel,
  emptyChannel,
  channelLinks,
  relatedTags,
  tagChannels,
  tagLinks,
  unpublishedLink,
  unpublishedLinks,
  config,
} from './mockdata.js';
import { baseConfiguration } from '../src/app/base-configuration.js';

export const apiMiddleware = (req, res, next) => {
  const authenticated = !!req.get('Authorization');
  if (req.method === 'GET') {
    switch (req.originalUrl) {
      case `/config`:
        res.status(200).json(config);
        break;
      case `/frontpage`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(frontpage);
        break;
      case `/statistics`:
        res.status(200).json(statistics);
        break;
      case `/archivepage?offset=0&count=${baseConfiguration.pageSize}`:
        res.status(200).json({ links, tags, channels });
        break;
      case `/archivepage?offset=${baseConfiguration.pageSize}&count=${baseConfiguration.pageSize}`:
        res.status(200).json({ links, tags, channels });
        break;
      case `/archivepage?offset=0&count=${baseConfiguration.pageSize}&search=%239-11`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json({ links: tagLinks, tags, channels });
        break;
      case `/archivepage?offset=${baseConfiguration.pageSize}&count=${baseConfiguration.pageSize}&search=%239-11`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json({ links: tagLinks, tags, channels });
        break;
      case `/archivepage?offset=0&count=${baseConfiguration.pageSize}&search=%239-10`:
        res
          .status(404)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json({ message: 'Mocked error' });
        break;
      case `/links/id/29358`:
        res.status(200).json(link);
        break;
      case `/links/id/39358`:
        res.status(200).json(emptyLink);
        break;
      case `/links/id/49358`:
        res.status(200).json(unpublishedLink);
        break;
      case `/links/id/29359`:
        res.status(404).json({ message: 'Mocked error' });
        break;
      case `/reactions/link/29358`:
        res.status(200).json(linkReactions);
        break;
      case `/reactions/link/39358`:
        res.status(200).json([]);
        break;
      case `/reactions/link/49358`:
        res.status(200).json([]);
        break;
      case `/reactions/link/29359`:
        res.status(404).json({ message: 'Mocked error' });
        break;
      case `/index`:
        res.status(200).json(tags);
        break;
      case `/channels`:
        res.status(200).json(channels);
        break;
      case `/activities`:
        res.status(200).json(activities);
        break;
      case `/sessions`:
        res.status(200).json(sessions);
        break;
      case `/channels/active`:
        res.status(200).json(activeChannels);
        break;
      case `/tags/active`:
        res.status(200).json(activeTags);
        break;
      case `/discussion?offset=0&count=${baseConfiguration.pageSize}`:
        res.status(200).json(discussion);
        break;
      case `/discussion?offset=${baseConfiguration.pageSize}&count=${baseConfiguration.pageSize}`:
        res.status(200).json(discussion);
        break;
      case `/channels/id/test-channel-1260`:
        res.status(200).json(channel);
        break;
      case `/channels/id/test-channel-2260`:
        res.status(200).json(emptyChannel);
        break;
      case `/channels/id/test-channel-3260`:
        res.status(200).json({
          ...channel,
          channel: {
            ...channel.channel,
            id: 3260,
            name: 'test-channel-3260',
          },
        });
        break;
      case `/channels/id/admin`:
        res.status(200).json({
          ...channel,
          channel: {
            ...channel.channel,
            id: 1,
            name: 'admin',
            language: 'nl',
            algorithm: 'mixed',
          },
        });
        break;
      case `/reactions/channel/test-channel-1260`:
        res.status(200).json(channelReactions);
        break;
      case `/reactions/channel/test-channel-1261`:
        res.status(200).json(channelReactions);
        break;
      case `/reactions/channel/test-channel-3260`:
        res.status(200).json(channelReactions);
        break;
      case `/reactions/channel/test-channel-2260`:
        res.status(200).json(channelReactions);
        break;
      case `/channels/id/test-channel-1261`:
        res.status(404).json({ message: 'Mocked error' });
        break;
      case `/links/channel/test-channel-1260?offset=0&count=${baseConfiguration.pageSize}`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(channelLinks);
        break;
      case `/links/channel/test-channel-3260?offset=0&count=${baseConfiguration.pageSize}`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(channelLinks);
        break;
      case `/links/channel/test-channel-1260?offset=${baseConfiguration.pageSize}&count=${baseConfiguration.pageSize}`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(channelLinks);
        break;
      case `/links/channel/test-channel-2260?offset=0&count=${baseConfiguration.pageSize}`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(channelLinks);
        break;
      case `/links/channel/test-channel-2260?offset=${baseConfiguration.pageSize}&count=${baseConfiguration.pageSize}`:
        res
          .status(200)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json(channelLinks);
        break;
      case `/links/channel/test-channel-1261?offset=0&count=${baseConfiguration.pageSize}`:
        res
          .status(404)
          .set('last-modified', new Date(2021, 10, 22, 21, 21, 21).toString())
          .json({ message: 'Mocked error' });
        break;
      case `/links/unpublished`:
        res.status(200).json(unpublishedLinks);
        break;
      case `/tags/related/9-11`:
        res.status(200).json(relatedTags);
        break;
      case `/tags/related/9-10`:
        res.status(404).json({ message: 'Mocked error' });
        break;
      case `/channels/tag/9-11`:
        res.status(200).json(tagChannels);
        break;
      case `/channels/tag/9-10`:
        res.status(404).json({ message: 'Mocked error' });
        break;
      case `/2factor/valid-mock-2fa-token`:
        res.status(200).json({
          token: { 'X-Session-Token': 'valid-mock-session-token' },
          channel: { id: 1260, name: 'test-channel' },
        });
        break;
      case `/2factor/invalid-mock-2fa-token`:
        res.status(401).json({ message: 'Mocked error' });
        break;
      case `/urlmetadata?urlencoded=https%3A%2F%2Fwww.mocksite.com%2Fsection%2Fpage`:
        res.status(200).json({
          url: 'https://www.mocksite.com/section/page',
          title: 'Mocked title',
          description: 'Mocked description',
          image: 'https://www.mocksite.com/section/image.jpg',
          language: 'nl',
          copyright: 'No Rights Apply',
          mimetype: 'text/html',
          keywords: ['mocked', 'keywords'],
        });
        break;
      case `/urlmetadata?urlencoded=https%3A%2F%2Fwww.mocksite.com%2Fsection%2Femptypage`:
        res.status(200).json({
          url: 'https://www.mocksite.com/section/page',
          title: 'Mocked title',
          description: 'Mocked description',
          image: null,
          language: null,
          copyright: 'No Rights Apply',
          mimetype: 'text/html',
          keywords: [],
        });
        break;
      case `/urlmetadata?urlencoded=https%3A%2F%2Fwww.mocksite.com%2Fsection%2Ferrorpage`:
        res.status(400).json({ message: 'Mocked error' });
        break;
      default:
        throw new Error(
          `unimplemented GET endpoint in mock api: ${req.originalUrl}`
        );
    }
  } else if (req.method === 'POST') {
    let isCreateFailing;
    let isPreviewEmpty;
    let isReactionFailing;
    let isLoginFailing;
    switch (req.originalUrl) {
      case `/links/id/29358`:
        res.status(200).send(true);
        break;
      case `/links/id/39358`:
        res.status(400).send(false);
        break;
      case `/votes/link/29358`:
        res.status(authenticated ? 200 : 401).json({ message: 'Mocked error' });
        break;
      case `/votes/link/39358`:
        res.status(400).json({ message: 'Mocked error' });
        break;
      case `/tags/link/29358`:
        if (authenticated) {
          res.status(200).send(123);
        } else {
          res.status(401).json({ message: 'Mocked error' });
        }
        break;
      case `/sessions`:
        isLoginFailing = req.body?.email?.includes('error');
        if (isLoginFailing) {
          res.status(400).json({ message: 'Mocked error' });
        } else {
          res.status(200).json({ message: 'success' });
        }
        break;
      case `/reactions/link/29358?preview=1`:
        res.status(200).json({
          markdown: 'test comment',
          xtext: 'test comment',
        });
        break;
      case `/reactions/link/29358`:
        isReactionFailing = req.body?.markdown?.includes('failing');
        if (isReactionFailing) {
          res.status(400).json({ message: 'Mocked error' });
        } else {
          res.status(200).json(9876);
        }
        break;
      case `/links`:
        isCreateFailing = req.body?.markdown?.includes('error');
        if (isCreateFailing) {
          res.status(400).json({ message: 'Mocked error' });
        } else {
          res.status(200).json({
            id: 123,
            url: isPreviewEmpty
              ? null
              : 'https://www.mocksite.com/section/page',
            title: 'Mocked title',
            markdown: 'Mocked description',
            image: isPreviewEmpty
              ? null
              : 'https://www.mocksite.com/section/image.jpg',
            language: isPreviewEmpty ? null : 'nl',
            copyright: 'No Rights Apply',
            mimetype: 'text/html',
            tags: isPreviewEmpty ? [] : ['mocked', 'keywords'],
          });
        }
        break;
      default:
        throw new Error(
          `unimplemented endpoint in mock api: ${req.originalUrl}`
        );
    }
  } else if (req.method === 'PATCH') {
    let isErroring;
    switch (req.originalUrl) {
      case `/channels`:
        isErroring = req.body?.name?.includes('erroring');
        if (isErroring) {
          res.status(400).json({ message: 'Mocked error' });
        } else {
          res.status(200).send();
        }
        break;
      default:
        throw new Error(
          `unimplemented endpoint in mock api: ${req.originalUrl}`
        );
    }
  } else if (req.method === 'DELETE') {
    const token = req.get('X-Session-Token');
    switch (req.originalUrl) {
      case `/links/id/39358`:
        res.status(200).send();
        break;
      case `/sessions`:
        if (token !== 'invalid-mock-session-token') {
          res.status(200).send();
        } else {
          res.status(500).send();
        }
        break;
      default:
        throw new Error(
          `unimplemented endpoint in mock api: ${req.originalUrl}`
        );
    }
  }
  next();
};
